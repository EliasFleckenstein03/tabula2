#!/bin/sh
# Dieses schlichte (jetzt dash-kompatible) Skript aktualisiert die Installation mittels
#  git (nachdem lokale Änderungen ge-stash-t wurden) und
#  startet dann den apache neu. BieC 2021-03-05

cd /opt/tabula.info/tabula2
(
    /bin/date
    /usr/bin/git stash
    /usr/bin/git pull
    /bin/systemctl restart apache2
)  >> ../local/data/log/update.log 2>&1

