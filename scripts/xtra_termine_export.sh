#!/bin/bash

#________________________________________________________________________
# s_turbo_export.sh                                                       
# This file is part of tabula.info, which is free software under              
#     the terms of the GPL without any warranty - see the file COPYING 
# Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
#         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
#________________________________________________________________________
#
# s_turbo_export.sh lädt eine vorhandene export.html per ftp hoch, und
# verschiebt sie unter neuem Namen mit Zeitstempel in das Log-Verzeichnis
# Zugangsdaten und Pfad stehen in der ti.config. 
# 
# Dieses Skript wird vom script_dispatcher.sh aufgerufen, welcher per 
#       Cron-Job mit den Rechten von www-data regelmäßig aufgerufen werden!
#________________________________________________________________________

scriptpath=`dirname $(readlink -f ${0})`/
basepath=`realpath ${scriptpath}..`/
##basepath=`/opt/tabula.info/bin/ti_tool.py basepath`
logpath=${basepath}data/log/

cd ${basepath}data/tmp
log=${logpath}"upload.log"

ftpc=`${basepath}bin/ti_tool.py  s_turbo_FTPCredentials`
ftppath=`${basepath}bin/ti_tool.py  s_turbo_FTPPath`
ftpcsspath=`${basepath}bin/ti_tool.py  s_turbo_FTPCSSPath`

termineurl='https://secure.kronberg-gymnasium.de/cgi-bin/a_termine.py?grab=hzwq'
termineurl2='https://secure.kronberg-gymnasium.de/cgi-bin/a_termine.py?grab=lzweq'

echo xtra `date` >>${log}
echo `pwd` >>${log}
curl -v -o termine.html ${termineurl} >>${log}
curl -v -o termine2.html ${termineurl2} >>${log}
ls -halt termine* >>${log}
if [ -f termine.html ];
then
	echo "Das Skript "$0" sollte jetzt hochladen" >>${log}
	curl -v --upload-file termine.html -u ${ftpc} ${ftpcsspath} >>${log}
	curl -v --upload-file termine2.html -u ${ftpc} ${ftpcsspath}/../lehrer/ >>${log}
fi

