��    5      �  G   l      �     �     �  	   �  >   �  3   �     $  S  ,  |  �  E   �     C     _     |     �  
   �     �     �  Q   �  ,   7	  	   d	     n	  =   �	  >   �	     �	     
     
     ,
     5
  '   O
     w
     �
     �
     �
     �
     �
  !   �
  !   �
  ;     "   Q  H   t     �     �     �     �       R      z  s  e  �  +   T     �  *   �     �     �  u  �     `     o     ~  :   �  '   �     �  �   �  3   �  7        L     c     z     �     �     �     �  2   �  &        )     2  9   D  :   ~     �     �     �     �  !   �  %        9     L     `     x     �  
   �     �     �  4   �       6   #      Z     {     �     �     �  X   �  �     k   �  <        >  %   Q     w     �         
                     *   +                             "          2            3   4   1                      5          &      #         '                       /   !   .   -   ,                          0          )   $          %   	   (        (keine Pläne) Administration Anmeldung Anzahl Sekunden, nach denen Extrameldungen ausgeblendet werden App liefert statischen Content selbst per WSGI aus. Aushang Benenne neben Q11, Q12 und Lehrkräfte hier eine von drei weiteren Clientkategorien,
                    an welche die Meldungen adressiert werden können.
                    Minuszeichen verhindert Anzeige. A, B, M sind die entsprechenden Endungen,
                    die beim Hochladen von Plänen ausgewertet werden (wie auch Q und L) Das neue Passwort muss mindestens 8 Zeichen lang sein.<br><br>
        <b>Tipps:</b><br>
        Verwenden Sie nie gleiche Passwörter für verschiedene Dienste.<br>
        Sonderzeichen sind gefährlich, da sie im Web nicht immer eindeutig übertragen werden.<br>
        Datenschutzinfo: Das eingegebene Passwort wird nicht im Klartext, sondern als "Hash mit Salt" gespeichert! Dieses Logfile wird hiermit abgeschlossen, umbenannt und neu angelegt Falscher Name oder Passwort Falscher Name oder Passwort! Fehlermeldung Hier klicken zum Weiterarbeiten Infoupload Infoupload erweitert Keine Meldungen anzuzeigen Keine Verbindung zur Konfigurationsdatenbank configdb.sq3. Rechte falsch gesetzt? Maximale Länge einer Meldung (Überschrift) Meldungen Meldungen erweitert Nachricht, die am unteren Rand links dauerhaft angezeigt wird Nachricht, die am unteren Rand rechts dauerhaft angezeigt wird Nachtruhemodus Name Neues Logfile nach Reset Passwort Passwort wurde geändert! Passwort wurde nicht geändert! Grund:  Passwort zu kurz Passwort ändern Passwörter verschieden Personenruf Plananzeige Pläne: Sie wurden erfolgreich abgemeldet Sie wurden erfolgreich angemeldet Soll an der linken Seite ein Werbe-GIF eingeblendet werden? Sollen Infoseiten angezeigt werden Sollen die Versionsnummer, Produktname und Platzhalter verborgen werden? Titel des Tabulasystems Unbekannter User Unvollständige Eingaben Vertretungsplan Weiterleitung... Welche Hintergrundfarbe soll um das Werbe-GIF angezeigt werden? Schwarz empfohlen. Wie viele Zeilen lang darf die Personenruf+Meldungs-Spalte sein, bevor die die Personenrufe nach rechts kommen.<br>Dabei werden Tage und Meldungsüberschriften mit dem Faktor 1, 2 gezählt.<br>Den aktuellen gerundeten Wert kann man im Quelltext der angezeigten "Aushang"-Seite unter virtuelle_nachrichten_zeilenzahl in den Debuggingmeldungen in den Kommentaren am Ende nachlesen Wieviel Sekunden soll die reine Meldungs/Infoseite (phase=0) mindestens angezeigt werden.<br>Addiert wird dazu dynamisch:<br>&nbsp;* je Meldung 1-3 Sekunden (je nach Länge)<br>&nbsp;* je 3 Personenrufe eine Sekunde.<br>Diese Zeit wird auch addiert, wenn es nur einen halbseitigen Plan gibt und rechts daneben alle Meldungen und Infoseiten angezeigt werden. Wieviele Minuten bleibt ein User eingeloggt composer.py scheiterte tabula.info - Bitte Vollbildschirm wählen tabula.info Basis tabula.info Erweitert Project-Id-Version: tabula.info
PO-Revision-Date: 2021-02-12 18:41+0100
Last-Translator: C. Bienmueller <cb@tabula.info>
Language-Team: cb
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
 (no schedules) Administration Login Number of seconds after which the extra messages fade out. The App serves static content via WSGI. Notice Board You can name 3 categories (aside from Q11, Q12 and Lehrkräfte (teachers) ) to which you can address messages. A minus sign disables the category
. The chosen character ist also used to send a plan to screens attached to this category. The new password must be 8 or more characters long! This logfile will be finished, closed and created again Wrong name or password Wrong name or password Error message Klick to continue Info upload Info upload extended No messages to show Cannot connect to config database. File rights ok? Maximum length of a message (headline) Messages Messages extended Message to be shown permanently at the bottom (left half) Message to be shown permanently at the bottom (right half) night sleep mode Name New log file after reset Password Password is successfully changed! Password couldn't be changed. Reason: Password too short Change the password Passwords not identical Persons called Schedule management Schedules: You are now logged out You are now logged in Should there be an animated gif left of the content? Should info pages be shown Should version number, product name etc. be displayed? Titel of your tabula.info system Unknown user Missing Input Schedule Forwarding... Which color do you prefer to be around the animated gif? Black might be the best choice. Maximum number of virtual lines in messages. If there are more than these calculated, both columns will be used for messages and persons call. Seconds to show a pure messages & info page (phase=0). 1 to 3 seconds are added per Message and prson call. Number of minutes a user stays logged in without interaction composer.py failed tabula.info - full screen recommended tabula.info base tabula.info extended 