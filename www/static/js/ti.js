// dieser Code erledigt Kleinigkeiten
// Kontext: frame.py in tabula.info
// Autor: C. Bienmueller
// Rechte: public domain (da trivial)


var d_s_handle;
var ti_time_element;
var counter=50;
var timeout=1000;
var counterdelta=1;       

// Wird 1x z.B. aus dem Body-Tag aufgerufen
function ti_start() {
	if (window.location.search.substring(1).indexOf("RasPi")>=0) {
		timeout=2000; 
        counterdelta=2;
		console.log("RasPi -> slow... -> alle 2 Sekunden Zeit aktualisieren");}
    ti_time_element=document.getElementById("ti_time");
    counter=parseInt(ti_time_element.innerHTML)
	ti_time();
    d_s_handle=window.setInterval( function(){ ti_time() },timeout);
    startelementscrolling();
}
function startelementscrolling() {
    var allelements = document.getElementsByClassName('scrollcontent');
    console.log(allelements,"!");
    for (let oneelement of allelements) {
        scrollelement(oneelement);
    }
}
// Wird immer wieder aufgerufen 
function ti_time() {
    ti_time_element.innerHTML=" "+counter+"s";
    if (counter>=counterdelta) {
        counter-=counterdelta;
    }
    else {
        counter=0;
    }
}

// Nun: Scrollen eines div-Inhalts:
// scrollelement(document.getElementById('scrollcontent'));
    
function scrollelement(element) {
    if (element!=undefined) {
        var lasttop = element.scrollTop;
        const animateScroll = function(){
            var newtop=lasttop+1;
            element.scrollTop = newtop;
            lasttop = element.scrollTop;
            if(newtop===lasttop) {
                //console.log(lasttop);
                setTimeout(animateScroll, 20);
            }
            else {
                //console.log("ganz unten");
                setTimeout(animateReScroll, 5000);
            };
        };
        const animateReScroll = function(){
            var newtop=lasttop-5;
            element.scrollTop = newtop;
            lasttop = element.scrollTop;
            if(newtop>0) {
                //console.log(lasttop);
                setTimeout(animateReScroll, 20);
            }
            else {
                //console.log("ganz oben");
                setTimeout(animateScroll, 5000);
            };
        };
        setTimeout(animateScroll, 5000);
    }
}

// BACKOFFICE / MANAGEMENT
var bo_request; //globale Variable
var bo_ti_status="9"; //kein legaler Wert, wird daher geändert

var call_ajax_bo_status=function() {
    console.log("call_ajax_bo_status");
    $.getJSON  (   {    url:    "/ti/do/ajax",
                        data:   "task=get_bo_status&origstatus="+bo_ti_status,
                        success: function(result) 
                                    {   nexttimeout=1000;
                                        if (result["status"]==='ok') {
                                            bo_ti_status=result["neu_status"];
                                            bo_zeige(result["html"]); 
                                            if (result["neu_status"]===405) {
                                                nexttimeout=60*1000;
                                            }
                                        }
                                        else {
                                            console.log("Ajax-Fehlermeldung: "+result["status"]);    
                                        }
                                        window.setTimeout("call_ajax_bo_status()", nexttimeout);
                                    },
                        error: function() { window.setTimeout("call_ajax_bo_status()", 1000);}
        }
    )
}            

var bo_getAJAX = function() {
    call_ajax_bo_status();
}

function noneesta() {
    bo_request= new XMLHttpRequest();
    bo_request.open("GET", "ajax?task=get_bo_status&origstatus="+bo_ti_status);
    bo_request.onreadystatechange = checkData;
    bo_request.send(null);
} // end function getAJAX

function checkData() {
    if (bo_request.readyState == 4) {
        if ( bo_request.status == 200) {
            felder=bo_request.responseText.split(';', 2);
            bo_ti_status=felder[0];
            bo_zeige(felder[1]);
        }
        else{
            bo_zeige('&nbsp;<br>&nbsp;');
        }
        window.setTimeout("bo_getAJAX()", 1000)
    }   
} // end function checkData

function bo_zeige(statustext) {
    s_div=document.getElementById("statusmeldung");
    s_div.innerHTML = statustext;
} // end function zeige()

