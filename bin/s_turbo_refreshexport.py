#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_turbo_refreshexport.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Zweck:
    Aktueller Vertretungsplan im Internet
 Nutzung:
    Aufruf durch cron-Job nach Mitternacht
 Funktion
    Stößt Erzeugung neuer export.html-Datei an (die ein anderer Prozess hochlädt)
________________________________________________________________________
'''

# Batteries
# (nix)
# TI
# import s_turbo


# # Deaktiviert FIXME! 
# s_turbo.export_html(True, "Automatisch aufgefrischt")

