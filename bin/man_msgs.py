#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 man_msgs.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 man_msgs.py ermöglicht die Meldungen im Browser zu verwalten und
 nutzt dabei die Datenstruktur message_obj aus messages.py
________________________________________________________________________
'''
# Batteries

# TI
import ti
import ti_lib
import konst
import messages
    
def get_edit_message_form(ti_ctx, myrights, tiflag, dayorflag, headline, 
                          details, author, is_extra_group, is_red, n2d, lasthour=7):
    if n2d < 0:
        n2d = "none"
    else:
        n2d = str(n2d)
    if tiflag < 0:
        dayorflag = ti.get_following_workdayofepoche(0 if ti.get_akt_hour() < 5 else 1)
    is_sonder = True if tiflag == messages.TI_SONDERMELDUNG else False
    is_info = True if tiflag == messages.TI_INFOMELDUNG else False
    
    extragroupindex = messages.indexfirstbit(is_extra_group) + 1  # kein gesetzes Gruppenbit liefert -1+1=0  :-)
    
    maxlen = ti_ctx.cnf.get_db_config_int("Message_MaxTitleLength", 40)
        
    rbuttons = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
    rbuttons[0] = 'checked="checked"'
    
    radios = 5*'\t'+'<table nobord="true"><tr>\n'+6*'\t'
    autogrey = ''
    todays_dow = ti.get_todays_dayofweek()
    if todays_dow < 5:  # mo-fr
        for i in range(todays_dow):
            radios += '<td nobord="true"'+autogrey+'> </td>'
    else:
        todays_dow = 0
    for i in range(0, 15-todays_dow):
    
        doe = ti.get_following_workdayofepoche(i)
        if doe == dayorflag:
            rbuttons[0] = ""
            rbuttons[i] = 'checked="checked"'
        datumslist = ti.get_string_from_dayofepoche(doe, mediumshort=True).split(", ")
        datumsstrg = f'<strong>{datumslist[0]}</strong>, <small>{datumslist[1]}</small>'
        radios += (f'<td nobord="true"{autogrey}> <input name="doy" value="{str(doe)}" '
                   f'type="radio" {{rb[{str(i)}]}}>{datumsstrg}&nbsp;&nbsp;&nbsp;</td>\n')
        if not (i + 1 + todays_dow) % 5:
            radios += '</tr><tr>\n' + 6 * '\t'
    radios += '</table>'
        
    # ## Einstellung, Ziele, Variation ###
    optionsstring = '''
                    <tr><td nobord="true">Einstellungen:</td>
                        <td nobord="true"><strong>Ziele<small> (wem wird es gezeigt)</small></td>
                        <td nobord="true"><strong>Variation<small> (statt Standardmeldungen)</small></td>
                    </tr>'''

    # ## NEUE Auswahlschleife für die Ziele
    optionsstring += '''<tr><td nobord="true"> </td>\n\t\t<td nobord="true">'''
    
    for i in range(7 if myrights & konst.RECHTEMeldungenerweitert else 5):
        gn, gk = ti_ctx.cnf.get_gruppenbez(i-1)  # GruppenName, GruppenKürzel 
        if gn[0] == '-' and i:
            continue
        wert = 0
        if i:
            wert = 1 << (i - 1)
        optionsstring += '<input type="radio" name="option_extra_group" value="'+str(wert)+'"'
        if extragroupindex == i:
            optionsstring += ' checked="checked"'
        optionsstring += '><strong><span style="color:black">'
        if i > 0: 
            optionsstring += gn  # s.o.
            if i > 4:
                optionsstring += "*"
        else:
            optionsstring += '<i>allgemein</i>'
        optionsstring += '</span> </strong><br>\n'
     
    optionsstring += '</td>'
    
    # ## NEU rechts daneben Liste der Variationen
    optionsstring += '<td nobord="true">'
    
    # >> Rotauswahl
    optionsstring += '<input type="checkbox" name="option_red" value="True"'
    if is_red:
        optionsstring += ' checked="checked"'
    optionsstring += '''><strong><span style="color:red"> Rote Meldung </span></strong><br><small> 
        (macht die Meldung als einzige an ihrem Tag rot<br> sparsam verwenden!)</small><br>\n'''

    # >> Info-allgemein
    optionsstring += '<input type="checkbox" name="option_info" value="True"'''   
    if is_info:
        optionsstring += ' checked="checked"'
    optionsstring += '''><strong><span style="color:black;background-color:yellow"> Info allgemein </span>, 
        ganz unten</strong><br><small>  (z.B. Nachhilfe gesucht, <br> nicht mit rot kombinierbar)</small></span><br>\n'''

    # >> Sondermeldung
    if myrights & konst.RECHTEMeldungenerweitert:
        optionsstring += '<input type="checkbox" name="option_sonder" value="True"'
        if is_sonder:
            optionsstring += ' checked="checked"'
        optionsstring += '''><strong><span style="color:white;background-color:green">
            Sondermeldung </span>*, ganz oben</strong><br><small>  
            (sollte max. <b>einmal im Monat</b> benutzt werden!<br> Kann mit rot als ALARM kombiniert werden)</small><br>\n'''
        optionsstring += '</td></tr>'
                    
    details = '\n'.join(details.split("<br>"))
            
    skeleton = '''
    <div class="firstmgmcontent">
    <h3>Bearbeitung einer Meldung</h3>
    </div>
    <div class="messages">

        <form action="manage?task=messages" method="post" style="text-align: center;">
        <input name="todo" value="add" type="hidden">
        <input name="number_2_delete" value="{n2d_predefined_value}" type="hidden">
        <table nobord="true" border="0">
            <tbody>
                <tr>
                    <td nobord="true" valign="top" ><label for="doy"><strong>letzter</strong>
                        Gültigkeitstag<BR> der Meldung<BR> (muss immer <BR> gewählt werden)</label><br></td>
                    <td nobord="true" colspan="2">
                    '''+radios+'''
                    </td>
                </tr>
                
                <tr><td nobord="true">&emsp;</td><td nobord="true">&emsp;</td><td nobord="true">&emsp;</td></tr>
                
''' + optionsstring + '''               
                <tr><td nobord="true">&emsp;</td><td nobord="true">&emsp;</td><td nobord="true">&emsp;</td></tr>
                <tr>
                    <td nobord="true"><label for="headline">Schlagzeile <br> ({maxlen} Zeichen)</label><br></td>
                    <td nobord="true" colspan="2">
                        <input name="headline" value="{headline_predefined_value}" 
                            id="headline" size="50" maxlength="{maxlen}" type="text" class="eingabe"><br></td>
                </tr>
                <tr><td nobord="true" colspan="3">&emsp;</td></tr>
                <tr>
                    <td nobord="true" valign="top">
                        <label for="details">Details</label><br><small>
                        (Nur bei Bedarf)<br>Das Wort "heute" wird<br> ggf. durch den<br>Wochentag ersetzt</small></td>
                    <td nobord="true" colspan="2">
                        <textarea name="details" id="details" 
                            class="eingabe" rows="6" cols="56">{details_predefined_value}</textarea><br></td>
                </tr>
                <tr><td nobord="true" colspan="3">&emsp;</td></tr>
                <tr>
                    <td nobord="true"><label for="lasthour">Letzte Unterrichts-</label><br>stunde zum Anzeigen</td>
                    <td nobord="true" colspan="2">
                        <input name="lasthour" value="{lasthour}" id="lasthour" 
                               size="2" maxlength="2" type="text" class="eingabe"><br></td>
                </tr>
                <tr>
                    <td nobord="true"><label for="author">Autor </label><br></td>
                    <td nobord="true" colspan="2">
                       <input name="author" value="{author_predefined_value}" id="author" 
                              size="16" maxlength="16" type="text" class="eingabe">
                       <br></td>
                </tr>
                
                <tr>
                    <td nobord="true">
                    </td>
                    <td nobord="true">
                        <input value="Absenden" type="image" src="../static/pix/ok.png">&ensp;
                    </td>
                    <td nobord="true">
                        <a href="manage?task=messages">
                            <img src="../static/pix/quit.png" width="150" height="19" border="0" alt="Beenden">
                        </a>
                    </td>
                    
                </tr>
            </tbody>
        </table>
        </form>
        
        <BR>
        <strong>Anleitung:</strong><small><pre>
        Wählen Sie den Tag, für den die Meldung gültig ist.
        Geben Sie eine knappe, aber verständliche Schlagzeile ein!
        Bei Bedarf können Details hinzugefügt werden.
        
                     "Fasse dich kurz", 
            denn die Bildschirmgröße und die Lesefreude
            der Schüler und anderer Leser ist begrenzt...

        Eine der Meldungen pro Tag bzw. pro Ziel kann rot sein. 
        Rote Meldungen werden an den Anfang ihres Blocks gezogen.
        Eine früher rot gewählte Meldung in dem Bereich wird damit
            schwarz.
         
        Infomeldungen und Sondermeldungen*  werden bis zum
            Gültigkeitstag unten bzw. oben angezeigt.
        Die "Variation" wird nur beim Ziel "allgemein" berücksichtigt. 
            Rot wird dann ignoriert.
        
        Mit der Tabulatortaste kann man zwischen den Feldern wechseln.
        Mit den Cursotasten kann man den Wochentag wählen.
        Mit der Eingabetaste wird das Formular abgeschickt.
        
        </pre></small>
    </div>
</body>
'''
    return skeleton.format(
                headline_predefined_value=headline,
                details_predefined_value=details,
                author_predefined_value=author,
                n2d_predefined_value=n2d,
                maxlen=str(maxlen),
                rb=rbuttons,
                lasthour=lasthour)


def do_manage(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    myrights = ti_ctx.check_management_rights(konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert)
    if not myrights:
        return [], -1
    maxlen = ti_ctx.cnf.get_db_config_int("Message_MaxTitleLength", 40)
    n2d = ti_ctx.req.get_cgi_int("number_2_delete", -1)  # Die rowid des Datenbankeintrags
        
    todo = ti_ctx.req.get_cgi("todo")
    ti_ctx.res.debug("*Todo*", todo)
    option_red = ti_ctx.req.get_cgi_bool("option_red")
    option_extra_group = ti_ctx.req.get_cgi_int("option_extra_group", 0)
    option_info = ti_ctx.req.get_cgi_bool("option_info")
    option_sonder = ti_ctx.req.get_cgi_bool("option_sonder")
    ti_ctx.res.debug("*options*", str(option_red)+"x"+"x"+str(option_extra_group))
    ti_ctx.res.debug("*n2d*", n2d)

    if todo == "edit":
        if n2d >= 0:
            try:
                ti_ctx.res.debug("going 2 edit", "True")
                mobj = messages.mdb_get_message(ti_ctx, n2d)  # selectiert nach rowid
                if mobj:
                    is_red = True if mobj.color == "red" else False
                    is_extra_group = mobj.target if mobj.flag == messages.TI_EXTRAMELDUNG else 0
                        
                    md_response.append_s(get_edit_message_form(ti_ctx, myrights, 
                                         mobj.flag, mobj.targetday, mobj.headline, mobj.details, mobj.author, 
                                         is_extra_group, is_red, n2d, mobj.lasthour))
            except():
                ti_ctx.res.debug("Meldung zum Editieren nicht gefunden")

    elif todo == "new":   
        md_response.append_s(get_edit_message_form(ti_ctx, myrights, -1, -1, "", "", 
                             ti_ctx.get_client_ip_or_name(), 0, False, -1))

    elif todo == "add":  # headline und details rauslesen, Linefeeds ersetzen und alle unerlaubten Zeichen entfernen
        details = ti_ctx.req.get_cgi("details").strip()
        details = details.replace("<", " ")
        details = details.replace(">", " ")
        
        details = '\n'.join(details.split("<br>"))
        details = '<br>'.join(details.split("\n"))
        
        headline = ti_ctx.req.get_cgi("headline")
        headline = headline.replace("<", " ")
        headline = headline.replace(">", " ")
        headline = ' '.join(headline.split("\n"))
        
        author = ti_ctx.req.get_cgi("author").strip()
        author = ''.join(author.split("\n"))
        author = author.replace("<", " ")
        author = author.replace(">", " ")
        
        lasthour = ti_ctx.req.get_cgi_int("lasthour", 5)
        
        # zu lange Headlines kürzen durch verschieben in details...
        while (len(headline) > maxlen):
            mlist = headline.rsplit(" ", 1)
            if len(mlist) > 1:
                details = mlist[1]+'. '+details
                headline = mlist[0]
            else:
                details = headline[maxlen:]+'. '+details
                headline = headline[:maxlen]
                
        # rausschreiben
        doy = ti_ctx.req.get_cgi_int("doy")
        if len(headline) > 0:
            if option_extra_group:
                flag = messages.TI_EXTRAMELDUNG
            elif option_info:
                flag = messages.TI_INFOMELDUNG
                option_red = False
            elif option_sonder:
                flag = messages.TI_SONDERMELDUNG
                # option_red = False
            else:
                flag = messages.TI_ACTIVE
            targetday = doy
            created = ti_ctx.sys.get_epoche()
            if option_red:
                color = "red" 
                if not option_sonder:
                    messages.mdb_uncolor(ti_ctx, flag, targetday)  # alle aus derselben Kategorie entfärben
            else:
                color = "x"  # dahinter im alphabet
            if len(author) < 4:
                author = ti_ctx.get_client_ip_or_name()
                
            ti_ctx.res.debug("Schreibe neue Meldung")               
            messages.mdb_write_message(ti_ctx, n2d, flag, targetday, created, 0, 
                                       lasthour, option_extra_group, headline, details, color, author)
            ti_ctx.set_erfolgsmeldung('+Ihre Meldung "'+headline+'" wurde korrekt übernommen')
        else:
            ti_ctx.set_erfolgsmeldung("-Ihre Meldung wurde leider NICHT übernommen (keine Schlagzeile eingegeben)")

    elif n2d >= 0 and todo == "delete":
        headline = messages.mdb_delete_message(ti_ctx, n2d)
        ti_ctx.set_erfolgsmeldung('+Meldung "'+headline+'" wurde gelöscht')
    e, m = messages.do_show_man_messages(ti_ctx)  # liefert zwei große Strings für Messages und Editbereich
    if todo in ["edit", "new"]:
        md_response.next_abschnitt()
    else:
        #md_response.append_s('<div class="firstmgmcontent" style="margin-bottom:0.5em"><h1>Meldungen verwalten</h1></div>\n')
        md_response.append_s('''
        <div class="messages"><h3><span alt_header=true>Neue Meldung anlegen:</span></h3>
            <div class="content"><table cellspacing="10"><tr><td  nobord="true">
            <a href="manage?task=messages&todo=new">
                <img src="../static/pix/new.png" width="150" height="19" border="0" alt="neu anlegen">
            </a></td></tr></table>
            </div>
        </div><br>''')
        md_response.append_s(e)
        md_response.next_abschnitt()
    md_response.append_s(m)
    return md_response

if __name__ == '__main__':
    ti_lib.quickabort2frames()
