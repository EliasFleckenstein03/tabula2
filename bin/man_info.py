#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 man_info.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 man_info.py ermöglicht informationen als PDF oder Bild hochzuladen
________________________________________________________________________
'''

# Batteries
import os
import uuid
import subprocess

# TI
import ti_lib
import konst
import infobox


def do_manage(ti_ctx):
    """Quatsch gelöscht
    """
    save_uploaded_file(ti_ctx)
    do_delete(ti_ctx)
    do_change(ti_ctx)

    md_response = ti_lib.Multi_Detail_Response()
    spezialrechte = ti_ctx.check_management_rights(konst.RECHTEInfoupload | konst.RECHTEInfouploaderweitert) \
        & konst.RECHTEInfouploaderweitert
    HTML_TEMPLATE_S = '''
        <hr>
        <h2 style="text-align:center;margin:3px">{}</h2>
    '''.format(_('Animierte Seiten verwalten'))
    HTML_TEMPLATE2 = '''
    <div class="infos"  style="margin-top:0.5em">

        {imageliste}
        
        
    </div>
    '''
    prio_color = ["#F79", "white", "#FFA", "#EE8", "#DD6", "yellow"]
    prio_label = ["Nie", "1", "2", "3", "4", "Max"]

    infopath = ti_lib.get_ti_dynpath() / "info/"
    webpath = '../dyn/info/'
    maxinfopix = min(ti_ctx.cnf.get_db_config_int('MaxInfoPix', 6), 99)  # 99, are U nuts?
    imagelist = '<div><h3>{}:</h3><br><table ><tr>'.format(
        _('Aktuelle Info-Seiten (sie werden im zufälligen Wechsel angezeigt)'))
    v_imagelist = '<div><h3>{}:</h3><br><table ><tr>'.format(_('Info-Seiten auf Vorrat'))
    zaehler = 0
    info_pix_cfgs_list = infobox.dir2info_pix_configs_list(ti_ctx, infopath)
                
    for ipcfg in info_pix_cfgs_list:
        if ipcfg.ext == 'png' and ipcfg.prio:
            if zaehler and not zaehler % 2:
                imagelist += '</tr>\n<tr>'

            prio_zeile = ''
            for prio_loop in range(6):
                if ipcfg.prio == prio_loop:
                    loop_label = f'<b>{prio_label[prio_loop]}</b>'
                    loop_color = 'black'
                else:
                    loop_label = prio_label[prio_loop]
                    loop_color = '#444'
                prio_zeile += f'''
                <a  href="manage?task=info&set_prio_for={ipcfg.name}&set_prio_to={prio_loop}" 
                    style="background-color:{prio_color[prio_loop]};color:{loop_color};">
                        {loop_label} 
                </a>&nbsp;'''

            imagelist += f'''
            <td style="padding:0.5em;text-align:center;vertical-align:bottom;background-color:{prio_color[ipcfg.prio%6]}">
                <a href="{webpath}{ipcfg.name}" target="_blank">
                    <img src="{webpath}{ipcfg.thumbnail}"></a><br>
                <span style="text-align:right">{_('Infoseite hochgeladen von')} {ipcfg.author}</span><br>
                <a href="manage?task=info&number_2_delete={ipcfg.ipid}">
                    <img src="../static/pix/erase.png" width="150" height="19" border="0" alt="delete"></a><br>
                {_("Häufigkeit")}: <span style="font-size:130%;background-color:#bbb;padding:2px;">{prio_zeile}
                </span>
            </td>\n'''
            zaehler += 1
    if zaehler > 0:
        imagelist += '</tr></table><br></div>'
    else:
        imagelist = ""
        
    # sammle nun die Vorratsbilder mit prio 0
    v_zaehler = 0
    for ipcfg in info_pix_cfgs_list:
        if ipcfg.ext == 'png' and not ipcfg.prio:
            if v_zaehler and not v_zaehler % 2:
                v_imagelist += '</tr>\n<tr>'

            prio_zeile = ''
            for prio_loop in range(6):
                if ipcfg.prio == prio_loop:
                    loop_label = f'<b>{prio_label[prio_loop]}</b>'
                    loop_color = 'black'
                else:
                    loop_label = prio_label[prio_loop]
                    loop_color = '#444'
                prio_zeile += f'''
                <a  href="manage?task=info&set_prio_for={ipcfg.name}&set_prio_to={prio_loop}" 
                    style="background-color:{prio_color[prio_loop]};color:{loop_color};">
                        {loop_label} 
                </a>&nbsp;'''

            v_imagelist += f'''
            <td style="padding:0.5em;text-align:center;vertical-align:bottom;background-color:{prio_color[ipcfg.prio%6]}">
                <a href="{webpath}{ipcfg.name}" target="_blank">
                    <img src="{webpath}{ipcfg.thumbnail}"></a><br>
                <span style="text-align:right">{_('Infoseite hochgeladen von')} {ipcfg.author}</span><br>
                <a href="manage?task=info&number_2_delete={ipcfg.ipid}">
                    <img src="../static/pix/erase.png" width="150" height="19" border="0" alt="delete"></a><br>
                {_("Häufigkeit")}: <span style="font-size:130%;background-color:#bbb;padding:2px;">{prio_zeile}
                </span>
            </td>\n'''
            v_zaehler += 1
    if v_zaehler > 0:
        v_imagelist += '</tr></table><br></div>'
    else:
        v_imagelist = ""
    
    inputlist = ""
    if zaehler >= maxinfopix:
        inputlist += '<div style="background-color:orange">{}</div>'.format(
                _('Meldung: Es können keine weiteren Bilder hinzugefügt werden!'))
    else:
        if zaehler > (maxinfopix / 2):
            imagelist += '<div style="background-color:yellow">{}</div>'.format(
                _('Warnung: Da so viele Bilder angezeigt werden sollen, sieht man die einzelnen Bilder recht selten!'))
        inputlist += f'''
            <h3>{_('Die aktuellen Info-Seiten sehen Sie im nächsten Rahmen')}</h3>
                <form action="manage?task=info" method="POST" enctype="multipart/form-data">
                {_('Eine Datei (PDF und Bildformate, möglichst quadratisch) wählen und abschicken')}:<br>
                    <input name="info_file" type="file" /> 
                    <input type="submit" name="hochladen" value="{_('Hochladen')}" /><br><br>
                    {_("""Beachte:<br>
                    Transparente Bereiche werden weiß und weisse Ränder werden abgeschnitten""")}
                </form>'''
    spezialliste = ""
    zaehler = 0
    if spezialrechte:
        spezialliste = HTML_TEMPLATE_S+'''
                        <div style="not-background-color:white">
                            <table style="not-background-color:white;border:0">
                                <tr>
                                    <th colspan=2><h3 style="margin:3px;">'''+_('Aktuelle animierte Seiten')+''':</h3></th>
                                <tr>'''
        for ipcfg in info_pix_cfgs_list:
            if ipcfg.ext == 'gif':

                prio_zeile = ''
                for prio_loop in range(6):
                    if ipcfg.prio == prio_loop:
                        loop_label = f'<b>{prio_label[prio_loop]}</b>'
                        loop_color = 'black'
                    else:
                        loop_label = prio_label[prio_loop]
                        loop_color = '#444'
                    prio_zeile += f'''
                    <a  href="manage?task=info&set_prio_for={ipcfg.name}&set_prio_to={prio_loop}" 
                        style="background-color:{prio_color[prio_loop]};color:{loop_color};">
                            {loop_label} 
                    </a>&nbsp;'''
                if zaehler and not zaehler % 2:
                    spezialliste += '</tr>\n<tr>'
                spezialliste += f'''
            <td style="padding:0.5em;text-align:center;vertical-align:bottom;background-color:{prio_color[ipcfg.prio%6]}">
                <a href="{webpath}{ipcfg.name}" target="_blank">
                    <img src="{webpath}{ipcfg.thumbnail}"></a><br>
                <span style="text-align:right">{_('Infoseite hochgeladen von')} {ipcfg.author}</span><br>
                <a href="manage?task=info&number_2_delete={ipcfg.ipid}">
                    <img src="../static/pix/erase.png" width="150" height="19" border="0" alt="delete"></a><br>
                {_("Häufigkeit")}: <span style="font-size:130%;background-color:#bbb;padding:2px;">{prio_zeile}
                </span>
                
            </td>\n'''
                zaehler += 1
        spezialliste += '</tr></table><br></div>'
        if zaehler < 2:
            spezialliste += f'''
                    <form action="manage?task=info" method="POST" enctype="multipart/form-data">
                        {_('Eine Datei (animiertes GIF oder mehrseitiges PDF, möglichst quadratisch) wählen und abschicken')}
                        :<br>
                        <input name="info_gif" type="file" />
                        <input type="submit" name="hochladen" value="{_('Hochladen')}" /><br><br>
                    <br>
                    { _("""Beachte:<br>
                    - der Import kann einige Sekunden dauern, <br>
                    - PDF-Seiten wechseln immer nach 4 Sekunden, <br>
                    - <small>bei Konvertierungs-/Darstellungsfehlern das hochzuladende GIF 
                        vorher in nicht-optimierter Form speichern</small>.""")}
            </form>'''
        else:
            spezialliste += _('Es können hier keine weiteren animierten Seiten hinzugefügt werden.')
        
    md_response.append_s('<div class="infos" style="margin-top:0.5em"><!-- Start erstes info-div -->')
    md_response.append_s(inputlist)
    md_response.append_s(spezialliste)
    md_response.append_s('</div><!-- Ende erstes info-div -->')
    
    if imagelist:
        md_response.next_abschnitt()
        md_response.append_s(HTML_TEMPLATE2.format(imageliste=imagelist))
    if v_imagelist:
        md_response.next_abschnitt()
        md_response.append_s(HTML_TEMPLATE2.format(imageliste=v_imagelist))
        
    return md_response


def save_uploaded_file(ti_ctx):
    """This saves a file uploaded by an HTML form.
    The form_field is the name of the file input field from the form.
    If no file was uploaded or if the field does not exist then
    this does nothing.
    """
    # FIXME: verhindere unbegrenzten Upload mit u.a. maxinfopix = min(ti_ctx.cnf.get_db_config_int('MaxInfoPix', 6), 20)
    success = False
    infopath = ti_lib.get_ti_dynpath() / "info"
    ist_gif = False
    unique = uuid.uuid4().hex
    upload_path = infopath / f'upl_{unique}'

    if ti_ctx.req.is_WSGI:
        origfilename = ti_ctx.req.save_file('info_file',upload_path)
        if not origfilename:
            origfilename = ti_ctx.req.save_file('info_gif',upload_path)
            if origfilename:
                ist_gif = True
            else:
                return False
    else:
        if len(ti_ctx.req.get_cgi('info_file')) < 2:
            if len(ti_ctx.req.get_cgi('info_gif')) >= 2: 
                ist_gif = True
            else: 
                ti_ctx.res.debug("HTML-Upload", "keine Dateien übergeben")   
                return ""
        resultat = "-" + _('Upload fehlgeschlagen')  # wird ggf. überschrieben...
        
        if ist_gif:
            fileitem = ti_ctx.req.get_cgi_object('info_gif')
        else:
            fileitem = ti_ctx.req.get_cgi_object('info_file')
        if not fileitem.file:
            return False
        origfilename=fileitem.filename
        ti_ctx.res.debug("HTML-Upload", "lese Info-Datei "+fileitem.filename)
        with upload_path.open(mode='wb') as fout:
            chunk = fileitem.file.read(1500000)   # limited to 1,5MB - wird ggf. wiederholt
            while chunk:
                fout.write(chunk)
                success = True
                chunk = fileitem.file.read(15000000)
            if success:
                ti_ctx.res.debug("HTML-Upload", "erfolgreich geschrieben")
            else:
                ti_ctx.res.debug("HTML-Upload", "doch keine erste Datei")
        # implizit fout.close()
        if not success:
            return False

    meldung = ""
    filetyp = 'gif' if ist_gif else 'png'
    
    temp_path = infopath / f'tmp_{unique}.{filetyp}'
    try:
        if ist_gif:
            if origfilename.lower().endswith('.pdf'):
                info_ani_delay = ' -delay {} '.format(ti_ctx.cnf.get_db_config_int('info_pdf_ani_delay', 5)*100)
            else:
                info_ani_delay = ''
            subprocess.check_output(
                f'gm convert {info_ani_delay} {upload_path.as_posix()}' 
                f' -coalesce -bordercolor white -border 0 -resize 950x1000 ' + 
                f' {temp_path.as_posix()}',
                shell=True, stderr=subprocess.STDOUT)
        else:
            subprocess.check_output(
                f'gm convert {upload_path.as_posix()}'
                f'[0] -background white -flatten -trim  -resize 950x1000  -bordercolor white'
                f' -interlace line {temp_path.as_posix()}',
                shell=True, stderr=subprocess.STDOUT)
        hashhex = ti_lib.berechne_md5_von(temp_path)
        file_path = infopath / (hashhex + '.' + filetyp)
        thumb_path = infopath / (hashhex + '-thumb.' + filetyp)
        
        temp_path.rename(file_path)
        subprocess.check_output(
            f'gm convert {file_path.as_posix()} '
            f'-resize  250x250 {thumb_path.as_posix()} &', 
            shell=True, stderr=subprocess.STDOUT)

        ipcfg = ti_ctx.cnf.infopix_get_full_cfg(file_path.name)
        ipcfg.author = ti_ctx.get_client_ip_or_name()
        ti_ctx.cnf.infopix_set_full_cfg(ipcfg)
        ti_ctx.res.debug("ipcfg as_tupel()", ipcfg.as_tupel())
        ipcfg = ti_ctx.cnf.infopix_get_full_cfg(file_path.name)
        ti_ctx.res.debug("ipcfg as_tupel()", ipcfg.as_tupel())

        subprocess.check_output('rm '+upload_path.as_posix(), shell=True, stderr=subprocess.STDOUT)

    except subprocess.CalledProcessError as err:
        meldung = '<pre>'+err.output.decode("utf-8", "ignore")+'</pre>'
    resultat = ('+'+_('erfolgreicher Upload mit Konvertierung')) if not meldung \
        else ('-'+_('Es ist ein Fehler bei der Konvertierung aufgetreten')+':<small><small><br>'+meldung+'</small></small>')
    ti_ctx.set_erfolgsmeldung(resultat)             
    ti_ctx.cnf.set_ti_lastupload()

    
def do_delete(ti_ctx):
    n2d = ti_ctx.req.get_cgi('number_2_delete', '')
    if n2d:
        infopath = ti_lib.get_ti_dynpath() / "info"
        info_pix_cfgs_list = infobox.dir2info_pix_configs_list(ti_ctx, infopath)
        for ipcfg in info_pix_cfgs_list:
            ti_ctx.res.debug("do_delete", n2d, ipcfg.name)
            if ipcfg.ipid == n2d:
                ti_ctx.res.debug("namen", ipcfg.name, ipcfg.thumbnail)
                try:
                    (infopath / ipcfg.name).unlink()
                    (infopath / ipcfg.thumbnail).unlink()
                    ti_ctx.set_erfolgsmeldung('+'+_('Erfolgreich gelöscht')) 
                    ti_ctx.cnf.infopix_rm_cfg(ipcfg.name)
                except Exception as e:
                    ti_ctx.set_erfolgsmeldung('-'+_('Fehler beim Löschen')+': '+str(e)) 
                break
        else:
            ti_ctx.set_erfolgsmeldung('-'+_('Zu löschende Datei nicht gefunden'))


def do_change(ti_ctx):
    filename = ti_ctx.req.get_cgi('set_prio_for', '')
    if filename:
        newprio = ti_ctx.req.get_cgi_int('set_prio_to', -1)
        ti_ctx.res.debug("set_prio", filename, newprio)
        if newprio >= 0 and newprio <= 5:
            ipcfg = ti_ctx.cnf.infopix_get_full_cfg(filename)
            if ipcfg.prio != newprio:
                ipcfg.prio = newprio
                ti_ctx.cnf.infopix_set_full_cfg(ipcfg)
                # ti_ctx.res.debug("ipcfg as_tupel()", ipcfg.as_tupel())

if __name__ == '__main__':
    ti_lib.quickabort2frames()
