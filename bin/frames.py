#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 frames.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 frames.py liefert die von composer.py generierten Seiten, welche
    tabula.info ausmachen, per CGI aus.
    Nur diese Seite wird von den Browsern zur Darstellung aufgerufen!                                         
________________________________________________________________________

'''

# Batteries (none))
 
# TI
import ti_lib

if __name__ == '__main__':
    ti_lib.quickabort2frames()

# Hier kommen wir nie mehr hin - CGI ist tot...
try:
    import ti               # gemeinsame Funktionen und 
    import composer

except Exception as e:
    ti_lib.panic(_('frames.py scheiterte bei den Imports'), e)


if __name__ == '__main__':
    # Aufruf als CGI-Skript - sonst geht nichts...
    try:
        ti_ctx = ti.master_init()
        output = composer.compose_frames(ti_ctx)
        ti.prt('\n'.join(output))
    except Exception as e:
        ti_lib.panic(_('frames.py scheiterte bei der Zusammenstellung'), e)

# & tschüß
