#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 config_clients.py                                                        
 This file is free software under         
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist  Freie Software gemäß
             der GPL ohne jegliche Garantie - siehe die Datei COPYING 
________________________________________________________________________

 config_clients.py vereint den Dialog und die Tätigkeiten
            zur Konfiguration und Überwachung der Clients
________________________________________________________________________
'''

# Batteries
# (nix)
# TI
import ti_lib
import ti
import config
import konst


def config_dialog(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    checkedsessionid = ti_ctx.get_checkedsessionid()
    
    clientverwaltungs_div_definition = '<div class="kategorie">'
    
    alleclients = ti_ctx.cnf.client_get_list()  # diese Liste enthält Tupel. Sortiert ist sie nach istconf, dann nach istanzg
    
    statemachine = 0  
    # 0: noch keine Überschrift geschrieben, 
    # 1: hat "konfigurierte Anzeigen" geschrieben, 
    # 2: hat "konfigurierte Clients geschrieben, 
    # 3: hat "unkonf. C." geschrieben
    
    for tupel in alleclients:
        ip, name, timestamp, maxphase, gruppen, istconf, istanzg, cukopplung, forward = tupel
        if statemachine == 0 and istconf and istanzg:
            md_response.append_s(f'''{clientverwaltungs_div_definition}
        <h3>Kontext: verwaltete Anzeigerechner \n
            {config.popuphilfe('Anzeigerechner', 
            """per IP-Adresse identifizierte Clients, welche als öffentlich Anzeigen dienen. 
            Deren Funktion kann überwacht werden. Rufe /ti/do/astatus auf!""")}
        </h3>\n''')
            statemachine = 1
        elif statemachine <= 1 and istconf and not istanzg:
            if statemachine > 0:  # der Vorgänger wurde genutzt
                md_response.append_s('</div><!--Kontext div zu-->\n') 
                md_response.next_abschnitt()
            md_response.append_s(clientverwaltungs_div_definition)
            md_response.append_s('<h3>Kontext: ' + 'verwaltete Clients' + '</h3>\n')
            statemachine = 2
        elif statemachine <= 2 and not istconf:
            if statemachine > 0:
                md_response.append_s('</div><!--Kontext div zu-->\n')  # ein Vorgänger wurde genutzt </div>
                md_response.next_abschnitt()
            md_response.append_s(clientverwaltungs_div_definition)
            md_response.append_s('<h3>Kontext: ' + 'unkonfigurierte Clients' + '</h3>\n')
            statemachine = 3  # final
        
        # Anfang einer Clientbeschreibung
        md_response.append_s(f'''
        <div style="float:luft;width:7em;padding-left:5px;
        background-color:blue;color:white;text-align:left"><b>{ip}</b></div>''')
        # Beginn des umfassenden DIVs um die Clientdetails
        md_response.append_s(f'''
        <div style="float:luft;margin-bottom:0.5em;padding:0;">
            <div style="float:left;">
                <form action="?task=config_clients" method="post" >
                    <input name="clientip" value="{ip}" type="hidden" />
                    <input name="newname" value="{name}" type="text" size="12" maxlength="16" class="eingabe" />
                    <input name="easid" value="{checkedsessionid}" type="hidden">
                    <input value="{_("Umbenennen")}" type="submit">
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Umbenennen', 
                """Jeder Client erhält beim ersten Zugriff seinen Hostnamen, 
                wenn er im DNS gefunden wird. Dieser kann geändert werden. 
                Er wird z.B. beim Anzeigenstatus angezeigt und bei über den Client 
                legitimierten Meldungen als Autor angegeben""")}
            </div>''')
        
        md_response.append_s(f'''
            <div style="float:left;">
                <form action="manage?task=config_clients" method="post" >&nbsp;
                    <input name="ip2remove" value="{ip}" type="hidden" />
                    <input name="easid" value="{checkedsessionid}" type="hidden" />
                    <input value="{_("Löschen")}" type="submit" 
                                    style="background-color:red;color:white;font-weight:bold;" />
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Client löschen', 
                """Der Client wird aus der Datenbank entfernt, 
                   bis er selber wieder auf das System zugreift.""")}
            </div>
        
            <div style="float:left;">       
                <form action="manage?task=config_clients" method="post" >&nbsp;
                    <input name="easid" value="{checkedsessionid}" type="hidden" />
                    <input name="cukopplung_ip" value="{ip}" type="hidden" />
                    <input value="Kopplung" type="submit" 
                    {'style="background-color:green;font-weight:bold;"' if cukopplung else ''} />
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Kopplung', 
                """Ein Anwender, der diesen Client zum Managen benutzt erhält die Rechte des 
                <i>gleichnamigen</i> Users - wenn einer existiert...<br><br>
                Funktioniert aus Sicherheitsgründen nur im ManagedNetwork!""")}
            </div>''')

        md_response.append_s('''
            <div style="float:left;padding:0px;">''')   
        warheute = ti.get_dayofepoche(timestamp) == ti.get_todays_dayofepoche()
        langher = ti.get_dayofepoche(timestamp) < ti.get_todays_dayofepoche() - 7
        if warheute:
            md_response.append_s('<span style="color:green;font-size:70%;font-weight:bold;line-height:60%;">')
        elif langher:
            md_response.append_s('<span style="color:grey;font-size:70%;line-height:60%;">')
        else:
            md_response.append_s('<span style="color:black;font-size:70%;line-height:60%;">')
        datum, leer, uhrzeit = ti.get_datestring_from_epoche(timestamp).rpartition(' ')
        md_response.append_s(f' &nbsp;{datum}<br>{uhrzeit}')
        md_response.append_s('</span>')
        md_response.append_s('''
            </div>''')
        
        ########################
        # zweite Zeile beginnen
        md_response.append_s('\n\t\t<!--Beginn 2. Zeile-->\n')
        md_response.append_s('''
            <div class="unfloatleft"></div>
            ''')
        if istanzg:
            gruppen |= 128
        md_response.append_s(config.htmloptionenzeile(
                gruppen, ti_ctx.cnf.get_gruppenbez, 
                'manage?task=config_clients', 'gruppentoggle', 'clientip', ip, 
                checkedsessionid))
        
        gruppennamenhilfe = 'Clientgruppen:<br><br>'
        for i in range(20):
            gn, gk = ti_ctx.cnf.get_gruppenbez(i)  # GruppenName, GruppenKürzel
            if gn:
                gruppennummer = 1 << i
                if gruppennummer == 64:
                    gruppennamenhilfe += '<br>Eigenschaften:<br>'
                if gruppennummer < 64:
                    gruppennamenhilfe += gk + ': ' + \
                                          ('Zeige Meldungen für ' if gruppen & gruppennummer 
                                           else 'Verberge Meldungen für ') + gn + '<br>'
                else:
                    gruppennamenhilfe += gk + ': ' + \
                                          ('Client ist vom Typ ' if gruppen & gruppennummer 
                                           else 'Client ist kein  ') + gn + '<br>'
        md_response.append_s('<div style="float:left;">')       
        md_response.append_s(config.popuphilfe('Aktueller Stand:', gruppennamenhilfe))
        md_response.append_s('\t</div>')
        
        # dritte Zeile für Anzeigeneinstellungen
        md_response.append_s('\n\t\t<!--Beginn 3. Zeile-->\n')
        if istanzg and not (gruppen & 256):
            mipha, mapha = ti_ctx.get_client_phaselimits(ip)
            md_response.append_s(f'''
            <div class="unfloatleft"></div>
            <div style="float:left;">
                <form action="manage?task=config_clients" method="post" >Anzeigebereich:
                    <input name="clientip" value="{ip}" type="hidden" />
                    <input name="minphase" value="{str(mipha)}" type="text" size="2" maxlength="2"  class="eingabe" /> bis
                    <input name="maxphase" value="{str(mapha)}" type="text" size="2" maxlength="2"  class="eingabe" />
                    <input name="easid" value="{checkedsessionid}" type="hidden"><input value="OK" type="submit">
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Anzeigebereich', """Der Bereich startet bei 0 für den Aushang.
                    Höhere Nummern stehen für die Pläne. 
                    Hiermit kann für diesen Client eingeschränkt werden, was er anzeigen soll.
                    Die angegebenen Grenzen sind inklusiv!
                    Passt ein folgender Plan noch mit auf die Seite des Bereichsendes, so wird er auch angezeigt!
                    Ist die erste Grenze negativ, so ist die Bereichsauswahl invertiert: 
                    -1 bis 1 heißt also alles außer 1 - perfekt für zwei Bildschirme, wo der erste immer 1, 
                    der andere den Rest dynamisch anzeigt.""")
                }
            </div>''')
            
        if gruppen & 256:  # vierte Zeile einfügen für Eingabe des Forward-Ziels (STATT 3. Zeile!)
            md_response.append_s('\n\t\t<!-- Beginn 4. Zeile-->\n')
            md_response.append_s(f'''
            <div class="unfloatleft"></div>
            <div style="float:left;">
                <form action="manage?task=config_clients" method="post" >
                    <input name="clientip" value="{ip}" type="hidden" />
                    <input name="forward" value="{forward}" type="text" 
                                                                  size="18" maxlength="255"  class="eingabe" />
                    <input name="easid" value="{checkedsessionid}" type="hidden">
                    <input value="Umleiten des Clients" type="submit">
                </form>
            </div><!-- Ende 4. Zeile-->\n''')
        
        # Ende des umfassenden DIVs um die Clientdetails
        md_response.append_s('''
        </div><!--Ende Clientdetails-->
        <div class="unfloatleft"></div>\n\n''')

    if statemachine == 0:
        md_response.append_s(clientverwaltungs_div_definition)
        md_response.append_s('      <h3 style="text-align:left;">Es sind noch keine Clients erkannt worden.</h3>\n')
    md_response.append_s('''
            </div><!-- Ende alle Clients -->''') 
    return md_response
    

def status_info(ti_ctx):
    tosay = []
    items2show = [('uploadtimestamp_klartext', 'Letzter Upload (Info o. V-plan)')]
    tosay.append('''<div style="float:left;
                                margin:0.5em;border:1;background-color:lightblue;
                                border-style:solid;border-color:blue;border-radius:5px;
                                padding:5px;text-align:right;">
                        <h3 style="text-align:left;">Status des Systems</h3>
                        <table>''')
    for parameter, beschreibung in items2show:
        wert = ti_ctx.cnf.get_db_config(parameter, '(nicht gesetzt)')  # muss ja nicht klappen
        tosay.append(statuszeile(beschreibung, wert))
        
    # handselektierte Stati:
    tosay.append(statuszeile('Aktueller User', ti_ctx.ses.get_session_user()))
       
    tosay.append('</table></div>')  # schließt Statusanzeige
    return '\n'.join(tosay)
    

def statuszeile(beschreibung, wert):
    return '<tr><td style="text-align:right">' \
            + beschreibung \
            + ':&nbsp;</td><td style="text-align:left"> ' \
            + str(wert) \
            + '</td></tr>'
    
    
def config_todo(ti_ctx):
    easid = ti_ctx.req.get_cgi('easid')
    ti_ctx.res.debug('easid', easid)
    if len(easid) > 5:
        if easid == ti_ctx.get_checkedsessionid():
            # Ab hier werden die Eingaben verarbeitet
            clip = ti_ctx.req.get_cgi('clientip')
            newname = ti_ctx.req.get_cgi('newname')
            if len(newname) > 0:
                ti_ctx.cnf.client_set_name(clip, newname)
                return '+ Gesetzt wurde als neuer Name für IP-Adresse ' + clip + ': ' + newname

            mipha = ti_ctx.req.get_cgi_int('minphase', 0)
            mapha = ti_ctx.req.get_cgi_int('maxphase', -1)
            if mapha >= 0:  # negative minphase sind nun erlaubt für invertieren des Bereichs
                if mipha > mapha:
                    mipha, mapha = mapha, mipha
                ti_ctx.cnf.client_set_phaselimits(clip, mipha, mapha)
                return '+ Anzeigebereich des Clients ' + clip + ' geändert von ' + str(mipha) + ' bis ' + str(mapha)
                
            gruppentoggle = ti_ctx.req.get_cgi_int('gruppentoggle')
            if (gruppentoggle > 0 and gruppentoggle < 65) or \
                    gruppentoggle in [256, 512, 1024, 2048]:
                # bei den großen immer nur ein Bit toggeln
                gruppen = ti_ctx.cnf.client_get_gruppen(clip)
                gruppen = gruppen ^ gruppentoggle  # xor
                ti_ctx.cnf.client_set_gruppen(clip, gruppen)
                return '+ Gruppenzugehörigkeit des Clients ' + clip + ' geändert'
                
            if gruppentoggle == 128:
                # Gibt an, ob der Client ein zu überwachender AnzeigePC ist
                istanzg = not ti_ctx.cnf.client_get_istanzg(clip)
                ti_ctx.cnf.client_set_istanzg(clip, istanzg)
                return '+ Client ' + clip + ' ist nun ' + ("k" if not istanzg else "") + 'eine zu überwachende Anzeige'
                
            cukopplung_ip = ti_ctx.req.get_cgi('cukopplung_ip')
            if len(cukopplung_ip) > 4: 
                cukopplung = not ti_ctx.cnf.client_get_cukopplung(cukopplung_ip)
                ti_ctx.cnf.client_set_cukopplung(cukopplung_ip, cukopplung)
                name = ti_ctx.cnf.client_get_name(cukopplung_ip)
                return f'''+ Client {cukopplung_ip} ist 
                        {("nicht mehr " if not cukopplung else "")} mit {name} gekoppelt'''
                
            ip2remove = ti_ctx.req.get_cgi('ip2remove')
            if len(ip2remove) > 0:
                ti_ctx.cnf.client_remove(ip2remove)
                return '+ Der Client mit der IP-Adresse ' + ip2remove + ' wurde aus der Clientliste entfernt'
                
            fwd = ti_ctx.req.get_cgi('forward', 'invalid')
            if fwd != 'invalid':
                ti_ctx.cnf.client_set_forward(clip, fwd)
                return '+ Der Client ' + clip + ' wird zukünftig auf "' + fwd + '" umgeleitet'
            ti_ctx.cnf.client_set_forward(clip, '')
            return '+ Kein Forward-Ziel definiert'

            # Abbruch
        else:
            return '-Falsche Session-ID! Nicht aus dem Browsercache aufrufen...'
    
    return ''  # wurde gar nicht mit Formular aufgerufen
    

HTML_TEMPLATE = '''
    <div class="unfloatleft"></div>
    <div><p>Anleitung:</p>
        <p>Der Name wird im ManagementNetwork als Verfassername für Meldungen verwendet, 
            sofern keine Anmeldung stattfand. Außerdem kann ein Client mit einem Useraccount
            ge<b>koppelt</b> werden, wenn sie a) denselben Namen haben und b) der Client im 
            ManagementNetwork liegt. Dann erhält dieser Client sowohl die Rechte des ManagementNetworks
            als auch die Rechte des gleichnamigen Users.</p>
        <p>Die Clienttypen geben wieder, ob zielspezifische Zusatzinfos eingeblendet werden 
            sollen, z.B. für Oberstufe, Lehrerzimmer usw.. 
            Einige dieser Gruppen können mit eigenen Namen versehen werden (-> Einstellungen) und 
            dann konsistent in dem Meldungseditor und auf den Anzeigen verwendet werden.</p>
        <p>Ein Client kann zu vielen Typen gehören, was aber der Übersicht in der Anzeige nicht
            dienlich ist...</p>
        <p>Ist ein Client eine Anzeige (<b>Anzg</b>), so kann sein regelmäßiger Datenabruf 
            mit /ti/do/astatus überprüft werden.</p>
        <p>Wichtig: Eine Clientkonfiguration ist nur sinnvoll, wenn die betroffenen Clients 
            <b>feste IP-Adressen</b> haben!</p>
        <p>Alternative: Starten Sie auf dem Clientbrowser die Seite
            http://tabulaserver/ti/do/frames?setclientgroups=32;sc_code={sc_code}</p>
        <p>Damit wird ein Lehrerclient simuliert. Der Code ist zufällig generiert und spezifisch 
            für diese Installation. Mit setclientgroups=1 bzw. 2 bzw. 3 wird Q11 bzw. Q12 bzw.
            beide angesteuert usw.</p>
    </div>
    {stati}
'''


def do_config(ti_ctx):
    sc_code = ti_ctx.cnf.get_db_config_int('sc_code')
    if sc_code < 100001:
        import random
        ti_ctx.cnf.set_db_config('sc_code', str(int(100000 + random.random()*900000)))
        sc_code = ti_ctx.cnf.get_db_config_int('sc_code')
    erfolg = config_todo(ti_ctx)
    ti_ctx.res.debug("Erfolg", erfolg)
    if erfolg:
        ti_ctx.set_erfolgsmeldung(erfolg)
    md_r = config_dialog(ti_ctx)
    md_r.next_abschnitt()
    md_r.append_s(HTML_TEMPLATE.format(sc_code=sc_code, stati=status_info(ti_ctx)))
    return md_r


if __name__ == '__main__':
    ti_lib.quickabort2frames()
