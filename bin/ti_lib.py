#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Batteries (included)
import sys
import os
import traceback
import ast
import pwd
import time
import hashlib
import configparser
import gettext
from pathlib import Path

# TI-Import
import konst

# Mutter der Porzellankiste
panic_template = '''Content-Type: text/html\n
<!DOCTYPE html>\n<head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>body, h3, p, pre{{ background:#505;color:#220}}
           pre {{color:grey}}
           h3, p {{color:#FF0}}
           .meldung {{color:white; font="sans";}}
    </style>
</head>
<body>
    <br><h3>Fatal Error / Nicht behebbarer Fehler:</h3>
    <p>{}</p><br>
    <pre class="meldung">{}</pre>
    <pre class="meldung">{}</pre>
    <pre class="meldung">{}</pre>
    <h3>{}</h3><br>
    <pre>{}</pre>
</body>'''


def panic(text, ausnahme, details=''):
    exinfo0 = str(sys.exc_info()[0]).replace('<', '&lt;').replace('>', '&gt;')
    tb = (traceback.format_exc()+'\n ').replace('<', '&lt;').replace('>', '&gt;').split("\n", 1)
    ausgabe = panic_template.format(text, str(ausnahme), exinfo0, details, tb[0], ''.join(tb[1]))
    sys.stderr.write(ausgabe)
    sys.stderr.flush()
    print(ausgabe)
    exit(1)


'''
________________________________________________________________________
 ti_lib.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_lib.py kapselt u.a. die Zugriffe auf die Konfigurationsdatenbank.
 Die Zugriffe werden von allen Modulen an ti.py gerichtet, welches sie 
 durchreicht und dann z.B. in int-werte umwandelt.
 
 ti_lib.py kann auch von der Kommandozeile/ Shellskripten aufgerufen werden.

 Es werde bewusst keine anderen ti-Module außer config_db aufgerufen!
________________________________________________________________________
'''


class S_any_vplan():
    def __init__(self, epoche, ziel, phase, spalten, url, dauer, weite=0, qf=False):
        self.epoche = int(epoche)
        self.ziel = ziel
        self.phase = phase
        self.spalten = spalten
        self.url = url
        self.dauer = dauer
        self.weite = weite
        self.qf = qf
   

class Multi_Detail_Response():
    """ Enthält die Rückgabewerte einer Routine, die mehrere Strings und ggf. eine Zahl und einen Bool-Wert liefert 
    
        man.py ruft Routinen für einzelne Aufgaben auf. Diese geben hiermit ihren i.d.R. HTML-Output zurück
    """
    def __init__(self, first_html_list=None, second_html_list=None, third_html_list=None, cancel_flag=False):
        """ die _html_listen enthalten Listen von Strings, welche die auszugebenden HTML-Codes enthalten
            extra_number kann eine Zahl enthalten
            cancel_flag gibt an, dass die Ausführung erfolglos beendet wurde.
        """
        self.html_list = [[], [], [], [], []]
        self.html_list[0] = [] if first_html_list is None else first_html_list 
        self.html_list[1] = [] if second_html_list is None else second_html_list
        self.html_list[2] = [] if third_html_list is None else third_html_list

        self.submenu_finished = False   # kann angeben, dass der Menüpunkt abgehandelt ist und nichts mehr darstellen will
        
        self.cancel_flag = cancel_flag
        self.sessionid = 0     # wird nur von login gesetzt, aber immer Richtung wsgi durchgereicht

        self._abschnitt = 0    # in welchen der Abschnitte sollen appends ohne Abschnittnummer eintragen
        
    def __repr__(self):
        return f"Multi_Detail_Response({self.html_list[0]}, {self.html_list[1]}," \
            f" {self.html_list[2]}, {self.cancel_flag})"
        
    def append_s(self, html_line, abschnitt=-1):
        if abschnitt <= 0 or abschnitt > 4:
            abschnitt = self._abschnitt
        self.html_list[abschnitt].append(html_line)
        
    def append_l(self, html_list, abschnitt=-1):
        if abschnitt <= 0 or abschnitt > 4:
            abschnitt = self._abschnitt
        self.html_list[abschnitt] += html_list
    
    def next_abschnitt(self):
        self._abschnitt += 1
    
    def ist_erfolg(self):
        return not self.cancel_flag

    def ist_abgebrochen(self):
        return self.cancel_flag

    def setze_abgebrochen(self):
        self.cancel_flag = True

    def setze_erfolg(self):
        self.cancel_flag = False

    def abschnitt_to_string(self, abschnitt):
        return '\n'.join(self.html_list[abschnitt])
    
    def abschnitt_max(self):
        for i in range(4, 0, -1):
            if len(self.html_list[i]) > 0:
                return i
        return -1
            

def isodatum_heute_plus_delta(delta):
    _timestamp_ = time.localtime(time.time())
    return ts_2_isodatum(ts_verschiebe(_timestamp_, delta))
    

def epoche_2_isodatum(e):
    return ts_2_isodatum(time.localtime(e))
    

def ts_2_isodatum(ts):
    return time.strftime('%Y-%m-%d', ts)


def ts_verschiebe(ts, delta_tage):
    return time.localtime(time.mktime((ts.tm_year, ts.tm_mon, ts.tm_mday+delta_tage, 0, 0, 0, 0, 0, -1)))


def ist_isodatum(datum):
    if len(datum) == 10 and \
            datum[:2] == '20' and  \
            datum[4] == '-' and  \
            datum[7] == '-' and  \
            datum[2:4].isdigit() and \
            datum[5:7].isdigit() and \
            datum[8:10].isdigit() and \
            int(datum[5:7]) < 13 and \
            int(datum[8:10]) < 32:
        return True
    return False
    

def strg2int(value, default=0):
    try:
        return int(ast.literal_eval(value))
    except Exception:
        return default
        

def strg2bool(value, default=False):
    if len(value) == 0:
        return default
    try:
        return (int(ast.literal_eval(value)) != 0)
    except Exception:
        if value[0] in "YyJj1Tt": 
            return True
        if value[0] in "Nn0Ff": 
            return False
    return default
    
# # basepath: Ort des bin-Verzeichnisses, also parent des Ortes, wo diese Datei liegt. Der
# #           Rest basiert darauf


def get_ti_basepath(parameter=''):
    """gibt pathlib.Path-Objekt zurück"""
    return __ti_basepath__ / parameter


def get_ti_binpath():  # nach Definition...
    """gibt pathlib.Path-Objekt zurück"""
    return __ti_basepath__ / 'bin'


def get_ti_datapath(parameter=''):
    """gibt pathlib.Path-Objekt zurück"""
    return __ti_datapath__ / parameter


def get_ti_dynpath(parameter=''):
    """gibt pathlib.Path-Objekt zurück"""
    return __ti_dynpath__ / parameter 

    
def get_ti_toolpath(parameter=''):
    """gibt pathlib.Path-Objekt zurück"""
    return Path(konst.TOOLPATH) / parameter

    
def prt(*args):
    for data in args:
        data8 = str(data).encode('utf-8')
        try:
            sys.stdout.buffer.write(data8)
        except Exception:
            sys.stdout.write(data8)
        

def get_now():  # liefert unix-epoche
    return time.time()  # sec since epoche


def get_now4logs():
    return time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime(get_now()))    
  

def get_akt_zeit():
    _timestamp_ = time.localtime(time.time())
    return _timestamp_.tm_hour*100+_timestamp_.tm_min
    

def beschreibe_exception(e):
    # t = 'Fehlermeldung: '+str(e)+'\n\n'
    # t += traceback.format_exc()
    # exc_type, exc_value, exc_traceback = sys.exc_info()
    # formatted_lines = traceback.format_exc().splitlines()
    # t += '\n'.join(formatted_lines)
    return '{}: {}\n\nTraceback:\n{}'.format(_('Fehlermeldung'), str(e), traceback.format_exc())
    

def reset_log():
    log('\n'+'#'*73+'\n# '+_('Dieses Logfile wird hiermit abgeschlossen, umbenannt und neu angelegt')+' #\n'+'#'*73)
    try:
        __log_old_path__.unlink()
    except Exception:
        pass
    __logfile_path__.rename(__log_old_path__)
    log(_('Neues Logfile nach Reset'))


def log(*args):
    items = []
    for i in args:
        items.append(str(i))
    try:
        with __logfile_path__.open(mode='a', encoding='utf-8') as fout:
            fout.write(time.strftime('%d %b %Y %H:%M:%S', time.localtime())+': '+' '.join(items)+'\n')
    except Exception as e:
        panic("ti_lib failed on writing logfile", e)


def get_akt_hour():
    _now_ = time.time()  # sec since epoche
    return time.localtime(_now_).tm_hour
    

def berechne_md5_von(filepath):
    md5_hash = hashlib.md5()
    with filepath.open("rb") as f:
        # Read and update hash in chunks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            md5_hash.update(byte_block)
        return md5_hash.hexdigest()
    
##########################################
# standardhtml-head erzeugen


def standardcss(ismanagement=False, count_subdirectories=1, til=''):
    the_end = "backend" if ismanagement else "frontend" 
    all_subs = '../' * count_subdirectories
    if not til:
        til = int(time.time() / 1000)  # ca. die 20-min-Intervalle seit 1970... Ändert sich also alle ca. 20 min

    if (get_ti_dynpath() / f'../local/{the_end}.css').is_file():
        localcss = f'<link rel="stylesheet" type="text/css" href="{all_subs}local/{the_end}.css?q={til}">'
    else:
        localcss = ''

    return f'''
    <link rel="stylesheet" type="text/css" href="{all_subs}static/css/all_end.css?q={til}">
    <link rel="stylesheet" type="text/css" href="{all_subs}static/css/{the_end}.css?q={til}">
    {localcss}
    ''' 


def write_ti_ini(datapath, ti_ini):
    """Schreibe eine ini-Datei in das Datenverzeichnis
    
    ti_ini muss vom Typ ConfigParser sein."""
    with (datapath / 'ti.ini').open(mode='w') as inifile:
        ti_ini.write(inifile)


def read_ti_ini(datapath):
    """Hier wird die ini-Datei eingelesen - zeitlich deutlich vor sqlite-Configs"""
    ti_ini = configparser.ConfigParser()
    ti_ini.read(datapath / 'ti.ini')
    # eigentlich fertig. Nun noch Spezialfälle wie migration
    if not ti_ini['DEFAULT']:
        ti_ini['DEFAULT'] = {'locale': ''}
        print("default neu angelegt")
        write_ti_ini(datapath, ti_ini)
    if not ti_ini['DEFAULT'].get('locale'):
        thelocale = "#undefined"
        try:
            with (datapath / 'locale.conf').open(mode='r', encoding='utf-8') as localedatei:
                while thelocale and not thelocale[0].isalpha():
                    thelocale = localedatei.readline().strip()
            if thelocale and thelocale[0].isalpha():
                ti_ini['DEFAULT']['locale'] = thelocale
            (datapath / 'locale.conf').unlink()
        except Exception:
            ti_ini['DEFAULT']['locale'] = 'de_DE'
        write_ti_ini(datapath, ti_ini)
    return ti_ini
    

def save_filename(name):
    if "\\" in name:  # grandioser Workaround, falls der Filename kompletten Windowspfad enthält (alter IE?)
        fn = name.split("\\")[-1]
        if fn:
            name = fn
        else:  # trailing backslash? wo kommt der her?
            name = name.split("\\")[-2]
    return ''.join([x for x in list(name) if (x.isdigit() or x.isalpha() or x in '_-.')])
    

def quickabort2frames():
    """Es wird nur ein kurzer Redirect zur eigentlichen Ausgabe gemacht.
        Zweck: Wenn ein Modul fälschlicherweise per CGI aufgerufen wird..."""
    prt('''Content-Type: text/html\n\n<!DOCTYPE html>
    <head>
        <meta http-equiv="refresh" content="1; URL=/ti/do/frames">
    </head>
    <body>
        Einen Moment bitte - dies ist die falsche Adresse :-)
    </body>''')
    exit()


def quickabort2management():
    """Es wird nur ein kurzer Redirect zur eigentlichen Ausgabe gemacht.
        Zweck: Wenn ein Modul fälschlicherweise per CGI aufgerufen wird..."""
    prt('''Content-Type: text/html\n\n<!DOCTYPE html>
    <head>
        <meta http-equiv="refresh" content="1; URL=/ti/do/manage">
    </head>
    <body>
        Einen Moment bitte - dies ist die falsche Adresse :-)
    </body>''')
    exit()


try:
    # ##
    # START
    # #
    if sys.version_info < (3, 7):
        panic('tabula.info benötigt eine Pythonverson ab 3.7!', 
              BaseException("Python version too old. 3.7 or newer required."))

    # ### quasi Konstanten, da nur bei neuem Programmstart mit anderen Werten möglich
    # # Zuerst absoluten Pfad die tabula.info-Installation feststellen
    # basepath sollte das Verzeichnis, in dem cgi-bin liegt, wiedergeben, z.B. /opt/tabula.info/tabula2.
    
    __ti_basepath__ = Path(__file__).absolute().parent.parent  # nur so per wsgi & cgi korrekt
    __ti_localpath__ = (__ti_basepath__ / '..' / 'local').resolve()  # kann für multihoming dynamisch werden
    __ti_datapath__ = __ti_localpath__ / 'data'
    __ti_dynpath__ = __ti_localpath__ / 'www' / 'dyn'
    __username__ = pwd.getpwuid(os.getuid())[0]
    if __username__ == 'www-data':
        __logfile__ = 'background.log'
    else:
        __logfile__ = __username__ + '.log'
    __logfile_path__ = __ti_datapath__ / 'log' / __logfile__
    __log_old_path__ = __ti_datapath__ / 'log' / (__logfile__ + '.old')
                    
    ti_ini = read_ti_ini(__ti_datapath__)
    
    thelocale = ti_ini['DEFAULT']['locale']
    if thelocale and thelocale != 'de_DE':
        try:
            translate = gettext.translation('tabula', __ti_basepath__ / 'locales',
                                            languages=[thelocale], fallback=False)
        except Exception:
            sys.stderr.write('\nti_lib.py: locale NOT set to '+thelocale+'\n')
            sys.stderr.flush()
            translate = gettext.translation('tabula', __ti_basepath__ / 'locales', 
                                            languages=['de_DE'], fallback=True)  # fallback ist das relevante...
    else:
        translate = gettext.translation('tabula', __ti_basepath__ / 'locales',
                                        languages=['de_DE'], fallback=True)  # fallback ist das relevante...
    translate.install()

    for pfad in ['upload', 'work']:
        if not (__ti_datapath__ / pfad).exists():
            (__ti_datapath__ / pfad).mkdir()
    
except Exception as e:
    panic("ti_lib failed initializing", e)

if __name__ == '__main__':
    quickabort2frames()
