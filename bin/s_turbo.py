#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_turbo.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo.py erzeugt eine tabellarische Wiedergabe des Vertretungsplans
 in verschiedenen Formatierungen - je nach Parameter
________________________________________________________________________

 Sollten Sie eine veränderte Fassung dieses Skript an der abc-Schule verwenden, so geben Sie
    ihm am Besten einen neuen Namen wie s_abc.py. Dann haben Sie mit zukünftigen Updates 
    keine Probleme.
________________________________________________________________________
'''
# Batteries
import string
import codecs
import time
import sys, csv
# TI
import ti, ti_lib
from s_turbo_import import schedule_obj, read_table, AUSSCHLUESSE_BEI_REDUZIERT, get_schedule_obj
from s_turbo_def import S_Schedule, S_Day, S_Row, debug

    
# !! diese Datei wird nicht internationalisiert, da kga-spezifisch !!
 

    
def get_messages_barebone(ti_ctx, preview=False, tofile=False):
    if  preview:
        morestyles = '<style type="text/css">body { background-color:#F00; }</style>\n'
    else:
        morestyles = ""
    return ti.get_html_head(ti_ctx, title='tabula.info - Schedule', extrastyle=morestyles, tofile=tofile, includesearch=True)
    
def iterate_rows(sched_obj, day_obj, first_row=0, last_row=199, header_txt="", table_background="", reduced=False):
    def add_comment(level=0):
        comment = day_obj.get_comment(level)
        comment_list = comment.split('<!--feed--><BR>')
        r_strg = ''
        for cc1 in comment_list:
            if reduced and not level: # Aussortierregel nur bei echten Kommentaren
                cc = ''
                for cl in cc1.split('<BR>'):
                    for killstring in AUSSCHLUESSE_BEI_REDUZIERT:
                        if killstring in cl.lower():
                            cl = ''
                    if len(cl)>0:
                        if len(cc)>0:
                            cc += '<BR>'
                        cc += cl
            else:
                cc = cc1
            r_strg += '\t<tr><td colspan="9" style="border-top-width:3px;">'+cc+'</td></tr>\n'
        return r_strg
        
    #global reduced

    class_array = ["", "s_t_fresh", "s_t_ausfall", "s_t_gelb", "s_t_gruen"]
    i_r_strg = ''
    if len(header_txt) == 0: # Länge 0: Automatisch erzeugen, Länge 1: unverändert (geplant: ein Leerzeichen)
        header_txt = day_obj.get_datestring()
    if len(header_txt)>1 and first_row == 0:
        i_r_strg += '<div class="firstcontent"><h1>Vertretungen {0}</h1></div>'.format(header_txt)
    max_hour = 99
    try:
        akthour = ti.get_akt_hour(day_obj.get_date()) ##????
        table_class = "" if table_background == "" else ' class="'+table_background+'"'
        i_r_strg += '\n<table '+table_class+' border="0" style="border-collapse:collapse">\n' 
        level =  1 if reduced else 0
        
        fatborder = False
        row_count = 0
        if debugging: debug("Iteriere Rows für:", str(day_obj.get_date())+" AH:"+str(akthour)+" FR:"+str(first_row)+" LR:"+str(last_row)+" AH:"+str(akthour))
        display_hour = 1
        if not first_row:  # damit der Kommentar bei mehreren Spalten nur bei der ersten erscheint
            i_r_strg += add_comment(1)
        for row_obj in day_obj.rows:
            if debugging: debug("IR: The Row", row_obj)
            row_hour = row_obj.get_expire()
            while row_hour>display_hour:
                display_hour += 1
                if debugging: debug("IR: Display_Hour", display_hour)
                if display_hour in [3, 5, 7, 10]:
                    fatborder = True
            try:
                if row_count >= first_row and row_count <= last_row and display_hour<max_hour:
                    row = row_obj.get_cells()
                    if debugging: debug("Zeige Zellen", str(row))
                    i_r_strg += '\t<tr>'
                    newstyle = ''
                    newclass = ''
                    if display_hour <= akthour:
                        newstyle += 'font-size:52%;'
                    elif row_obj.get_highlight():
                        newclass = ' class="'+class_array[row_obj.get_highlight()]+'" '
                    row[8] = row[8].split('**')[0]
                    if fatborder:
                        fatborder = False
                        newstyle += 'border-top-width:3px;'
                    tdstr = '<td '+newclass
                    if len(newstyle): tdstr += ' style="'+newstyle+'" '
                    for i in range(len(row)):
                        if sched_obj.get_column_display(level, i):
                            i_r_strg += tdstr+sched_obj.get_column_style(level, i)+'>'+str(row[i])+'</td>'
                        # else: # Vorsicht: Verrät zu viel!
                        #   i_r_strg += '<!-- '+str(row[i])+' -->'
                    i_r_strg += '</tr>\n'
                row_count += 1
            except:
                if debugging: debug("Fehler bei iterate_row_objs bei display_hour", display_hour)
                if debugging: debug("Exception:", sys.exc_info()) 
        # jetzt sind alle rows durchgelaufen
        if row_count <= last_row and last_row == 199: #also nur am Ende der Anzeigenfolge
            i_r_strg += add_comment(0)
        i_r_strg += '\t<tr><td colspan="9" style="border-top-width:3px;font-size:0;"></td></tr>\n'
        i_r_strg += '</table>\n'
        # End of iterating days 
    except():
        if debugging: debug("Fehler bei iterate_rows", first_row)
        if debugging: debug("Exception:", sys.exc_info())
    
        ti.prt("<br><h2>File I/O-Fehler?</h2>Sorry, I can't create Messages in html...<br>")
        #FIXME??? if debugging: ti.ti_res.debug_flush()
    return i_r_strg

class TurboExportDatei(): # soll in einem Array mehrfach übergeben werden
    def __init__(self, html, columns, htmlex=''):
        self.html = html
        self.columns = columns
        self.htmlex = htmlex
    

class S_Interface():
    

    columnmode = 0
    schedule_txt = ""
    schedule_txt_export = ""
    rows = [0, 0, 0, 0, 0]
    
    def __init__(self, ti_cnf):
        self.ti_cnf = ti_cnf
    
    def analyse_schedule(self, phase, phaselimit):
        read_table()
        first5days = schedule_obj.get_first5days()
        # dummy, soloscreen, dummy2 = self.client.groups()
        soloscreen = 0  # # Festgelegt, bis Zukunft geklärt. FIXME
        debug("first5days anzahl", len(first5days))   
        for i in range(len(first5days)):
            debug("first5days["+str(i)+"]", str(first5days[i].get_datestring()))
        # ## Phase klären ###
        # Ist zu dem Tag überhaupt noch ein Plan auszugeben?
        if phase<phaselimit and phase<len(first5days):
            # leere Tage überspringen
            while first5days[phase].get_rowcount() == 0 and phase<phaselimit:
                phase += 1
                if phase >= len(first5days):
                    phase = phaselimit
        else:
            phase = phaselimit

        nextphase = phase+1 if phase<phaselimit else 0 
        if debugging: debug("Turbo-Analyse: arbeite mit Phase", phase)
        if phase<phaselimit:
            for tageszaehler in range(5):
                if len(first5days)>tageszaehler+phase:
                    self.rows[tageszaehler] = first5days[phase+tageszaehler].get_rowcount()
                else:
                    self.rows[tageszaehler] = 0
            maxrows = self.ti_cnf.get_db_config_int("s_turbo_maxRowsInSchedule", 26)
            if soloscreen:  # Spezialität für Sekretariat
                maxrows = maxrows-3
            # verkleinern bei extrem langem Vertretungsplan
            if self.rows[0]>2*maxrows:
                self.columnmode = 2 if self.rows[0]<2.5*maxrows else 3 # biec 2018
                if debugging: debug("Turbo-Analyse: lang - muss also dreispaltig sein")
                self.schedule_txt = self.create_schedule_html(self.columnmode, phase=phase)    # mit divs
                for i in range(1, 5):
                    self.rows[i] = 0  # löschte aktuelle und zukünftige row-Anzahlen => nicht anzuzeigen
                return phase, nextphase
            if self.rows[0]>maxrows:
                self.columnmode = 2
                if debugging: debug("Turbo-Analyse: recht lang - muss also zweispaltig sein")
                self.schedule_txt = self.create_schedule_html(self.columnmode, phase=phase)    # mit divs
                for i in range(1, 5):
                    self.rows[i] = 0  # löschte aktuelle und zukünftige row-Anzahlen => nicht anzuzeigen
                return phase, nextphase

            # aha, einspaltig möglich - was passt noch dazu?
            if debugging: debug("Turbo-Analyse: kurz - dieser Tag wird nur einspaltig sein")
            # produziere ersten Vertretungsplan (ohne divs drum herum - passiert am Ende der Funktion)
            self.columnmode = 1
            leftrows = self.rows[0]+2
            schedule_txt_left = self.create_schedule_html(-1, phase=phase)+'<br>'
            rightrows = 0
            schedule_txt_right = ''
            # Nun prüfe, ob noch einer auf den Bildschirm kann
            if phase+1<len(first5days):
                if debugging: debug("Turbo-Analyse: teste nächsten Tag")
        
                # passt vielleicht noch einer dazu?
                for tageszaehler in range(1, 5):
                    # Sonderbehandlung Tag 1: er darf noch links dazu - wenn er passt:
                    if tageszaehler == 1 and 0<self.rows[1] and self.rows[1]+leftrows <= maxrows:
                        if debugging: debug("Turbo-Analyse: Respekt, noch ein Tag Nr."+str(tageszaehler)+" passt LINKS rein")
                        schedule_txt_left += self.create_schedule_html(-1, phase=phase+1) # schreibe weiteren Tag nach links
                        nextphase += 1                                    # überspringe ihn beim nächsten Mal
                    # Teste Tage für die rechte Seite
                    elif not soloscreen and (0<self.rows[tageszaehler] and rightrows+self.rows[tageszaehler] <= maxrows):             # darf & passt er rechts rein?
                        if debugging: debug("Turbo-Analyse: Respekt, noch ein Tag Nr."+str(tageszaehler)+" passt rein")
                        schedule_txt_right += self.create_schedule_html(-1, phase=phase+tageszaehler)+'<br>' # lese weiteren Tag ein
                        nextphase += 1                                # und überspringe ihn beim nächsten Mal
                        self.columnmode = 2
                        rightrows += self.rows[tageszaehler]+2
                    else:
                        if debugging: debug("Turbo-Analyse: nein, kein Tag Nr."+str(tageszaehler))
                        if debugging: debug("Turbo-Analyse: Grund:tageszaehler, rows, rightrows, maxrows"+str(tageszaehler)+', '+str(self.rows)+', '+str(rightrows)+', '+str(maxrows))
                        
                        for i in range(tageszaehler, 5):
                            self.rows[i] = 0  # löschte aktuelle und zukünftige row-Anzahlen => nicht anzuzeigen
                        break      # beendet die tageszaehler-Schleife!                 
                    
                self.schedule_txt += '<div id="column1_2">'+schedule_txt_left+'</div>'
                self.schedule_txt += '<div id="column2_2">'+schedule_txt_right+'</div>'
            else:
                if debugging: debug("Turbo-Analyse: passt kein Tag mehr dazu")
                self.schedule_txt += '<div id="column1_2">'+schedule_txt_left+'</div>'
                for i in range(1, 5):
                    self.rows[i] = 0 # löschte aktuelle und zukünftige row-Anzahlen => nicht anzuzeigen
                
        
        return phase, nextphase, self.columnmode
    ## End of analyse_schedule

    def export_any(self): # liefert null bis drei HTML-Exporte (in div einzubinden) als Strings sowie deren jeweilige Spaltenzahlen (0-2) zurück
        #read_table()
        ti_lib.log('s_turbo_exp_any: start')
        ted_array = []  # wird die resultierenden HTML-Fragmente in Form von TurboExportDateien enthalten
        first5days = schedule_obj.get_first5days()
        for tageszaehler in range(5):
            if len(first5days)>tageszaehler:
                self.rows[tageszaehler] = first5days[tageszaehler].get_rowcount()
            else:
                self.rows[tageszaehler] = 0
        maxrows = self.ti_cnf.get_db_config_int("s_turbo_maxRowsInSchedule", 26)
        phase = 0
        ti_lib.log('s_turbo_exp_any: len f 5 d', len(first5days))
        while phase<len(first5days):
            # verkleinern bei extrem langem Vertretungsplan
            if self.rows[phase]>2*maxrows:
                self.columnmode = 3
                self.schedule_txt, self.schedule_txt_export = self.create_schedule_html(self.columnmode, phase=phase)    #mit divs
                phase += 1  # da erledigt
            elif self.rows[phase]>maxrows:
                self.columnmode = 2
                self.schedule_txt, self.schedule_txt_export = self.create_schedule_html(self.columnmode, phase=phase)    #mit divs
                phase += 1  # da erledigt
            else:      # aha, einspaltig möglich - was passt noch dazu?
                if debugging: debug("Turbo-Analyse: kurz - dieser Tag wird nur einspaltig sein", phase)
                #produziere ersten Vertretungsplan (ohne divs drum herum - passiert am Ende der Funktion)
                self.columnmode = 1
                leftrows = self.rows[phase]+2
                schedule_txt_left, schedule_txt_left_export = self.create_schedule_html(-1, phase=phase)
                rightrows = 0
                schedule_txt_right, schedule_txt_right_export = '', ''
                phase += 1  # da erledigt
                while phase<len(first5days) and leftrows+self.rows[phase]<maxrows: # solange noch einer auf dieselbe Spalte kann
                    c_s , c_s_e  = self.create_schedule_html(-1, phase=phase)
                    schedule_txt_left += '<br>'+c_s
                    schedule_txt_left_export += '<br>'+c_s_e
                    leftrows += self.rows[phase]+1
                    phase += 1  # da erledigt
                while phase<len(first5days) and rightrows+self.rows[phase]<maxrows : # solange noch einer auf dieselbe Spalte kann
                    if self.columnmode == 2:
                        schedule_txt_right += '<br>'
                        schedule_txt_right_export += '<br>'
                    c_s , c_s_e = self.create_schedule_html(-1, phase=phase)
                    schedule_txt_right += c_s
                    schedule_txt_right_export += c_s_e
                    self.columnmode = 2
                    rightrows += self.rows[phase]+2
                    phase += 1  # da erledigt
                if self.columnmode == 1:
                    self.schedule_txt += schedule_txt_left
                    self.schedule_txt_export += schedule_txt_left_export
                elif self.columnmode == 2:        
                    self.schedule_txt += '<div id="column1_2">'+schedule_txt_left+'</div>'
                    self.schedule_txt += '<div id="column2_2">'+schedule_txt_right+'</div>'

                    self.schedule_txt_export += '<div id="column1_2">'+schedule_txt_left_export+'</div>'
                    self.schedule_txt_export += '<div id="column2_2">'+schedule_txt_right_export+'</div>'

            ted_array.append( TurboExportDatei(self.schedule_txt, self.columnmode, self.schedule_txt_export))
            # zurücksetzen:
            self.schedule_txt, self.schedule_txt_export = '', ''
            self.columnmode = 0
            # phase steht weiter für den nächsten zu verwendenden Tag
        return ted_array
        ## End of export_any
    
    def create_schedule_html(self, columns, phase, table_background=""):
    
        read_table() # Evtl. bereits geschehen - wird dort abgefangen!
        maxrows = self.ti_cnf.get_db_config_int("s_turbo_maxRowsInSchedule", 26)
        schedulestring = ''
        schedulestring_export = ''
        if table_background == "":
            table_background = "ph"+ ( str(phase) if phase<3 else "n")
        first5days = schedule_obj.get_first5days()
        
        if columns == -1:  # einspaltig für einen Tag, gedacht als halbe Breite, div muss anderweitig dran kommen!
                schedulestring += iterate_rows(schedule_obj, first5days[phase], table_background=table_background)
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase], table_background=table_background, reduced=True)
        elif columns == 1:  # linke Spalte für einen Tag, halbe Breite
                schedulestring += '<div id="column1_2">' 
                schedulestring += iterate_rows(schedule_obj, first5days[phase], table_background=table_background)
                schedulestring += '</div>'
                
                schedulestring_export += '<div id="column1_2">' 
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase], 
                                                      table_background=table_background, reduced=True)
                schedulestring_export += '</div>'
                
        elif columns == 2: # zweispaltig für einen Tag, Bildschirm halbiert
            
                # linke Spalte   
                schedulestring += '\n<div id="column1_2">\n'
                schedulestring += iterate_rows(schedule_obj, first5days[phase], 
                                               last_row=first5days[phase].get_rowmedian(), 
                                               table_background=table_background)
                # rechte spalte
                schedulestring += '\n</div>\n<div id="column2_2">\n<h1>&nbsp;<small><small>(Fortsetzung)</small></small></h1>\n'
                schedulestring += iterate_rows(schedule_obj, first5days[phase],
                                               first_row=first5days[phase].get_rowmedian()+1,
                                               table_background=table_background)
                schedulestring += '\n</div>\n'
                
                # linke Spalte   
                schedulestring_export += '\n<div id="column1_2">\n'
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase],
                                                      last_row=first5days[phase].get_rowmedian(),
                                                      table_background=table_background, reduced=True)
                # rechte spalte
                schedulestring_export += '\n</div>\n<div id="column2_2">\n<h1>&nbsp;<small><small>(Fortsetzung)</small></small></h1>\n'
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase], 
                                                      first_row=first5days[phase].get_rowmedian()+1,
                                                      table_background=table_background, reduced=True)
                schedulestring_export += '\n</div>\n'
                
            
        elif columns == 3: # dreispaltig für einen Tag, Bildschirm gedrittelt
            
                # links  
                schedulestring += '\n<div id="column1_3">\n'
                schedulestring += iterate_rows(schedule_obj, first5days[phase], \
                                               last_row=first5days[phase].get_rowthird(),
                                               table_background=table_background)
                # mitte
                schedulestring += '\n</div>\n<div id="column2_3">\n<h1>&nbsp;<small><small>(Fortsetzung)</small></small></h1>\n'
                schedulestring += iterate_rows(schedule_obj, first5days[phase], \
                                               first_row=first5days[phase].get_rowthird()+1,
                                               last_row=first5days[phase].get_rowtwothird(), 
                                               header_txt=" ", table_background=table_background)
                # rechts
                schedulestring += '\n</div>\n<div id="column3_3">\n<h1>&nbsp;<small><small>(Fortsetzung)</small></small></h1>\n'
                schedulestring += iterate_rows(schedule_obj, first5days[phase],
                                               first_row=first5days[phase].get_rowtwothird()+1,
                                               header_txt=" ", table_background=table_background)
                schedulestring += '\n</div>\n'
            
                # links  
                schedulestring_export += '\n<div id="column1_3">\n'
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase],
                                                      last_row=first5days[phase].get_rowthird(), 
                                                      table_background=table_background, reduced=True)
                # mitte
                schedulestring_export += '\n</div>\n<div id="column2_3">\n<h1>&nbsp;<small><small>(Fortsetzung)</small></small></h1>\n'
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase],
                                                      first_row=first5days[phase].get_rowthird()+1,
                                                      last_row=first5days[phase].get_rowtwothird(),
                                                      header_txt=" ", table_background=table_background, reduced=True)
                # rechts
                schedulestring_export += '\n</div>\n<div id="column3_3">\n<h1>&nbsp;<small><small>(Fortsetzung)</small></small></h1>\n'
                schedulestring_export += iterate_rows(schedule_obj, first5days[phase],
                                                      first_row=first5days[phase].get_rowtwothird()+1,
                                                      header_txt=" ", table_background=table_background, reduced=True)
                schedulestring_export += '\n</div>\n'
        
        return schedulestring, schedulestring_export


def get_interface():
    return S_Interface()


debugging = False


    
