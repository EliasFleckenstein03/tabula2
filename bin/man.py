#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 man.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 man.py erzeugt das Backoffice-Frontend von tabula.info und ermöglicht 
 damit alle Verwaltungsaufgaben
 ________________________________________________________________________
'''

# Batteries
from time import sleep

# TI
import ti_lib
try:
    import ti
    import konst
    import login
    import man_msgs
    import man_psns
    import man_s_any
    import motto
    import man_info
    import config
    import config_clients
except Exception as e:
    ti_lib.panic('ti.py: IMPORT Teil 1 fehlgeschlagen', e, '')


def get_bo_status(ti_ctx):
    if not ti_ctx.ses.checkedusername:
        return 405, '-'
    origstatus = ti_ctx.req.get_cgi_int("origstatus")
    for loop in range(3):  # bis zu 3 sek lang auf Änderungen warten lassen ("long polling")
        # try:
        status = 0
        statusmeldung = []
        bs = ti_ctx.cnf.get_db_config('background_semaphore')
        if bs != 'free':
            status = 1
            statusmeldung.append('Import ...'+bs.split('.')[0][-3:]+' läuft.')
        if ti_ctx.cnf.get_db_config_bool('s_any_force_convert'):
            status += 2
            if bs == 'free':
                statusmeldung.append('Import veranlasst!')
            else:
                statusmeldung.append('Erneuter Import geplant!')
        if status != origstatus:
            break
        sleep(0.5)  # Time in seconds
        # except:
        #    status = 8
        #    statusmeldung = ['Bye', '', '']
        #    break
        
    return status, f'''<span style="background-color:lightgreen">{'<br>'.join(statusmeldung)}</span>'''

def do_debugfile(ti_ctx):
    return config.debugfile(ti_ctx)
    
def do_the_managing(ti_ctx, wsgi=False):
    menustruct = [
        # taskname or script, Menütext, Titel, akzeptable Rechte (ein Bit genügt),
        #  muss man eingeloggt sein?, Link in _top anzeigen?, funktion
        ('1',  'Übersicht', '', 0,
         0, 0, None), 
        ('/ti/do/frames', 'Gesamtansicht', '', 0,
         0, 0, None), 
        ('/ti/do/astatus', 'Anzeigenstatus ', '', 0,
         0, 0, None), 

        ('2',  'Inhalte', '', 255,
         0, 0, None), 
        ('messages', 'Meldungen', 'Meldungen verwalten', konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert,
         0, 0, man_msgs.do_manage), 
        ('persons', 'Personenruf', 'Personenruf verwalten', konst.RECHTEPersonenruf,
         0, 0, man_psns.do_manage), 
        ('motto', 'Wochenmotto', 'Motto setzen', konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert,
         0, 0, motto.manage_motto), 
        ('info', 'Info-Seiten', 'Info-Seiten verwalten', konst.RECHTEInfoupload | konst.RECHTEInfouploaderweitert,
         0, 0, man_info.do_manage), 
        ('sched', 'Pläne', 'Pläne verwalten', konst.RECHTEVertretungsplan,
         0, 0, man_s_any.do_manage), 

        ('3',  'Administration', '', konst.RECHTEAdministration,
         0, 0, None), 
        ('config', 'Einstellungen', 'Einstellungen bearbeiten', konst.RECHTEAdministration,
         0, 0, config.do_config), 
        ('config_clients', 'Clients & Status', 'Client-Einstellungen', konst.RECHTEAdministration,
         0, 0, config_clients.do_config), 

        ('4',  'Login', '', 0, 
         0, 0, None), 
        ('login', 'Anmelden', 'Anmeldung', 0,
         0, 1, login.handle_login), 
        ('logout', 'Abmelden', '', 255,                                        
         1, 1, login.handle_logout), 
        ('chpw', _('Passwort ändern'), _('Passwort ändern'), 255,
         1, 0, login.change_password), 
    ] 

    task = ti_ctx.req.get_cgi("task")
    task_titel = ''
    
    #  md_response nimmt die Rückmeldung des Tasks auf, welche aber erst nach Erzeugung des Menüs ausgegeben wird.
    #  Das Menü wiederum kann von den Ergebnissen des Tasks abhängen, so dass dieser vorher bearbeitet wird!
    md_response = ti_lib.Multi_Detail_Response()  # falls unten kein neues übergeben wird
    for mtupel in menustruct:
        menutask, menutext, menutitel, menurights, mustbeloggedin, mustbetop, menufunktion = mtupel
        if menutask == task:
            if menutitel:
                task_titel = menutitel
            if not menurights or ti_ctx.check_management_rights(menurights):
                ti_ctx.res.debug(f"Rufe menufunktion() {menutask} auf")
                md_response = menufunktion(ti_ctx)
                if md_response.submenu_finished:
                    task_titel = ''
                if md_response.ist_abgebrochen():    # Ergebnis einer Abweisung wg. Rechtedefizit
                    ti_ctx.res.debug("menufunktion() wurde abgebrochen")
                    md_response = ti_lib.Multi_Detail_Response([skeleton_abweisung])
                    # ()eigentlich eine Fehlfunktion, da tasks nur mit ausreichenden Rechten aufgerufen werden sollen)
            else:  # uups, Rechteproblem (z.B. als Lesezeichen aufgerufen ohne angemeldet zu sein)
                ti_ctx.res.debug("menufunktion() mangels Rechten übersprungen")
                md_response = ti_lib.Multi_Detail_Response([skeleton_abweisung])
    if not task_titel:
        task_titel = '<small><small>Wählen Sie einen Menüpunkt</small></small>'
    ti_ctx.res.debug("+++++ Starte Management +++++")
    ti_session = ti_ctx.ses
        
    # ## Vorlagen ###
    themenu = [f'''
        <h1 style="text-align:center">
            <a href="https://tabula.info" target="_blank">
                <img src="https://tabula.info/version/{konst.TI_LOGO}" alt="ti Logo" width="64" height="64">
            </a>
            <br>tabula.info Verwaltung
        </h1>
        <div id="statusmeldung" style="text-align:center;font-size:80%">status<br>&nbsp;</div>
    ''']
    # normale Einträge
    entryskeleton = '<p><a href="?task={menutask}">{menutext}</a></p><p></p>'
    new_tab_skel = '<p><a href="{menutask}" target="_blank">{menutext}</a></p><p></p>'
    # Sektionenüberschrift
    headingskeleton = '<h4>{menutext}</h4><p></p>'

    ti_ctx.res.debug('meine Rechte', ti_session.get_alle_rechte(255))
    jumpto = '?'  # unklar... FIXME
    jumpwithrights = 0

    for mtupel in menustruct:
        menutask, menutext, menutitel, menurights, mustbeloggedin, mustbetop, menufunktion = mtupel
        # ti_ctx.res.debug('menutupel', mtupel)
        # ## Drei Arten von Menüeintragen
        if len(menutask) <= 1:       # Der Abschnittsname, hat natürlich kein eigener Task ist, hat einstelligen Platzhalter
            if (menurights == 0 or ti_session.get_alle_rechte(menurights)) and \
                    (not mustbeloggedin or len(ti_session.get_session_user()) > 0):
                themenu.append(headingskeleton.format(menutext=menutext))
              
        else:  # der normale Menüeintrag
            if (menurights == 0 or ti_session.get_alle_rechte(menurights)) and \
                    (not mustbeloggedin or ti_session.ist_angemeldeter_user()):
                if ".py" in menutask or "/" in menutask:
                    themenu.append(new_tab_skel.format(
                        menutext=menutext,
                        menutask=menutask,
                        ))
                else:
                    themenu.append(entryskeleton.format(
                        menutext=menutext,
                        menutask=menutask,
                        ))
                if ((menurights > 0 and jumpwithrights == 0) or jumpto == '') and not mustbetop and not mustbeloggedin:
                    jumpto = menutask
                    jumpwithrights = menurights  # jumpto unklar FIXME
    ziel = ti_session.get_session_command()
    if ziel == '':
        ti_session.set_session_command(jumpto)
    else:
        ti_session.set_session_command(ziel)

    man_content = [ti.get_html_head(ti_ctx, title='tabula.info - Management',
                   timeout=300, url='?task=""', ismanagement=True, keinemeldung=True, 
                   onload='bo_getAJAX()', tofile=wsgi)
                   ]
    man_content.append(konst.MAN_FRAMES_HTML_MENU.format(
        man_menu='\n\t\t<div class="mgmcontent">\n'+"\n".join(themenu)+'\n\t\t</div>\n',
        man_titel=task_titel,
        man_meldung=ti_ctx.ses.get_session_meldung()))

    # Nun wird der eigentliche Content eingefügt. Login hat oben bereits vor jeglicher Ausgabe Cookies abgehandelt
    
    for i in range(5):
        if md_response.html_list[i]:
            man_content.append('<div class="managementdetail">')                        # String anhängen
            man_content += md_response.html_list[i]                                     # Liste anhängen
            man_content.append('</div><!-- Ende eines divs "managementdetail" -->')     # String anhängen
    # always:
    man_content.append(konst.MAN_FRAMES_HTML_END)
    if wsgi:
        man_content.append(ti_ctx.get_html_tail())
    return man_content, md_response.sessionid


skeleton_abweisung = '''
    <div class="firstmgmcontent">
    <h1>Zugriff abgelehnt</h1>
    <p>Sie verfügen nicht über ausreichende Rechte<br>um die gewünschte Funktion nutzen zu können</p>
    <p><a href="?task=login">zum Login</a>'''
            

if __name__ == '__main__':
    ti_lib.quickabort2management()
