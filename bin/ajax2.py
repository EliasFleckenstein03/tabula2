# -*- coding: utf-8 -*-
#
# ajax2.py
# 
# bearbeitet die AJAX-Aufrufe und liefert immer json-Datensätze zurück

# Batteries
import json
import tool2
# TI
import messages
import persons
import pages
import man


def do_ajax(ti_ctx):
    task = ti_ctx.req.get_cgi('task')
     
    resultat = {}
    if task not in ['get_all', 'get_msections', 'get_popup', 'get_bottom', 'get_plan', 'get_bo_status']:
        resultat["status"] = "unbekannter Datenwunsch!"
        return json.dumps(resultat)

    resultat["status"] = "ok"

    # Backoffice - Import - Status ermitteln
    if task=='get_bo_status':
        resultat['neu_status'],resultat['html']= man.get_bo_status(ti_ctx)
        return json.dumps(resultat) 
        
    # Ab hier Ajax-Anfragen für das neue Interface...
    if task in ['get_all', 'get_plan']:
        resultat["plans"] = alle_seiten(ti_ctx)

    if task in ['get_all', 'get_bottom']:
        resultat["bottom"] = [messages.create_messages(ti_ctx, 1), messages.create_messages(ti_ctx, 2)]

    if task in ['get_all', 'get_msections']:
        clientgroups = 0
        messagesstring = messages.create_messages(ti_ctx, clientgroups)
        personsstring = persons.iterate_persons(ti_ctx, False)
        resultat["msections"] = [messagesstring, personsstring]
    resultat["frontstatus"] = tool2.get_string_from_sste(typ=1)
    return json.dumps(resultat) 


def iframe(url):
    """liefert den notwendigen HTML-Code, um ein iframe mit der Wunschurl einzublenden"""
    return '''<iframe class="plan_iframe" src="{}">
                Uuups, Ihr Browser kann keine eingebetteten Frames anzeigen</iframe>'''.format(url)


def zweiplaene(linker, rechter):
    return '''
        <div id="contentpl" class="tiP_Row">{}</div>
        <div id="contentpr" class="tiP_Row">{}</div>
        '''.format(linker, rechter)


def alle_seiten(ti_ctx):
    pa = pages.PageAssembly(ti_ctx)
    pa.analyse_whole_schedule()
    return [page.get_dict() for page in pa.pages]
