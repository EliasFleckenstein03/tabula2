'''
________________________________________________________________________
 webservice.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 webservice.py holt zusätzliche Daten von Webservices
________________________________________________________________________

'''
# Batteries
import json
# TI
# import ti


def hole_externe_daten(exturl):
    q11, q12 = '', ''
    try:
        data = hole_json(exturl)
        resi = json2messages(data)
        if len(resi) >= 2:
            q11, q12 = resi[0:2]
        else:
            q11, q12 = 'Übertragungsfehler', 'Übertragungsfehler'
    except Exception:
        q11, q12 = 'Verbindungsfehler', 'Verbindungsfehler'
    return q11, q12


def hole_json(url):
    """Ruft eine url auf, decodiert das Ergebnis in utf-8 was einen json-Ausdruck ergeben müsste
       dieser wird nun in ein Pythonobjekt übersetzt und zurückgegeben
    """
    import urllib.request
    try:
        with urllib.request.urlopen(url) as f:
            rawdata = f.read()
            decoded = json.loads(rawdata.decode("utf-8"))
    except Exception:
        raise Warning('Error in the connection to '+url)
    else:
        return decoded


def json2messages(jsonliste):
    """ jsondata ist ein array aus subarrays von tupeln
        daraus machen wir ein 
                array von html-teiltexten (basierend auf dem Skelett), 
                welche aufgezählte Namen (aus den Tupeln) enthalten, 
                die passend (aus den Tupeln) formatiert sind."""
    skelett = '''<div class="content"><table cellspacing="5"><tr>
    <td nobord="true"><h4>Fehlende Entschuldigungen bzw. Atteste klären!</h4>
    <p>{namen}<small><small> (absenzenverwaltung)</small></small></p>
    </td></tr></table>\n</div>'''
        
    if len(jsonliste) < 2:
        return ["fehlgeschlagen", "fehlgeschlagen"]
    resultatliste = []
    for subarray in jsonliste:
        namensliste = []
        for name, flag in subarray:
            if flag == 1:
                pre, small, post = '<span class="s_t_fresh">', '<small class="s_t_fresh">&nbsp;', '</span>'
            elif flag == 2:
                pre, small, post = '<span style="color:brown">', '<small style="color:brown">&nbsp;', '</span>'
            else:
                pre, small, post = '', '<small>&nbsp;', ''
            namegeteilt = name.split(' ')            
            namensliste.append(pre+namegeteilt[0]+small+'&nbsp;'.join(namegeteilt[1:])+'</small>'+post)
        if namensliste:
            resultatliste.append(skelett.format(namen=', '.join(namensliste)))
        else:
            resultatliste.append('')
    return resultatliste


def selbsttest():
    try:
        data = hole_json("http://192.168.2.100:8380/aapp?task=abrufe")
        print(data)
        resi = json2messages(data)
        for supi in resi:
            print(supi)
    except Exception:
        print("Datenabruf im selbsttest nicht erfolgreich")


if __name__ == '__main__':
    ti_lib.quickabort2frames()
