#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 ti_init.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_init.py führt zwei Arten von Initialisierung durch, 
            die erst via WSGI einen Unterschied machen:
    - für das System, einmalig bis zum Neustart des Skripts
    - für den Clientrequest
________________________________________________________________________
'''
# Batteries
import cgi
import time
import locale
import os
import sqlite3
import sys
from abc import ABCMeta, abstractmethod
import inspect  # für die debugging-Funktion
# Toolbox
import werkzeug
# TI
import ti_lib
 
try:
    import client
except Exception as e:
    ti_lib.panic('ti_init.py: IMPORT fehlgeschlagen', e, '')

 
# ################################################    
# ## hier beginnt der Code zur Initialisierung ###
# ################################################
class TI_System():
    """Enthält alle Informationen für das "hochgefahrene" TI-System
       Leider müssen Datenbank-initialisierungen threadweise getrennt vorgenommen werden.
    """
    def __init__(self):
        try:
            locale.setlocale(locale.LC_ALL, ('de_DE', 'UTF8'))   
        except Exception:
            ti_lib.panic('Kann nicht auf deutsche Einstellungen schalten', 
                         BaseException("Es muss Linux mit deutschem locale installiert sein!"))
        self.start_epoche = ti_lib.get_now()
        
        try:
            self.cdb_conn = sqlite3.connect(ti_lib.get_ti_datapath('content.sq3'))
            cursor = self.cdb_conn.cursor()
            cursor.execute("""CREATE TABLE IF NOT EXISTS messages (
                                flag INTEGER, 
                                targetday INTEGER, 
                                created INTEGER, 
                                deleted INTEGER, 
                                lasthour INTEGER, 
                                target INTEGER, 
                                headline TEXT, 
                                details TEXT, 
                                color TEXT, 
                                author TEXT ) """)
            cursor.execute("""CREATE TABLE IF NOT EXISTS persons (
                                longname TEXT, 
                                target TEXT, 
                                timestamp INTEGER) """)
            cursor.close()
            self.cdb_conn.commit()
        except Exception as e:
            ti_lib.panic('Content-DB konnte nicht initialisiert werden', e)
    
    def get_epoche(self):
        return self.start_epoche

       
class TI_Response():
    """sammelt Informationen für die Response
       inkl. Fehlermeldungen"""
    def __init__(self, epoche, debug2stderr=False): 
        self.debug_messages = []
        self.start_epoche = epoche
        self.debug2stderr = debug2stderr
    #######################################
    # einfache Debuggingmöglichkeit

    def debug(self, *args, use_caller_caller=False):
        values = [escape_html(repr(t)) for t in args]
        if isinstance(args[0], str):
            values[0] = escape_html(args[0])
        caller = inspect.currentframe().f_back.f_code if not use_caller_caller else inspect.currentframe().f_back.f_back.f_code 
        file_name = caller.co_filename.split('/')[-1]
        if len(file_name) > 3 and file_name[-3:] == '.py':
            file_name = file_name[:-3]
        func_name = '.'+caller.co_name
        caller_name = ' '*(21-len(file_name))+file_name+func_name+' '*(21-len(func_name))
        if len(values) > 1 and (values[1] and values[1][0] not in ':=') \
                and (values[0] and values[0][-1] not in ':='):
            values[0] += " ="
        thevalues = ' '.join(values)
        self.debug_messages.append(f'<!-- Info von {caller_name}: {thevalues}{ " "*(70-len(thevalues)) }-->')
        if self.debug2stderr:
            sys.stderr.write(thevalues+'\n')
            sys.stderr.flush()
    # muss am Ende händisch aufgerufen werden, kann aber auch mehrfach geschehen

    def debug_flush(self):
        all_messages = '\n'.join(self.debug_messages)
        self.debug_messages = []
        return all_messages
        
    def debug_timestamp(self, eventname, timestamp=0):
        
        if timestamp:
            self.debug(eventname, str(timestamp-self.start_epoche), True)
        else:
            self.debug(eventname, str(time.time()-self.start_epoche), True)
        
# danke Dan Bader
# Abstract Base Class stellt sicher, dass beide Klassen dasselbe (Grund)-Interface bereitstellen


class TI_Request_ABC(metaclass=ABCMeta):
    @abstractmethod
    def get_cgi(self, parameter, default=""):
        pass
        
    @abstractmethod
    def get_cgi_bool(self, parameter, default=False):
        pass
        
    @abstractmethod
    def get_cgi_int(self, parameter, default=0):
        pass
        
    @abstractmethod
    def get_cgi_object(self, parameter):
        pass
        

class TI_Request_CGI(TI_Request_ABC):
    """enthält alle Informationen für den aktuellen Aufruf per CGI"""
    def __init__(self, ti_cnf, ti_res): 
        self.client_name = ""
        self.cgiFS = cgi.FieldStorage()
        self.is_WSGI = False
        # ti_res.debug('Trusted Proxy?', ti_cnf.get_db_config('Man_TrustedProxy').strip())
        try:
            ip_address = cgi.escape(os.environ["REMOTE_ADDR"]).strip()
        except Exception:
            ip_address = '127.0.0.1'
        
        # ti_res.debug("Trusted Proxy, IP-Adresse", ti_cnf.get_db_config('Man_TrustedProxy').strip(), ip_address)
        if ip_address and ip_address == ti_cnf.get_db_config('Man_TrustedProxy').strip():
            ti_res.debug('Trusted Proxy entdeckt', ip_address)
            if 'HTTP_X_FORWARDED_FOR' in os.environ:
                # ti_res.debug('HTTP_X_FORWARDED_FOR', cgi.escape(os.environ["HTTP_X_FORWARDED_FOR"]))
                ip_address = cgi.escape(os.environ["HTTP_X_FORWARDED_FOR"]).split(', ')[-1].strip()
                ti_res.debug('endgültig: X_FORWARDED_FOR', ip_address)
        self.client = client.TI_Client(ip_address, ti_cnf, ti_res)
        # für messages.py
        self.mess_manage_mode = False
                
    # Cookies:
    def get_websessionid(self):
        if 'HTTP_COOKIE' in os.environ:
            cookies = os.environ['HTTP_COOKIE']
            cookies = cookies.split('; ')
            handler = {}
      
            for cookie in cookies:
                cookie = cookie.split('=')
                handler[cookie[0]] = cookie[1]
            if "ti_sessionid2" in handler:
                return handler["ti_sessionid2"]
        return ""
   
    # ###############################
    # CGI-Werte handhaben

    def get_cgi(self, parameter, default=""):
        return self.cgiFS.getfirst(parameter, default)

    def get_cgi_bool(self, parameter, default=False):
        return ti_lib.strg2bool(self.get_cgi(parameter), default)
        
    def get_cgi_int(self, parameter, default=0):
        return ti_lib.strg2int(self.get_cgi(parameter), default)
    
    def get_form(self, parameter, default=""):
        return self.get_cgi(parameter, default="")
        
    # für Uploads
    def get_cgi_object(self, parameter):
        if parameter in self.cgiFS:
            return self.cgiFS[parameter]
        else:
            return None


class TI_Request_WSGI(TI_Request_ABC):
    """enthält alle Informationen für den aktuellen Aufruf per WSGI via werkzeug"""
    def __init__(self, request, ti_cnf, ti_res):
        self.is_WSGI = True
        # das request-Objekt kommt von "werkzeug", das ti_res...ponseobjekt muss vorher initialisiert worden sein
        self.client_name = ""
        # hier nicht: self.cgiFS=request.FieldStorage()
        self.request = request
        # Zukunft?: self.s_interface=s_any.S_Interface()
        try:
            ip_address = request.remote_addr
        except Exception:
            ip_address = '127.0.0.1'

        # ti_res.debug("Trusted Proxy, IP-Adresse", ti_cnf.get_db_config('Man_TrustedProxy').strip(), ip_address)
        if ip_address and ip_address == ti_cnf.get_db_config('Man_TrustedProxy').strip():
            ti_res.debug('Trusted Proxy entdeckt', ip_address)
            # ti_res.debug('request',request.environ)
            if 'HTTP_X_FORWARDED_FOR' in request.environ:
                # ti_res.debug('HTTP_X_FORWARDED_FOR', cgi.escape(request.environ["HTTP_X_FORWARDED_FOR"]))
                ip_address = cgi.escape(request.environ["HTTP_X_FORWARDED_FOR"]).split(', ')[-1].strip()
                ti_res.debug('endgültig: X_FORWARDED_FOR', ip_address)

        self.client = client.TI_Client(ip_address, ti_cnf, ti_res)
        # quasiglobale Variablen für messages.py
        self.mess_manage_mode = False
        
    # Cookies:
    def get_websessionid(self):
        multidict = werkzeug.http.parse_cookie(self.request.environ, charset='utf-8', errors='replace', cls=None)
        if "ti_sessionid2" in multidict:
            return multidict["ti_sessionid2"]
        else:
            return ""

    ################################
    # CGI-Werte handhaben
 
    def get_cgi(self, parameter, default=""):
        try:
            value = self.request.args[parameter]
        except Exception:
            value = default
            
        if not value and self.request.method == 'POST':
            return self.get_form(parameter, default)

        return value

    def get_cgi_bool(self, parameter, default=False):
        return ti_lib.strg2bool(self.get_cgi(parameter), default)
        
    def get_cgi_int(self, parameter, default=0):
        return ti_lib.strg2int(self.get_cgi(parameter), default)
        
    def get_form(self, parameter, default=""):
        try:
            value = self.request.form[parameter]
        except Exception:
            return default
        return value

    # für Uploads (ungetestet 2020-01-12)
    def get_cgi_object(self, parameter):
        return None  # disabled
    
    def save_file(self, parameter, filepath):
        """Speichert genau eine Datei ab, die im Formular unter parameter abgeschickt wurde.
            Der filepath ist der komplette Pfad MIT Dateiname, der zum Speichern verwendet werden soll.
            Rückgabewert: (nicht verwendeter, nicht gesicherter) Original-Dateiname bei Erfolg, sonst None"""
            
        if parameter not in self.request.files:
            return None
        ufile = self.request.files.get(parameter)
        ufile.save(filepath.as_posix())
        return ufile.filename
        
    def save_files(self, parameter, targetpath, path_is_filename=False):
        """Speichert mehrere Dateien ab, die im Formular unter parameter abgeschickt wurde.
            Der filepath ist der Pfad OHNE Dateiname, der zum Speichern verwendet werden soll.
            Verwendet wird der übergebene Dateiname, nachdem er gestrippt (save_filename) wurde.
            Rückgabewert: LISTE mit Dateinamen, ggf. leere Liste"""
        fnlist = []
        if parameter not in self.request.files:
            return fnlist
        for ufile in self.request.files.getlist(parameter):
            targetfn = ti_lib.save_filename(ufile.filename)
            if targetfn:
                ufile.save((targetpath / targetfn).as_posix())
                fnlist.append(targetfn)
        return fnlist
        
    def debug_get_all_args(self):
        return self.request.args


def escape_html(dangerous):
    save = dangerous.replace("&", '&amp;')
    saver = save.replace("<", '&lt;').replace(">", '&gt;').replace('--', '- -').replace('    ', " ").replace('   ', " ")
    return saver.replace('  ', " ")


if __name__ == '__main__':
    ti_lib.quickabort2frames()
