#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""ti2.py ist der zentrale Aufruf von/für tabula.info in der Version 2
   Neu in Version 2: Aufruf via wsgi, systematischere Content-Verteilung
"""
# Batteries (included)
import sys

# Importe von werkzeug (per apt installiert)
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException  # , NotFound
try:  # ab 0.16 (Ubuntu Focal)
    from werkzeug.middleware.shared_data import SharedDataMiddleware
except Exception:  # fallback für 0.14 (Debian Buster)
    from werkzeug.wsgi import SharedDataMiddleware
# from werkzeug.utils import redirect

# Importe ti2 (eigene Module)
import html_barebone2
import ajax2

# Importe von ti-classic - die Schnittstelle
import ti_lib
import config_db
import ti_init
import ti_context
import composer
import ti
import login
import man


def hacklog(*args):
    sys.stderr.write("!! {}\n".format(": ".join([str(i) for i in args])))
    sys.stderr.flush()
    

class TiApp(object):
    # Kontruktor: Legt Zuordnung zwischen der URL und der aufgerufenen Funktion fest
    def __init__(self, config):
        # links URL-Pfad bezogen auf das aufgerufene Skript
        # Pfade kommen doppelt vor, damit per mod_wsgi@apache und per run_simple (s.u.) lauffähig
        self.url_map = Map([
            Rule('/',  endpoint='forward'), 
            Rule('/ti', endpoint='forward'), 
            Rule('/ti/do/frontend', endpoint='frontend'),  # kompletter Pfad für run_simple
            Rule('/frontend', endpoint='frontend'),  # für mod_wsgi (das als /ti/do läuft)
            Rule('/ti/do/ajax', endpoint='ajax'), 
            Rule('/ajax', endpoint='ajax'), 
            Rule('/ti/do/manage', endpoint='manage'), 
            Rule('/manage', endpoint='manage'), 
            Rule('/ti/do/debugfile', endpoint='debugfile'), 
            Rule('/debugfile', endpoint='debugfile'), 
            Rule('/ti/do/frames', endpoint='frames'), 
            Rule('/frames', endpoint='frames'), 
            Rule('/ti/do/astatus', endpoint='astatus'), 
            Rule('/astatus', endpoint='astatus'), 
            
        ])
        hacklog("ti.app initialisiert")
        self.threadcounter = 0

    # unsere App ist ein callable
    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    # diese Funktion wird ggf. mehrfach parallel aufgerufen. 
    # Sie muss also alle dynamischen Variablen (insbesondere ti_ctx) selbst erzeugen und nicht in self ablegen!
    def wsgi_app(self, environ, start_response):
        self.threadcounter += 1  # Nur zur Info. Race condition kann ignoriert werden
        
        request = Request(environ)
        
        ti_sys = ti_init.TI_System()  # Basis
        ti_cnf = config_db.TI_Config()  # Config-Datenbank
        ti_res = ti_init.TI_Response(ti_sys.get_epoche())  # für die Ausgabe
        ti_req = ti_init.TI_Request_WSGI(request, ti_cnf, ti_res)  # der Request
        # hacklog("Client", ti_req.client.ip_address, "Loginuser", ti_req.get_cgi("tiuser"),"PW_Laenge",
        #  len(ti_req.get_cgi("tipassword")))
        # None statt 
        ti_ses = login.TI_Session(ti_req, ti_cnf, ti_sys.get_epoche())

        ti_ctx = ti_context.TIConTeXt(ti_sys, ti_cnf, ti_req, ti_res, ti_ses)
        # kann nun jedem Tool zur Verfügung gestellt werden können

        # in dem folgenden Aufruf wird die eigentlich gewünschte Nutz-Funktion aufgerufen und das Ergebnis zurückgegeben
        response = self.dispatch_request(request, ti_ctx)
        
        return response(environ, start_response)

    def dispatch_request(self, request, ti_ctx):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            hacklog(ti_ctx.req.client.ip_address, "ruft", endpoint, request.method, "- T,is_mp,is_mt", 
                    self.threadcounter,
                    request. is_multiprocess, request.is_multithread)
            return getattr(self, 'do_' + endpoint)(request, ti_ctx, **values)
        except HTTPException as e:
            hacklog('Fehlschlag bei URL:', request.environ['REQUEST_URI'])
            return e

    # Ab hier kommen die Nutz-Funktionen:

    def do_forward(self, request, ti_ctx):
        return Response(html_barebone2.root_forward, mimetype='text/html')
        
    def do_frontend(self, request, ti_ctx):
        return Response(html_barebone2.frontend, mimetype='text/html')

    def do_frames(self, request, ti_ctx):
        if ti.check_n_background(ti_ctx.cnf):
            ti_ctx.res.debug('Background noetig')
        res = Response(composer.compose_frames(ti_ctx, wsgi=True), mimetype='text/html')
        return res
    
    def do_ajax(self, request, ti_ctx):
        return Response(ajax2.do_ajax(ti_ctx), mimetype='application/json')
        
    def do_astatus(self, request, ti_ctx):
        # res = Response('\n'.join(ti.astatus(self.ti_ctx, wsgi=True)), mimetype='text/html')
        res = Response(ti.astatus(ti_ctx, wsgi=True), mimetype='text/html')
        return res
     
    def do_manage(self, request, ti_ctx):
        htmlresponse, sessionid = man.do_the_managing(ti_ctx, wsgi=True)
        res = Response(htmlresponse, mimetype='text/html')
        if not sessionid:
            sessionid = ti_ctx.ses.setcookiesessionid  # Dann wird der Wert für vianetwork gesetzt
        # if not sessionid:
        #    sessionid=b'Kill:123456789A'
        if sessionid:
            hacklog("Ein Cookie zu setzen!", sessionid)
            res.set_cookie('ti_sessionid2', sessionid, path='/ti/do', samesite='Strict')
        return res
        
    def do_debugfile(self, request, ti_ctx):
        res = Response(man.do_debugfile(ti_ctx), 
                       headers=[('Content-Disposition','attachment;filename=debug.html.xz')],
                       mimetype='application/octet-stream')
        return res
     

# # Hier wird die eigentliche App erzeugt - diese muss als callable multithreaded aufgerufen werden können
def create_app(selfcontained=False):
    # eigentliche App anlegen, Demodaten werden nicht benutzt
    app = TiApp({
        'redis_host':       123, 
        'redis_port':       8000
    })
    # App ergänzen, damit statische Inhalte unterhalb von www gezeigt werden
    if selfcontained:
        hacklog(_('App liefert statischen Content selbst per WSGI aus.'))
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/ti/static':  ti_lib.get_ti_basepath('www/static').as_posix(), 
            '/ti/dyn':     ti_lib.get_ti_dynpath().as_posix(), 
            '/ti/local':   ti_lib.get_ti_dynpath('../local').resolve().as_posix(),
        })
    return app
    

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app(True)
    run_simple('0.0.0.0', 5000, app, use_debugger=True, use_reloader=True, threaded=True)
