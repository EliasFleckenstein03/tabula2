
# Batteries
import time
import locale
locale.setlocale(locale.LC_ALL, '')
# TI
# (nix)


def get_string_from_sste(sste=None, typ=0):
    """sste seconds since the epoch
    sste=None -> jetzt, sonst in Sekunden seit 1.1.70 gegebene Zeit 
    liefert je nach typ folgendes Format:
    0: 2019-01-28_13:41
    1: 28.01.2019 13:41
    2: Montag, der 28. Januar 2019, 13:41
    """
    muster = ["%Y-%m-%d_%H:%M", "%d.%m.%y %H:%M", "%A, der %d. %B %Y, %H:%M"]
    return time.strftime(muster[typ], time.localtime(sste))


if __name__ == '__main__':
    for i in range(3):
        print(get_string_from_sste(typ=i))
        
