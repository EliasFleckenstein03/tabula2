#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 login.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 login.py verwaltet das Ein- und Ausloggen, Passwort ändern und das
 Session-Management für das tabula.info-Backoffice
________________________________________________________________________
'''
# Batteries
import os
import sqlite3
import random
import http.cookies
import bcrypt
# TI
import ti
import ti_lib
import konst


def hacklog(*args):
    import sys
    sys.stderr.write("!! {}\n".format(": ".join([str(i) for i in args])))
    sys.stderr.flush()


class TI_Session():
    """enthält alle Sessioninformationen für 
       eingeloggte oder anders legitimierte User"""
    def __init__(self, ti_req, ti_cnf, epoche, set_cookie=True, force_sessionid=0):
        ''' Diese Funktion testet einmalig, ob man bereits in einer session ist
            und setzt passende Variablen.
            Diese Funktion muss früh (vor dem ersten prt) 
            ausgeführt werden, um am Ende noch ein Cookie setzen zu können.'''
        self.clientobject = ti_req.client
        self.ti_req = ti_req
        self.ti_cnf = ti_cnf
        self.epoche = epoche
        self.debugmeldungen = []
        self.db_connect()   # legt z.B. self.access_cursor an, was alle anderen Methoden nutzen können
        self.checkedsessionid = 0
        self.checkedusername = ''
        self.checkedrights = 0
        
        sessionid = force_sessionid if force_sessionid else ti_req.get_websessionid()
        self.debug("sessionid lt. Cookie", sessionid)
        self.setcookiesessionid = 0  # Wert, der per Cookie übertragen werden soll. Nur für wsgi nötig
        
        if sessionid:
            now = ti_lib.get_now()
            rows = self.access_cursor.execute('SELECT count(*) FROM sessions WHERE expires <= ?', (now, )) 
            # Es gibt was aufzuräumen (das Nachfragen ist preiswert)
            for row in rows:
                if row[0]:
                    self.access_cursor.execute('DELETE FROM meldungen WHERE id IN ( SELECT id FROM sessions WHERE expires<=?)',
                                               (now, ))
                    self.access_cursor.execute('DELETE FROM sessions WHERE expires <= ?', (now, ))
                    self.access_conn.commit()
            self.access_cursor.execute('SELECT username, rechte FROM sessions WHERE id=?', (sessionid, ))
            for row in self.access_cursor:
                self.checkedsessionid = sessionid
                self.checkedusername = row[0]
                self.checkedrights = row[1]
                self.debug('Gültige Session mit', f'{self.checkedsessionid}, {self.checkedusername}, {self.checkedrights}')
                break
            else:
                sessionid = ''  # da obige websessionid offensichtlich ungültig
        if not sessionid:        
            # ok, hier landen wir ohne gültige sessionid
            # falls wir im Managementnetwork sind, dann legen wir eine an.
            # wenn keine Session, dann ist eben self.checkedsessionid=0
            if self.clientobject.ist_in_management_network():
                self.debug('lege eine Netzwerksession an!')
            
                sessionid = str(random.randint(123456789, 987654321))
                expires = str(ti_lib.get_now()+ti_cnf.get_db_config_int("Login_Duration", 15)*60)    
                # nach idR. 15 minuten verfällt die Session
                rechte = self.get_userrechte('vianetwork')
                self.access_cursor.execute('INSERT INTO sessions (id, expires, username, rechte) VALUES (?, ?, ?, ?)',
                                           (sessionid, expires, 'vianetwork', rechte))
                self.access_conn.commit()
                cookie = http.cookies.SimpleCookie()
                cookie['ti_sessionid2'] = sessionid
                self.debug("Cookie.output", cookie.output())
                if set_cookie:
                    ti.prt(cookie.output() + "\n") 
                # dauerhaft legales ti.prt, da cookies früh rausmüssen - außer per wsgi
                else:
                    self.setcookiesessionid = sessionid          # für wsgi
                self.checkedsessionid = sessionid
                self.checkedusername = 'vianetwork'
                self.checkedrights = rechte
                self.debug('Session angelegt mit:', f'{self.checkedsessionid}, {self.checkedusername}, {self.checkedrights}')
                
        self.allrights = self.checkedrights  # aus Session-DB
        if self.clientobject.ist_in_management_network():
            self.allrights |= self.get_userrechte('vianetwork') 
            # bitweises ODER um Rechte für Verwaltungsnetzwerk zu addieren
            
            if self.clientobject.ist_cukopplung():
                self.allrights |= self.get_userrechte(self.clientobject.get_name())
                # bitweises ODER um Rechte durch Client-User-Kopplung zu addieren

        return 
        
    def debug(self, *args):
        self.debugmeldungen.append("TI_Session: "+', '.join(args))

    def get_n_clear_debugmeldungen(self):
        ratz = ''
        for d in self.debugmeldungen:
            ratz += f'<!-- TI_Session.debug meldet:{d} --> \n'
        self.debugmeldungen = []
        return ratz
        
    def db_connect(self):
        self.access_conn = sqlite3.connect(ti_lib.get_ti_datapath('accessdb.sq3'))
        self.access_cursor = self.access_conn.cursor()
        
        self.access_cursor.execute('CREATE TABLE IF NOT EXISTS users (name TEXT PRIMARY KEY, passhash TEXT, rechte INTEGER) ')
        # # im Notfall:
        # #  entkommentieren, um frischen admin anzulegen, 
        # #  nach Seitenaufruf wieder auskommentieren, Seite neu laden, Login
        # # self.access_cursor.execute('DELETE FROM users WHERE name == "admin"') 
        
        # Nun werden die zwei Standardeinträge geprüft (preiswert) und ggf. angelegt (teuer)
        self.access_cursor.execute('SELECT count(name) FROM users WHERE name in ("admin" , "vianetwork") ')
        res = self.access_cursor.fetchall()
        if res[0][0] < 2:
            try: 
                self.access_cursor.execute('INSERT INTO users VALUES ("admin", ?, 255) ', (make_bc_hash("goti"), )) 
                self.debug('Neuer Administrator "admin" angelegt')
            except Exception: 
                self.debug('"admin" existiert schon')
            try: 
                self.access_cursor.execute('INSERT INTO users VALUES ("vianetwork", ?, 0) ',
                                           (make_bc_hash(str(random.randint(2**31, 2**32))), )) 
                self.debug('vianetworks angelegt')
            except Exception:
                self.debug('vianetworks existiert bereits')
        self.access_cursor.execute('''CREATE TABLE IF NOT EXISTS sessions
                                        (id INTEGER PRIMARY KEY, 
                                         expires INTEGER, 
                                         username TEXT DEFAULT "", 
                                         rechte INTEGER DEFAULT 0) ''')
        self.access_cursor.execute('CREATE TABLE IF NOT EXISTS meldungen (id INTEGER, meldung TEXT, iscommand INTEGER) ')
        self.access_conn.commit()
        
    def get_session_id(self):
        return self.checkedsessionid
    
    def get_session_user(self):
        return self.checkedusername
            
    def ist_angemeldeter_user(self):
        if self.checkedusername and self.checkedusername != 'vianetwork':
            self.debug('ist angemeldeter user')
            return True
        else:
            self.debug('ist KEIN angemeldeter user')
            return False

    def get_alle_rechte(self, rechte=255):  # für Aufrufer relevante Rechte als Bitmuster
        """Die angeforderten Rechte werden mit den eigenen allrights binär undiert und zurückgegeben.
            i.d.R.: >0 -> Request zulässig"""
        return self.allrights & rechte
        
    def get_meine_rechte(self): 
        return self.checkedrights

    def get_session_meldung(self):
        # diese Meldung soll eigentlich Ergebnisse zurückliefern
        # Erfolge beginnen mit +, Fehler mit -
        # Wenn emptytoo gesetzt ist, so wird auch ohne anzuzeigende Meldung eine leere Meldung als Platzhalter geliefert
        
        resultat = ''
        if not self.checkedsessionid:
            return ''
        self.access_cursor.execute('SELECT meldung FROM meldungen WHERE id=? AND iscommand=0', (self.checkedsessionid, ))
        meldungen = self.access_cursor.fetchall()
        for row in meldungen:
            meldung = row[0]
            if meldung:
                color = 'grey'
                if meldung[0] == '-':
                    color = 'orange'
                    meldung = meldung[1:]
                elif meldung[0] == '+':
                    color = 'lightgreen'
                    meldung = meldung[1:]
                    # prima, Erfolge führen zu einer Verlängerung der Session-Lebensdauer:
                    expires = ti_lib.get_now()+self.ti_cnf.get_db_config_int("Login_Duration", 15)*60   
                    # nach idR. 15 minuten verfällt die Session
                    # Bedingung, ob es sich lohnt. Nur eine Verlängerung pro Minute.
                    old_expires = self.access_cursor.execute('SELECT expires FROM sessions WHERE id=?',
                                                             (self.checkedsessionid, )).fetchall()[0][0]
                    self.debug("old_expires", str(old_expires), "expires", str(expires))
                    if expires-old_expires > 60:
                        self.access_cursor.execute('UPDATE sessions SET expires=? WHERE id=?',
                                                   (expires, self.checkedsessionid))
                        self.access_conn.commit()
                resultat += '<h4 style="margin:0px;text-align:center;background-color:'+color+'">'+meldung+'</h4>\n'
        if resultat:
            self.access_cursor.execute('DELETE FROM meldungen WHERE id=? AND iscommand=0', (self.checkedsessionid, ))
            self.access_conn.commit()
        else:
            resultat = '<h4 style="margin:0px;text-align:center;background-color:none">&nbsp;</h4>\n'
        return resultat

    def get_session_command(self):
        # Ein Kommando an das nachfolgende Programm sollte von ihm interpretiert werden
        resultat = ''
        if not self.checkedsessionid:
            return ''
        self.access_cursor.execute('SELECT meldung FROM meldungen WHERE id=? AND iscommand != 0', (self.checkedsessionid, ))
        for row in self.access_cursor:
            resultat = row[0]
        if len(resultat):
            self.access_cursor.execute('DELETE FROM meldungen WHERE id=? AND iscommand != 0', (self.checkedsessionid, ))
            self.access_conn.commit()
        return resultat

    def set_session_meldung(self, meldung):
        # diese Meldung wird im nächsten cgi-Aufruf wieder ausgelesen
        self.access_cursor.execute('INSERT INTO meldungen (id, meldung, iscommand) VALUES (?, ?, 0)',
                                   (self.checkedsessionid, meldung))
        self.access_conn.commit()

    def set_session_command(self, command):
        # dieses Kommando (z.B. Url) wird im nächsten cgi-Aufruf wieder ausgelesen
        self.access_cursor.execute('INSERT INTO meldungen (id, meldung, iscommand) VALUES (?, ?, 1)', 
                                   (self.checkedsessionid, command))
        self.access_conn.commit()

    def get_userrechte(self, username):
        rechte = 0
        rows = self.execute('SELECT rechte FROM users WHERE name=?', (username, ))
        for row in rows:
            rechte = row[0]
        return rechte
        
    def set_userrechte(self, username, rechte):
        self.execute('UPDATE users SET rechte=? WHERE name=?', (rechte, username))
        self.commit()
        
    def execute(self, sql, tupel=()):
        return self.access_cursor.execute(sql, tupel)
        
    def commit(self):
        self.access_conn.commit()
        
    def check_management_rights(self, ti_ctx, rechte=255):
        '''bei zu geringen Rechten wird das Skript von hier aus
                        mit entsprechender Meldung abgebrochen!'''
        response = []
        allerechte = self.get_alle_rechte(rechte)
        if allerechte:
            return allerechte
            
        # Quittung für fehlende Rechte:
        return 0  # keine verbliebenen Rechte
        

    def list_rechte(self, rechte=-1, sep=", "):
        if rechte < 0:
            rechte = self.get_meine_rechte()
        liste = []
        for i in range(10):
            if rechte & (1 << i):
                rn, rk = get_rechte_bez(i)
                if rn:
                    liste.append(rn)
        if not len(liste):
            liste = ['(keine)']
        return sep.join(liste)

    def get_userlist(self):
        return list(self.execute('SELECT name, rechte FROM users WHERE name<>"admin" ORDER BY name'))
     
    def reset_userpasswd(self, username):
        self.execute('UPDATE users SET passhash=? WHERE name=?', (make_bc_hash('tabula.info'), username))
        self.commit()
        
    def adduser(self, username):
        self.execute('INSERT INTO users (name, passhash, rechte) VALUES (?, ?, ?)', (username, make_bc_hash('tabula.info'), 0))
        self.commit()
        
    def remove_dbuser(self, user2delete):
        self.execute('DELETE FROM users WHERE name=?', (user2delete, ))
        self.commit()

    def passwordcheck(self, changepasswd=False):
        hacklog("Starte passwordcheck")
        user = self.ti_req.get_form("tiuser")
        password = self.ti_req.get_form("tipassword")
        hacklog("nach passwortauslesen. Laengen:",len(user),len(password) )
        if changepasswd:
            np1 = self.ti_req.get_cgi("newpassword1")
            np2 = self.ti_req.get_cgi("newpassword2")
        rows = self.execute('SELECT passhash, rechte FROM users WHERE name=?', (user, ))
        for row in rows:
            pw_hash = row[0]
            # self.debug('User gefunden mit hash', pw_hash)
            if pw_hash[:4] in ['$2a$', '$2b$', b'$2a$', b'$2b$']:
                bc = check_bc_pass(password, pw_hash)
            else:
                bc = False
                self.debug('gespeicherter Hash ist kein bcrypt-Hash')
                
            if bc:
                if changepasswd:
                    if (len(np1) > 5) and np1 == np2:
                        self.execute('UPDATE users SET passhash=? WHERE name=?', (make_bc_hash(np1), user))
                        self.commit()
                        return True, user, "", row[1]
                    else:
                        return False, user, (_("Passwort zu kurz") if len(np1) <= 5 else _("Passwörter verschieden")), 0
                else:
                    return True, user, "", row[1]
        return False, user, _("Unbekannter User"), 0

# # Ende der Class-Definition von TI_Session
        
        
def get_rechte_bez(i):
    RECHTENAMEN = [_('Personenruf'), _('Meldungen'),       _('Meldungen erweitert'),
                   _('Infoupload'), _('Infoupload erweitert'), _('Vertretungsplan'),
                   _('Administration')]
    RECHTEKUERZEL = [' ', ' ', 'e',     
                     ' ', 'e', ' ', 
                     ' ']
    if i < len(RECHTENAMEN):
        return RECHTENAMEN[i], RECHTENAMEN[i][0]+RECHTEKUERZEL[i]
    else:
        return "", ""


def get_rechte_nr(i):
    RECHTENUMMER = [konst.RECHTEPersonenruf, konst.RECHTEMeldungen, konst.RECHTEMeldungenerweitert,
                    konst.RECHTEInfoupload, konst.RECHTEInfouploaderweitert, konst.RECHTEVertretungsplan,
                    konst.RECHTEAdministration]  # natürlich Zweierpotenzen, so aber leichter lesbar
    # siehe auch die unbedingt dazu passenden RECHTE-Definitionen in ti.py
    if i < len(RECHTENUMMER):
        return RECHTENUMMER[i]
    else:
        return -1


skeleton = '''
    <div class="login"><br>
        <form action="?task=login" target="_top" method="post" style="text-align: center; margin-top: 10px;">
            {}: <input name="tiuser" ><br>
            {}: <input name="tipassword" type="password" ><br>
            <input value="Absenden" type="submit">
        </form><br>
    </div>
'''.format(_('Name'), _('Passwort'))

skeleton2 = '''
    <div class="login"><br>
        <form action="manage?task=chpw" method="post" style="text-align: center; margin-top: 10px;">
          <table style="border:0;">
            <tr><td  style="border:0;text-align:right">Name:</td>
                    <td  style="border:0;text-align:left"><input name="tiuser" value="{{deruser}}"></td></tr>
            <tr><td  style="border:0;text-align:right">Aktuelles Passwort:</td>
                    <td  style="border:0;text-align:left"><input name="tipassword" type="password" ></td></tr>
            <tr><td  style="border:0;text-align:right">Neues Passwort:</td>
                    <td  style="border:0;text-align:left"><input name="newpassword1" type="password" ></td></tr>
            <tr><td  style="border:0;text-align:right">Erneut das neue Passwort:</td>
                    <td  style="border:0;text-align:left"><input name="newpassword2" type="password" ></td></tr>
          </table>
          <input name="easid" value="{{easid}}" type="hidden" />
          <input value="Absenden" type="submit" />
        </form><br>
        {}
    </div>
'''.format(_('''Das neue Passwort muss mindestens 8 Zeichen lang sein.<br><br>
        <b>Tipps:</b><br>
        Verwenden Sie nie gleiche Passwörter für verschiedene Dienste.<br>
        Sonderzeichen sind gefährlich, da sie im Web nicht immer eindeutig übertragen werden.<br>
        Datenschutzinfo: Das eingegebene Passwort wird nicht im Klartext, sondern als "Hash mit Salt" gespeichert!'''))

   
def make_bc_hash(password):
    # try:
    # aktuelles bcrypt 2.0.0 nimmt bytes und gibt sie zurück
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
    # except:
    # im Fehlerfall vielleicht noch altes bcrypt, das strings nimmt & gibt
    #    return bcrypt.hashpw(password, bcrypt.gensalt())


def check_bc_pass(password, pw_hash):  
    # Parameter als Strings
    import bcrypt
    # try:
    # aktuelles bcrypt 2.0.0 nimmt bytes und gibt sie zurück
    return bcrypt.hashpw(password.encode('utf-8'), pw_hash.encode('utf-8')).decode('utf-8') == pw_hash
    # except:
    #    return bcrypt.hashpw(password, pw_hash) == pw_hash
    
     
def change_password(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    if ti_ctx.req.get_cgi("newpassword1"):
        erfolg, user, grund, rechte = ti_ctx.ses.passwordcheck(True)
        if erfolg:
            ti_ctx.ses.set_session_meldung('+'+_('Passwort wurde geändert!'))
            # md_response.append_s("<h3>  </h3>")
            md_response.submenu_finished=True
        else:   
            ti_ctx.ses.set_session_meldung('-'+_('Passwort wurde nicht geändert! Grund: ')+grund)
            md_response.append_s(skeleton2.format(deruser=ti_ctx.ses.get_session_user(), easid=ti_ctx.ses.get_session_id()))
    else:
        md_response.append_s(skeleton2.format(deruser=ti_ctx.ses.get_session_user(), easid=ti_ctx.ses.get_session_id()))
    return md_response

    
def handle_logout(ti_ctx, set_cookie=True):
    websessionid = ti_ctx.req.get_websessionid()
    hacklog("Starte handle_logOUT")
    md_response = ti_lib.Multi_Detail_Response()
    
    if websessionid and websessionid.isdigit():
        hacklog("Erfolg in handle_logOUT")
        wsid = int(websessionid)
        ti_ctx.ses.execute('DELETE FROM sessions WHERE id=?', (wsid, ))
        ti_ctx.ses.commit()
        ti_ctx.replace_session(TI_Session(ti_ctx.req, ti_ctx.cnf, ti_ctx.sys.get_epoche()))
        ti_ctx.ses.set_session_meldung('+'+_('Sie wurden erfolgreich abgemeldet'))
        # md_response.append_s('<h3>bye...</h3>')
    return md_response
    
    
def handle_login(ti_ctx, set_cookie=True):
    """gibt ein MultiResponse-Objekt zurück"""
    hacklog("Starte handle_login")
    md_response = ti_lib.Multi_Detail_Response()
    md_response.sessionid=0 
    
    # echte Anmeldung:
    erfolg, user, grund, rechte = ti_ctx.ses.passwordcheck()
    if erfolg:
        hacklog("Erfolg in handle_login")
        sessionid = str(random.randint(123456789, 987654321))
        # nach idR. 15 minuten verfällt die Session
        expires = str(ti_lib.get_now()+ti_ctx.cnf.get_db_config_int("Login_Duration", 15)*60)
        ti_ctx.ses.execute('INSERT INTO sessions (id, expires, username, rechte) VALUES (?, ?, ?, ?)', 
                           (sessionid, expires, user, rechte))
        ti_ctx.ses.commit()
        
        if set_cookie: 
            cookie = http.cookies.SimpleCookie()
            cookie['ti_sessionid2'] = sessionid
            ti_ctx.res.debug("Cookie.output", cookie.output())
            ti.prt(cookie.output()+"\n")
        
        ti_ctx.replace_session(TI_Session(ti_ctx.req, ti_ctx.cnf, ti_ctx.sys.get_epoche(), 
                                          set_cookie=False, force_sessionid=sessionid))
        if rechte:
            ti_ctx.ses.set_session_meldung('+' + _('Sie wurden erfolgreich angemeldet'))
        else:
            ti_ctx.ses.set_session_meldung('+' + _('''Sie wurden erfolgreich angemeldet, 
            haben aber noch keine Rechte vom Administrator erhalten'''))
        if rechte == 255:
            ti_ctx.ses.set_session_command('config.py')
        # md_response.append_s('<h3> </h3>')
        # hacklog("Fertig in Erfolg handle_login")
        md_response.submenu_finished=True
        md_response.sessionid = sessionid
        return md_response

    if len(ti_ctx.req.get_cgi("tiuser")) > 0:
        md_response.append_s('<div class="login"><h4>{}!</h4></div>'.format(_('Falscher Name oder Passwort')))
    
    md_response.append_s(skeleton)  # der Logindialog...
    rows = ti_ctx.ses.execute('SELECT count(name) FROM users')
    for row in rows:
        anzahl = row[0]
    if anzahl == 2:
        # Es gibt nur die User admin und vianetworks, (sonst wären sie oben neu angelegt worden und es wären mehr User)
        md_response.append_s('<div class="login">' +
                             _('''<h4>Tipp:</h4><p>Erster Useraccount nach der Installation: admin Standardpasswort:
                                  goti<br>Bitte nutzen und neues Passwort setzen sowie weitere User anlegen</p>''')+'</div>')
    hacklog("Ende von handle_login")
    return md_response

if __name__ == '__main__':
    ti_lib.quickabort2frames()
