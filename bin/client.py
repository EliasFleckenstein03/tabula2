#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 client.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 client.py verwaltet zentral 
        die Informationen über den 
        aufrufenden Client
            (Refaktoring 2019)
________________________________________________________________________
'''

# keine Importe!


class TI_Client():  # gemeint ist Hardwareclient, Browser
    def __init__(self, ip_address, ti_cnf, ti_response):
        self.ip_address = ip_address
        self.ti_cnf = ti_cnf
        # im Folgenden wird ggf. auch ein neuer Eintrag angelegt
        if self.ist_in_internal_network():
            ti_cnf.client_ping(self.ip_address, ti_response)

            # Initialisierung aus der Datenbank
            self.name, self.timestamp, self.minphase, self.maxphase, \
                self.gruppen, self.istanzg, self.cukopplung, self.forward   \
                = ti_cnf.client_get_all(self.ip_address)
        else:
            self.name, self.timestamp, self.minphase, self.maxphase, \
                self.gruppen, self.istanzg, self.cukopplung, self.forward = \
                "extern_"+self.ip_address, '', 0, 9, 0, False, '', ''
            ti_response.debug(f'Externer Aufrufer {self.ip_address}, keinen Client vermerken')
        if not self.gruppen & 256:
            self.forward = '' 
        
    # liefert Client-spezifische Grenzen für anzuzeigende
    # Pläne, erste Grenze < 0  invertiert Bereich

    def phaselimits(self):
        return self.minphase, self.maxphase

    def groups(self):
        return self.gruppen & 63, self.gruppen >> 6, self.forward
        
    def max_groups_number(self):
        gruppen = self.gruppen & 63
        cgn = 6
        while cgn > 0:
            if gruppen >= 1 << (cgn - 1):
                return cgn
            cgn -= 1
        return 0
        
    def ist_rpi(self):
        return (self.gruppen & 512) > 0
        
    def ist_lehrer(self):
        return self.gruppen & 32
            
    def ist_cukopplung(self):  # Flag für Kopplung von Client und User
        return self.cukopplung

    def ist_in_management_network(self):
        paar = self.ti_cnf.get_db_config("ManagementNetwork").split("/", 1)
        if len(paar) > 1:
            network, mask = paar
            return ip_in_network(network, mask, self.ip_address)
        else:
            return False
    
    def ist_in_internal_network(self):
        if ip_in_network('127.0.0.0', 8, self.ip_address) or \
                ip_in_network('10.0.0.0', 8, self.ip_address) or \
                ip_in_network('172.16.0.0', 12, self.ip_address) or \
                ip_in_network('192.168.0.0', 16, self.ip_address):
            return True
        return False
        
    def get_name(self):
        return self.name


################################
# Netzwerkcheck-Helper
def ip2number(ipaddress):
    ipbin = 0
    numbers = ipaddress.split(".")
    try:
        for i in range(4):
            # debug("ip-Komponente", numbers[i])
            ipbin = (ipbin << 8) + int(numbers[i])
            # debug("ipbin", ipbin)
        return ipbin
    except ValueError:
        return 0
        

def ip_in_network(network, mask, ip): 
    # network und ip sind Strings, die je 4 mit Punkt getrennte Zahlen beinhalten
    # mask ist ein String einer Zahl zwischen 0 und 32, 
    #  welche die Anzahl der führenden gesetzten Bits der Netzwerkmaske angibt (z.B. 24 für Class-C)
    
    # debug("ManagementNetwork", network)
    # debug("client ip", ip)
    # debug("mask", mask)
    n = ip2number(network)
    i = ip2number(ip)
    try:
        msk = int(mask)
    except ValueError:
        return False
    m = ((2 ** 32 - 1) >> (32 - msk)) << (32 - msk)
    # debug("n:" + str(n) + " i:" + str(i) + "Maske:" + bin(m))
    if n == 0 or i == 0:
        return False
    return (n & m == i & m)

