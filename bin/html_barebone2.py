# -*- coding: utf-8 -*-
# html_barebone.py
#
# enthält nur statische HTML-Inhalte, die tw. von JS ausgefüllt werden

root_forward = '''<!DOCTYPE html>
<head>
    <meta http-equiv="refresh" content="0; URL=/ti/do/frontend">
    <link rel="shortcut icon" type="image/x-icon" href="../static/pix/favicon.ico">
</head>
<body>forwarding...</body>'''

manage = '''<!DOCTYPE html>
<head>
    <link rel="shortcut icon" type="image/x-icon" href="../static/pix/favicon.ico">
</head>
<body>Managing</body>'''
        
frontend = '''<!doctype html>
<head>
    <title>t.i Anzeige</title>
    <link rel=stylesheet href="../static/css/frontend.css" type=text/css>
    <link rel="shortcut icon" type="image/x-icon" href="../static/pix/favicon.ico">
</head>
<body onload='ti_start()'>
<div id="contentp" class="tiP_Content">&nbsp;1&nbsp;</div>
<div id="contentm" class="tiM_Content">
    <div class="tiM_Status" id="contentmstatus">
        <div class="overlap_right"><span id="ti_time">10</span><span id="alerter">*</span></div>
        <span id="frontstatus"></span>
    </div>
    <div class="tiM_Square" id="square_m0">
        <div id="contentm0">&nbsp;...&nbsp;</div>
    </div>
    <div class="tiM_Square" id="square_m1">
        <div id="contentm1">&nbsp;...&nbsp;</div>
    </div>
</div>
<div style="float:none"> </div>

<div id="bodenvorhang"><div style="float:left" id="contentbottomleft"><h2>
    &nbsp;&nbsp;BOTTOM</h2></div><div style="float:right" id="contentbottomright"><h2>&nbsp;&nbsp;BOTTOM</h2></div></div>

<!-- Ende der visuellen Komponenten -->
<script src="../static/js/jquery.js" charset="utf-8"></script>
<script src="../static/js/ti2.js"     charset="utf-8"></script>
</body>
'''
