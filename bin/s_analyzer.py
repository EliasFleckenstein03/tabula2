#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 s_analyzer.py                                                         
 This file is part of tabula.info, which is free software under           
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_analyzer hat keine Interaktion via cgi bzw. http
 
 s_analyzer.py liest potentielle Vertretungspläne oder gleichwertige 
               Dateien ein, konvertiert ggf. und notiert das Resultat in 
               der Datenbank. frames.py muss diese Konfiguration nur noch 
               auslesen und darstellen lassen.

 Es wird die Datenbank /cgi-bin/data/configdb.sq3 mitgenutzt
    
 Pfade (/ steht für das Webserverrootverzeichnis):
    /local/data/upload enthält hochgeladene Dateien
                            Quelle dazu ist entweder s_any_upload
    /local/data/download enthält heruntergeladene  Dateien
                            Quelle dazu ist s_any_download
    /local/data/work   enthält die umbenannten Dateien aus upload und download
                            
    /local/www/dyn/<epoche>       Hier werden die aus den work-Dateien konvertierten Dateien
                            abgelegt und dann von frame.py eingebunden - sobald die 
                            Konvertierungen abgeschlossen sind 
                            (erst am Ende wird die neue Epoche getriggert)
                        
 s_analyzer.py macht einen kompletten Check des upload-Verzeichnisses, ist also recht "teuer".
    Daher sollte er nur nach einem erfolgten Up/Download oder durch einen 
    anderen Trigger - und nicht jeden Seitenaufruf - aufgerufen werden.
    - testet und verhindert weiteren Start durch do_lock (gemeinsamer lock mit up- und downloader)
    - es werden md5-Summen berechnet, in der DB abgelegt und verglichen. 
    - gibt es etwas zu tun:
        - übernimmt Dateien aus upload unter Vereinheitlichung der Namen
        - lege neues Verzeichnis /dyn/<epoche> an
        - konvertiere notwendige Dateien (md5sum oder getriggert) aus work dorthin, 
        - lege Reihenfolge & Anzahl fest
        - stelle dyn-Verzeichnis für frames.py um
    - gibt frei mit release_lock 
    
________________________________________________________________________

Namensprioritaeten

Die s_Dateien werden nach Regeln priorisiert und übernommen.
Entsprechend ihrer Priorität werden sie der Reihe nach angezeigt - soviele der client zeigen darf.
<Ziffer> beginnt bei Null und wird bei jedem Verwenden inkrementiert. Nach 9 wird abgebrochen.
* Ignoriert werden yyyy-mm-tt in der Vergangenheit oder mehr als 60 Tage in der zukunft sowie unbekannte Dateitypen
* Höchste Priorität haben Datumsangaben (yyyy-mm-tt) heute und in der Zukunft
--> werden in Datumsreihenfolge (alphabetisch) übernommen
--> Dabei werden Namen die "heute" enthalten (auch LZ_heute) und "morgen" eingeflochten
* es folgen sonstige Dateien in alphabetischer Reihenfolge
--> werden in anyplan<ziffer> mit ansteigenden Ziffern umgerechnet

Ergebnis ist eine Folge von Dateien, die nach dem Alphabet sortiert in der richtigen Reihenfolge
angezeigt werden können

_Ziellogik_

Verschiedene Ziele jenseits "alle Clients" lassen sich adressieren mit name_L.ext, 
also einem angehängten Unterstrich+Buchstaben vor dem Punkt und der Erweiterung.
Buchstabe: Ziel (gruppennummer) sind
        L: Lehrer (6)
        M: (z.B. Mensa) (5)
        B: (z.B. Bibliothek)(4)
        A: (z.B. Aula)(3)
        Q: Q-stufe (1 und 2)
        _: Default (sonstige/Schüler) 0

Diese Dateien  werden als solche erkannt und nur den entsprechenden Zielen angezeigt.
Gibt es Dateien für ein Ziel, so werden dort NUR diese Dateien angezeigt, 
gibt es keine, so werden dort die allgemeinen Dateien gezeigt.

_Dateitypen_

Htm(l): wird mit Javascript zum Scrollen angereichert und halbseitig gezeigt
pdf:    wird in png konvertiert, ggf. mehrere Seiten nebeneinander, Einzelseiten ggf. nebeneinander
csv:    wird als turbo-plan in html konvertiert, ggf. mehrspaltig
jp(e)g, gif, png:
        wird kopiert und vom Browser skaliert
url:    die in der ersten Zeile angegebene URL wird in einen iframe eingeblendet
doc(x): wird (wenn installiert) mit libreoffice in pdf konvertiert, dann weiter wie pdf

FIXME: Offen ist noch die Verteilung auf halbseitig bzw. ganzseitig. 
       Vorläufig: pdf-mehrseitig und turbo-lang im Vollbild
                  alle anderen halbseitig
        docX fehlt noch
'''

# Batteries
import copy
import shutil
import subprocess

# TI
import ti_lib
import s_turbo_import
import s_turbo

# Alle akzeptierten Erweiterungen
SOURCE_ERWEITERUNGEN = ['htm', 'html', 'pdf', 'csv', 'jpg', 'jpeg', 'png', 'gif', 'url', 'css']
# Alle Erweiterungen, die nach Konvertierung geändert sind
TARGET_ERWEITERUNGEN = {'pdf': 'png', 'csv': 'html'}
SUPPORT_ERWEITERUNGEN = ['css']  # geben keinen eigenen Plan, stehen aber im gleichen Verzeichnis zur Verfügung
ZIELCODES = ["_", "q", "x", "a", "b", "m", "l"]
MAXPIXINCOLUMN = 950
MAXPIXINSCREEN = 1920


class s_file():
    def __init__(self, filepath):
        """filepath ist ein pathlib.Path-Objekt"""
        # Dateiname speichern und analysieren
        self.filepath = filepath 
        self.path = filepath.parent
        self.name = filepath.name
        
        self.suchname = self.name.lower()
        self.fullname = filepath.as_posix()
        self.root = filepath.stem
        self.ext = filepath.suffix  
        if self.ext.startswith("."):    # impliziert nicht leer
            self.ext = self.ext[1:]
            
        self.behalten = self.ext.lower() in SOURCE_ERWEITERUNGEN  # nur solche, bei denen wir die Extension kennen, sind toll
        self.spalten = 1

    def loesche_dich(self):
        self.name = ''
        self.suchname = ''
        self.root = ''
        self.ext = ''
        self.behalten = False


class Any_Analyzer():
    def __init__(self, ti_cnf):
        ti_lib.log('ANY: INIT')
        self.ti_cnf = ti_cnf
        self.now_ts = int(ti_lib.get_now())
        self.uploadpath = ti_lib.get_ti_datapath('upload')
        self.workpath = ti_lib.get_ti_datapath('work')
        self.downloadpath = ti_lib.get_ti_datapath('download')
        self.toolpath = ti_lib.get_ti_toolpath()
        self.dyn_path = ti_lib.get_ti_dynpath()
        self.dyn_ts_path = self.dyn_path / str(self.now_ts)
        self.web_dyn_path = '../dyn/{ts}/'.format(ts=str(self.now_ts))
        self.scripts_path = ti_lib.get_ti_basepath() / 'scripts'
        self.turbo_export_filename = ti_lib.get_ti_datapath('tmp/export.html')
        self.turbo_export_filenamefull = ti_lib.get_ti_datapath('tmp/exportfull.html')
        
    def finden_und_md5vergleich(self):
        ist_neu = False
        keine_Dateien = True
        # alle Dateinamen in...
        file_collection = []
        for path in [self.uploadpath, self.downloadpath, self.toolpath]:
            try:
                for filepath in path.iterdir():
                    ti_lib.log('ANY: nächster Dateiname', filepath.name)
                    if not filepath.name.startswith('vplansemaphore'):
                        keine_Dateien = False
                    s_datei = s_file(filepath)
                    if s_datei.behalten:
                        ti_lib.log('ANY: -> wird genommen (', filepath.name, ')')
                        file_collection.append(s_datei) 
                    else:
                        ti_lib.log('ANY: -> wird ABGELEHNT (', s_datei.root, s_datei.ext, ')')
            except Exception as e:
                ti_lib.log('Any: Pfad nicht vorhanden?', path, "Exception:", e)
        # alle Dateien checken, ob bekannt & unverändert
        self.ti_cnf.s_files_uncheck()  # alle Markierungen löschen
        for s_datei in file_collection:
            md5alt = self.ti_cnf.s_files_get_md5(s_datei.fullname)  # liefert leeren String, wenn filename nicht in Datenbank
            md5neu = ti_lib.berechne_md5_von(s_datei.filepath)
            if md5alt != md5neu:
                ist_neu = True
            # print(s_datei.name+" md5: "+md5alt+" -> "+md5neu)
            self.ti_cnf.s_files_set_md5(s_datei.fullname, md5neu)  # setzt auch Markierung, daher auch für unveränderte wichtig
        anzahl = self.ti_cnf.s_files_count_n_delete_unchecked()
        # zählt markierte und entfernt alle md5sums ohne Markierung - also die nichtexistenter Dateien
        
        ti_lib.log('ANY: anzahl Dateien ergibt', anzahl)
        ti_lib.log('ANY: Keine Dateien sagt', keine_Dateien)
        # if anzahl:
        #    ist_neu = True    ## redundant und nur wenn irgendein md5sum verändert wurde relevant
        do_konvert = False
        if ist_neu:
            ti_lib.log('ANY: Neue Datei(en) gefunden')
            do_konvert = True
        if self.ti_cnf.get_db_config_bool('s_any_force_convert'):
            ti_lib.log('ANY: Konvertierung wurde erzwungen')
            do_konvert = True
        autoconvert_epoche = self.ti_cnf.get_db_config_int('s_any_autoconvert_epoche', 0)
        if autoconvert_epoche and autoconvert_epoche < ti_lib.get_now():
            self.ti_cnf.set_db_config('s_any_autoconvert_epoche', 0)
            ti_lib.log('ANY: Konvertierung war geplant via autoconvert')
            do_konvert = True
        if do_konvert or keine_Dateien:
            self.ti_cnf.set_db_config('s_any_force_convert', 0)
            return file_collection, keine_Dateien
        return None, keine_Dateien
        
    def kopiere_und_konvertiere(self, file_collection):
        filematrix = [[], [], [], [], [], [], []]
        # doppelte .. um auch aus dem dyn/123-Verzeichnis herauszukommen via count_subdirectories
        html_kopf = '''<!DOCTYPE html>\n<html>\n  <head>{}
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">\n  </head>\n<body>\n
                '''.format(ti_lib.standardcss(count_subdirectories=2))
        html_kopf_export = '''<!DOCTYPE html>\n<html>\n  <head>{}
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">\n  </head>\n<body>\n
                '''.format(ti_lib.standardcss(count_subdirectories=1))

        # Lösche das work-Verzeichnis
        for filepath in self.workpath.iterdir():
            filepath.unlink()
            
        if file_collection:
            file_collection.sort(key=lambda loopobj: loopobj.suchname)  # Ausreichend optimiert für kleine Anzahl

            # Datumspräfix finden
            for i in range(61):  # Datum von heute bis heute+ 60 Tage durchtesten
                testdatum = ti_lib.isodatum_heute_plus_delta(i)
                for s_datei in file_collection: 
                    if (i == 0 and 'heute' in s_datei.root) or \
                            (i > 0 and 'morgen' in s_datei.root) or \
                            (s_datei.suchname.startswith(testdatum)):
                        self.vermerke(filematrix, s_datei)
                        s_datei.loesche_dich()

            # Andere Datumspräfixe ignorieren
            for s_datei in file_collection:
                if (not ti_lib.ist_isodatum(s_datei.name[:10])) and s_datei.behalten:
                    # denn akzeptable Daten wären schon erfasst
                    self.vermerke(filematrix, s_datei)
                    s_datei.loesche_dich()
                                
            # Kopiere nun zuerst alle Dateien
            for zielnummer in range(7):
                zielliste = filematrix[zielnummer]
                for s_datei in zielliste:
                    # ti_lib.log("S:"+self.uploadpath+s_datei.name+"\nT:"+self.self.workpath+s_datei.workname)
                    try:
                        shutil.copyfile(s_datei.fullname, self.workpath / s_datei.workname)
                    except Exception:
                        pass
            
            # Stand: Nun sind genau die zu nutzenden Dateien mit Standardnamen im work-Verzeichnis
            
        s_any_dauer1 = self.ti_cnf.get_db_config_int('s_any_dauer1', 90)
        s_any_dauer2 = self.ti_cnf.get_db_config_int('s_any_dauer2', 125)
        # konvertiere/kopiere nun nach Bedarf
        self.dyn_ts_path.mkdir()  # Anlegen, weil auch was passiert
        any_erfolg = False
        is_any_file = False
        for zielnummer in range(7):
            if zielnummer == 2:
                continue
            phase = 0
            for s_datei in filematrix[zielnummer]:
                is_any_file = True
                ti_lib.log('ANY: ', zielnummer, s_datei.name, s_datei.ext)
                zielerfolg = False  # Erfolg für dieses Ziel muss erkannt werden
                weite = 0
                if s_datei.ext in SUPPORT_ERWEITERUNGEN:
                    shutil.copyfile(self.workpath / s_datei.workname, self.dyn_ts_path / s_datei.targetname)
                    # zielerfolg wird nicht gesetzt, weil die Datei nicht als Plan gilt!!
                elif s_datei.ext == 'pdf':
                    zielerfolg, weite, qf = self.externes_script('s_any_pdf_convert.sh', s_datei.workname, s_datei.targetname)
                    if weite > MAXPIXINCOLUMN:
                        s_datei.spalten = 2
                elif s_datei.ext in ['html', 'htm']:
                    zielerfolg, spaltenzahl = self.copy_html_injecting(s_datei.workname, s_datei.targetname)
                    if spaltenzahl == 2 or self.ti_cnf.get_db_config_bool('flag_s_any_html_ganzseitig', False):
                        s_datei.spalten = 2
                elif s_datei.ext in ['png', 'gif', 'jpg', 'jpeg']:
                    try:
                        shutil.copyfile(self.workpath / s_datei.workname, self.dyn_ts_path / s_datei.targetname)
                        zielerfolg, weite, qf = self.externes_script('s_any_picwidth.sh', s_datei.targetname)
                        if weite > MAXPIXINCOLUMN:
                            s_datei.spalten = 2
                    except Exception:
                        pass
                elif s_datei.ext == 'csv':
                    ti_lib.log('ANY_turbo: csv erkannt')
                    
                    ti_lib.log('ANY_turbo: s_turbo importiert')
                    s_turbo_import.read_table(filename=self.workpath / s_datei.workname)
                    ti_lib.log('ANY_turbo: vplan eingelesen')
                    ted_array = s_turbo.S_Interface(self.ti_cnf).export_any()
                    if ted_array and ted_array[0].columns > 0:  # lässt turbos regelmäßig neu konvertieren 
                        ti_lib.log('ANY_turbo: min 1 column erzeugt')
                        self.ti_cnf.set_db_config('s_any_autoconvert_epoche',
                                                  int(ti_lib.get_now() +
                                                      self.ti_cnf.get_db_config_int('s_any_autoconvert_intervall_min', 20) *
                                                      60))
                    else:
                        ti_lib.log('ANY_turbo: keine column erzeugt')
                    # erzeuge Exportdatei zum Veröffentlichen im Internet
                    with self.turbo_export_filename.open(mode='w', encoding="utf-8") as fout:
                        fout.write(html_kopf_export)
                        if len(ted_array) < 2:
                            leftclass, rightclass = '', ''
                        else:
                            if ted_array[0].columns == ted_array[1].columns:
                                leftclass, rightclass = 's_turbo_export49', 's_turbo_export49'
                            elif ted_array[0].columns > ted_array[1].columns:
                                leftclass, rightclass = 's_turbo_export66', 's_turbo_export33'
                            else:
                                leftclass, rightclass = 's_turbo_export33', 's_turbo_export66'
                        fout.write('\n<div class="'+leftclass+'">\n')
                        if len(ted_array) > 0:
                            for line in ted_array[0].htmlex.split("\n"):
                                ti_lib.log(line)  # FIXME
                                fout.write(line)
                                # fout.write(ted_array[0].htmlex)
                        if len(ted_array) > 1:
                            fout.write('\n</div>\n<div class="'+rightclass+'" style="float:right;">\n')
                            fout.write(ted_array[1].htmlex)
                        fout.write('\n</div>\n')
                        fout.write('</body>')
                        fout.close()
                    # erzeuge Exportdatei für Lehrer
                    with self.turbo_export_filenamefull.open(mode='w', encoding="utf-8") as foutfull:
                        foutfull.write(html_kopf_export)
                        if len(ted_array) < 2:
                            leftclass, rightclass = '', ''
                        else:
                            if ted_array[0].columns == ted_array[1].columns:
                                leftclass, rightclass = 's_turbo_export49', 's_turbo_export49'
                            elif ted_array[0].columns > ted_array[1].columns:
                                leftclass, rightclass = 's_turbo_export66', 's_turbo_export33'
                            else:
                                leftclass, rightclass = 's_turbo_export33', 's_turbo_export66'
                        foutfull.write('\n<div class="'+leftclass+'">\n')
                        if len(ted_array) > 0:
                            foutfull.write(ted_array[0].html)
                        if len(ted_array) > 1:
                            foutfull.write('\n</div>\n<div class="'+rightclass+'" style="float:right;">\n')
                            foutfull.write(ted_array[1].html)
                        foutfull.write('\n</div>\n')
                        foutfull.write('</body>')
                        foutfull.close()
                    # ti_lib.log('ANY: ', str(ted_array))
                    # erzeuge eigentliche Anzeigedateien
                    loopcounter = 0
                    for ted in ted_array:
                        if ted.columns > 0:
                            with (self.workpath / (s_datei.workname+'.tmp')).open(mode='w', encoding="utf-8") as fout:
                                fout.write(html_kopf)
                                fout.write(ted.html)
                                fout.write('</body>')
                                
                            mytargetname = s_datei.targetname[:8]+str(loopcounter)+s_datei.targetname[8:]
                            self.copy_html_injecting(s_datei.workname+'.tmp', mytargetname)
                            zielerfolg = True
                            s_datei.spalten = ted.columns
                            ti_lib.log("ANY: Speichere erfolgreiche Konvertierung von", mytargetname, 'mit Weite', weite)
                            self.ti_cnf.set_s_any_vplan(ti_lib.S_any_vplan(self.now_ts, zielnummer,
                                                                           phase, s_datei.spalten,
                                                                           self.web_dyn_path + mytargetname,
                                                                           s_any_dauer1 if ted.columns < 2 else s_any_dauer2,
                                                                           weite))
                            phase += 1
                            loopcounter += 1
                            any_erfolg = True
                elif s_datei.ext == 'url':
                    with (self.workpath / s_datei.workname).open(mode='r', encoding='utf-8') as urldatei:
                        exturl = urldatei.readline().strip()
                        if exturl.lower().startswith('http'):
                            s_datei.targetname = exturl
                            s_datei.spalten = 2
                            zielerfolg = True
                else:
                    ti_lib.log("ANY: Ooops - Dateityp nicht zugeordnet?", s_datei.ext)
                if zielerfolg and not s_datei.ext == 'csv':
                    ti_lib.log("ANY: Speichere erfolgreiche Konvertierung", s_datei.targetname)
                    self.ti_cnf.set_s_any_vplan(
                        ti_lib.S_any_vplan(self.now_ts, zielnummer,
                                           phase, s_datei.spalten,
                                           (self.web_dyn_path if s_datei.ext != 'url' else '') + s_datei.targetname,
                                           s_any_dauer1 if s_datei.spalten < 2 else s_any_dauer2,
                                           weite))        
                    phase += 1
                    any_erfolg = True
        if any_erfolg or not is_any_file:  # Fix für "keine Pläne"
            keepepochen = self.ti_cnf.set_s_any_aktuelle_epoche(self.now_ts)
            ti_lib.log("ANY: Epoche gesetzt auf ", self.now_ts, "Keep:", keepepochen)
            for filepath in self.dyn_path.iterdir():
                if filepath.name.isdigit() and filepath.name not in keepepochen: 
                    shutil.rmtree(filepath, ignore_errors=True)
        else:
            ti_lib.log("ANY: Kein Erfolg beim Konvertieren, lösche Unterverzeichnis ", self.dyn_ts_path.as_posix())
            shutil.rmtree(self.dyn_ts_path)  # bei Misserfolg keinen Müll hinterlassen

    def externes_script(self, a, b, c=''):  # Parameter werden automatisch um Pfade ergänzt
        kommando = '{} {} {}'.format(self.scripts_path / a, self.workpath / b, self.dyn_ts_path / c if c else '')
        try:
            stdout = subprocess.check_output(kommando, shell=True, stderr=subprocess.STDOUT).decode("utf-8", "ignore")
            # ti_lib.log('ANY extern:\n', stdout)
            externe_weite = 0  # dummywert
            querformat = False  # dummywert
            for zeile in stdout.split("\n"):
                if zeile:
                    ti_lib.log('ANY extern: ->', zeile)
                    if zeile.startswith("S_ANY_BREITE"):
                        zeile = zeile[13:].strip()
                        if zeile.isdigit():
                            if len(zeile) > 7:  # ein animated gif funkt mit wiederholten Weiten rein...
                                if zeile[0:3] == zeile[3:6]:
                                    externe_weite = int(zeile[0:3])
                                elif zeile[0:2] == zeile[2:4]:
                                    externe_weite = int(zeile[0:2])
                                else:
                                    externe_weite = int(zeile[0:4])
                            else:
                                externe_weite = int(zeile)
                    elif zeile.startswith('S_ANY_QUERFORMAT'):
                        querformat = True
            return True, externe_weite, querformat
        except subprocess.CalledProcessError as err:
            ti_lib.log('ANY extern Exception:\n', err.output.decode("utf-8", "ignore"))
        return False, 0, False

    def html_split(self, chunk, lowered, max_zeilen):
        table_von, table_bis = self.get_tag_limits(lowered, b'<table', 1)
        extra_tag = self.ti_cnf.get_db_config('s_any_html_split_extratag', '').encode('utf-8')  # z.B. center
        extra_tag = extra_tag.replace(b'<', b'')
        extra_tag = extra_tag.replace(b'>', b'')
        if extra_tag:
            extra_untag = b'\n</'+extra_tag.split(b' ')[0]+b'>\n'
            extra_tag = b'\n<'+extra_tag+b'>\n'
        else:
            extra_untag = b''
            extra_tag = b''
        zeilenanzahl = lowered.count(b'<tr')
        ti_lib.log('ANY html split, da TabellenZeilenZahl', zeilenanzahl, '>', max_zeilen)
        tr_pos = table_von  # vorher brauchen wir nicht suchen
        for tr_counter in range(zeilenanzahl//2+1):
            tr_pos = self.get_tag_beginn(lowered, b'<tr', tr_pos+1)
        # jetzt kennen wir die mittlere aller Zeilen! Nun muss die passende Tabellendefinition gefunden werden
        table_von_neu = table_von
        table_bis_neu = table_bis
        while table_von_neu > 0 and table_von_neu < tr_pos:
            table_von, table_bis = table_von_neu, table_bis_neu
            table_von_neu, table_bis_neu = self.get_tag_limits(lowered, b'<table', table_bis_neu)
        # jetzt ist in table_von/bis die vorletzte Tabellendefinition drin -> win
        table_definition = chunk[table_von: table_bis]
        body = self.get_tag_ende(lowered, b'<body')+1
        unbody = self.get_tag_beginn(lowered, b'</body')
        
        chunk = chunk[:body] + \
            b'\n<div style="float:left;margin:0.3em;width: 49.5%; width: calc(50% - 1em);">' + \
            b'<!-- Eingefuegt von s_analyzer -->\n' + \
            chunk[body:tr_pos] + \
            b'\n</table>' + extra_untag + \
            b'\n</div><!-- Eingefuegt von s_analyzer -->\n' + \
            b'<div style="float:left;margin:0.3em;width: 49.5%; width: calc(50% - 1em)">\n' + \
            extra_tag + \
            table_definition + \
            chunk[tr_pos:unbody] + \
            b'\n</div><div style="clear:left" /><!-- Eingefuegt von s_analyzer -->\n' + \
            chunk[unbody:]
        # '<div style="float:left;margin:0.3em;">'
        return chunk
                  
    def get_tag_limits(self, html, tag, sucheab=0):
        
        von = html.find(tag, sucheab)
        bis = html.find(b'>', von)+1
        # ti_lib.log('debug, any, self.get_tag_limits von', tag, html[von:bis])
        return von, bis
        
    def get_tag_beginn(self, html, tag, sucheab=0):
        return html.find(tag, sucheab)

    def get_tag_ende(self, html, tag, sucheab=0):
        return html.find(b'>', self.get_tag_beginn(html, tag, sucheab))
            
    def copy_html_injecting(self, source, target):  # jeweils + anywork bzw. self.dyn_ts_path davor
        spaltenzahl = 1
        with (self.workpath / source).open(mode="rb") as fin:
            chunk = fin.read(1000000)                 # limited to 1M
        if not chunk:
            return False, 0

        do_scroll = self.ti_cnf.get_db_config_bool('flag_s_any_html_do_scroll', True)
        max_zeilen = self.ti_cnf.get_db_config_int('s_any_html_max_tabellenzeilen', 9999)
        htmlscanerror = b''
        lowered = chunk.lower()
        if max_zeilen > 10 and max_zeilen < 1000 and lowered.count(b'<tr') > max_zeilen:
            chunk = self.html_split(chunk, lowered, max_zeilen)
            lowered = chunk.lower() 
            spaltenzahl = 2
        positionhead = lowered.find(b'<head')  # finde Beginn des head-Tags
        positionbodytag = lowered.find(b'<body')
        if positionbodytag >= 0:
            positionbodytag += 5  # finde Beginn des Inneren des body-Tags
        if positionhead < 0:  # kein head gefunden
            positionhead = 0
            htmlscanerror += b'\n<!-- Oops, s_analyzer hat kein HTML-HEAD-TAG gefunden -->\n'
        else:
            positionhead = lowered.find(b'>', positionhead+4)+1  # finde erstes Zeichen nach Ende des head-Tags
            
        if do_scroll:
            if positionbodytag < 4:  # kein body gefunden
                positionbodytag = len(chunk)
                js = b''
                onload = b''
                htmlscanerror += b'\n<!-- Oops, s_analyzer hat kein BODY-TAG gefunden -->\n'
            else:
                js = b'\n\t<script src="../../static/js/scroll.js" type="text/javascript"></script>\n'
                # da die Datei aus dem dyn/1233/-Unterverzeichnis gestartet wird muss man zweimal raus
                onload = b' onload="scrollstart()" style="margin:0px;" '
            
        else:
            js = b'\n\t<!-- s_analyzer entscheidet: kein scrolling -->\n'
            onload = b''
        chunk2 = chunk[:positionhead]+js+chunk[positionhead:positionbodytag]+onload+chunk[positionbodytag:]+htmlscanerror

        if self.ti_cnf.get_db_config_bool('flag_s_any_html_remove_refresh', True):
            # entferne nun <meta http-equiv=“refresh“ content=“4; URL=subst_001.htm“>
            import re
            chunk3 = re.sub(b'<meta\\s+http-equiv\\s*=\\s*"refresh".*?>+', b'<!--s_any_hat_refresh_entfernt --> ',
                            chunk2, flags=re.DOTALL+re.IGNORECASE)
            # erlaubt viele Whitespaces und ignoriert Groß/Kleinschrift - löscht bis zum nächsten schließenden >
        else:
            chunk3 = chunk2  # nur ein pointer
        if htmlscanerror:
            ti_lib.log('c_HTML_i:', htmlscanerror.decode('utf-8'))    
        with (self.dyn_ts_path / target).open(mode='wb') as fout:
            fout.write(chunk3)
        return True, spaltenzahl
     
    # vermerke bekommt die Datenstruktur, eine s_file-Objekt und ggf. einen Ziel-Dateinamen übergeben
    # das s_file-Objekt wird entsprechend in die Datenstruktur kopiert, 
    # # damit das Original in seinem Array gelöscht werden kann.
    def vermerke(self, filematrix, s_datei_orig):
        if not s_datei_orig.behalten:
            ti_lib.log('ANY vermerke: gelöschter Eintrag wird nicht vermerkt')
            return
        s_datei = copy.deepcopy(s_datei_orig)  # damit es kein Verweis ist
        zielnummer = 0
        zielcode = ZIELCODES[0]  # für default
        if len(s_datei.root) > 1 and s_datei.root[-2] == '_':
            zielcode = s_datei.root[-1].lower()
            if zielcode in ZIELCODES:
                zielnummer = ZIELCODES.index(zielcode)
            else:
                zielcode = ZIELCODES[0]  # also doch default
        # ti_lib.log(f"Für vermerke ermittelt aus{s_datei.root}: zielnummer{zielnummer}")
        # workname ist der Dateiname im work-Verzeichnis
        if s_datei.ext in SUPPORT_ERWEITERUNGEN:
            s_datei.workname = s_datei.name
            s_datei.targetname = s_datei.name
        else:
            s_datei.workname = 'anyplan{nr}_{ziel}.{ext}'.format(
                    nr=str(len(filematrix[zielnummer])), 
                    ziel=zielcode, 
                    ext=s_datei.ext)
            s_datei.workname2 = 'anyplan{nr}b_{ziel}.{ext}'.format(
                    nr=str(len(filematrix[zielnummer])), 
                    ziel=zielcode, 
                    ext=s_datei.ext)
            # targetname ist der Dateiname im dyn-Verzeichnis, ggf. durch Konvertierung mit anderer Endung
            if s_datei.ext in TARGET_ERWEITERUNGEN:
                s_datei.targetname = 'anyplan{nr}_{ziel}.{ext}'.format(
                    nr=str(len(filematrix[zielnummer])), 
                    ziel=zielcode, 
                    ext=TARGET_ERWEITERUNGEN[s_datei.ext])
            else:
                s_datei.targetname = s_datei.workname
        lenfmzn = len(filematrix[zielnummer])
        filematrix[zielnummer].append(s_datei)
        ti_lib.log(f'ANY vermerkte Nr:{lenfmzn}@{zielcode} = {s_datei.name} via '
                   f'{s_datei.workname} als {s_datei.targetname}, {s_datei.root}...{s_datei.ext}')
                                                                  

def do_analyze(ti_cnf):
    aa = Any_Analyzer(ti_cnf)
    try:
        # fc: file_collection
        fc, keine_Dateien = aa.finden_und_md5vergleich()
        if fc or keine_Dateien:
            aa.kopiere_und_konvertiere(fc)
        else:
            ti_lib.log('ANY: Nichts zu tun')
        ti_lib.log('ANY: FERTIG')
    except Exception as e:
        ti_lib.log('ANY: Konvertierung lieferte Exception:\n', ti_lib.beschreibe_exception(e))


if __name__ == '__main__':
    import ti
    ti_ctx = ti.master_init()
    print("Content-Type: text/html\n\n")
    
    print(do_analyze(ti_ctx.cnf))
