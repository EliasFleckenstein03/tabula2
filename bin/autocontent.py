#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 autocontent.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 autocontent.py liefert quasi-statische Informationen wie Uhrzeit, 
 Eigenwerbung und interne Meldungen
________________________________________________________________________
'''
# Batteries
import random
# TI
import ti
import konst


def get_branding(ti_ctx):
    ti_ctx.res.debug('Flag_HideTI',ti_ctx.cnf.get_db_config_bool('Flag_HideTI', False))
    if not ti_ctx.cnf.get_db_config_bool('Flag_HideTI', False):
        return konst.TI_BRANDING
    return ""


def get_autocontent(ti_ctx, countdown, hochkant=False):
    # contenttime: divs sind definiert mit float:right und werden daher in umgekehrter Reihenfolge dargestellt
    if not hochkant:
        atime = ti.get_string_from_epoche(pre="Es ist ", middle=", der ", post=" Uhr.")
    else:
        atime = ti.get_string_from_epoche(middle="", dtsep=" - ")
    contenttime = '''
        <div class="navi-div-cd" id="ti_time">{countdown}s</div>
        <div class="navi-div-zeit"           >{time}</div>
        '''.format(time=atime,
                   countdown=countdown,
                   random=random.random())
    st_l = ti_ctx.cnf.get_db_config("SubTextL", "").strip()
    st_r = ti_ctx.cnf.get_db_config("SubTextR", "").strip()
    base_l = ""
    base_r = ""
    if ti_ctx.cnf.get_db_config_bool("Flag_IPeinblenden"):
        base_r = f'\n<div class="firstcontent">{ti_ctx.req.client.name} ({ti_ctx.req.client.ip_address})</div>\n'
    if len(st_l):
        base_l = '\n<div class="firstcontent">{internalinfo}</div>\n'.format(internalinfo=st_l)
    if len(st_r):
        base_r += '\n<div class="firstcontent">{internalinfo}</div>\n'.format(internalinfo=st_r)

        
    return contenttime, base_l, base_r

