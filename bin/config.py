#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 config.py                                                        
 This file is free software under         
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist  Freie Software gemäß
             der GPL ohne jegliche Garantie - siehe die Datei COPYING 
________________________________________________________________________

 config.py vereint den Dialog und die Tätigkeiten zur Konfiguration
________________________________________________________________________
'''

# Batteries
import lzma
import sys

# TI
import ti
import ti_lib
import konst
    

def htmloptionenzeile(bitmuster, bez_fkt, url, 
                      inputname, hiddenname, 
                      hiddenvalue, sid, autobreak=False):
    '''gibt 
        Buttons aus, die nach dem 
        Bitmuster eingefärbt sind und mit den 
        kuerzeln, welche die bez_fkt als zweiten Wert zurückliefert, beschriftet sind.
    Ein Klick ruft die 
        url auf, wobei drei Werte übertragen werden: versteckt der
        hiddenname mit dem hiddenvalue, sowie
        easid mit dem sid sowie als
        inputname der angeklickte Wert (eine Zweierpotenz)'''
    
    liste = ['\n\t<!-- eine htmloptionenzeile aus config.py beginnt -->']
    staticcontent = f'''
    <div style="float:left;width:{{weite}}em;text-align:right;">
        <form action="{url}" method="post" >
            <input name="{hiddenname}" value="{hiddenvalue}" type="hidden" />
            <input name="easid" value="{sid}" type="hidden" />'''
    
    for i in range(20):
        gn, gk = bez_fkt(i)  # GruppenName, GruppenKürzel
        if gk:
            bitwert = 1 << i
            wert = bitmuster & bitwert
            weite = 1.5
            weite += 0.6*len(gk)
            liste.append(staticcontent.format(weite=weite))
            liste.append(f'''
            <input name="{inputname}" value="{str(bitwert)}" type="hidden" />
            <input style="background:{'green' if wert else 'orange'}
            {';' if not wert else ';font-weight:bold;'}" value="{gk}" type="submit" />
        </form>
    </div>''')
    liste.append('\n\t<!-- eine htmloptionenzeile aus config.py endet -->\n')
    return '\n'.join(liste)


def config_dialog(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    checkedsessionid = ti_ctx.get_checkedsessionid()
    kats = konst.get_configkategorien(ti_ctx.cnf.get_db_config_bool('flag_turboplan', False))
    # get... liefert ein Array aus Tupeln (kategoriename, config_struct), ggf. inkl. Turbo
    kategoriekastendefinition = '''\t<div class="kategorie">\n'''
    for kname, cstruct in kats:
        if kname == "Plananzeige":  # Willkürlicher Wechsel in die nächsten man_details...
            md_response.next_abschnitt()
        md_response.append_s(kategoriekastendefinition)
        md_response.append_s(f'\t\t<h3>Kontext: {kname}</h3>\n')
        cstruct.sort()
        for parameter, chelp, cstandard in cstruct:
            if parameter.startswith('!INFO'):
                md_response.append_s(f'''
                \n\t\t\t<div style="float:left;width:35em;text-align:justify;">{chelp}</div>
                \n\t\t\t<div class="unfloatleft"></div>''')
                continue
            chelp += '<br>Standardwert: ' + (cstandard if cstandard else '&lt;keiner&gt;')
            if parameter[0] == '+':
                eingabelaenge = 25
                maximallaenge = 250
                parameter = parameter[1:]
            else:
                eingabelaenge = 10
                maximallaenge = 50
            
            # Behandlung von Flags/boolschen Werten
            if parameter.lower()[:5] == 'flag_':
                anzeigeparameter = parameter[5:]
                if cstandard.lower() in ['1', 'ja', 'yes', 'true']:
                    cstandard = '1'
                else:
                    cstandard = '0'
                # klar auf nur "True" oder "False" setzen...
                alterwert = ti_ctx.cnf.get_db_config_bool(parameter, False)
                if alterwert not in ['1', '0']:
                    if alterwert:           # irgendwas stand drin, also True
                        ti_ctx.cnf.set_db_config(parameter, 'True')
                    else:                   # nichts stand drin, also Standardwert
                        ti_ctx.cnf.set_db_config(parameter, cstandard) 
            else:
                anzeigeparameter = parameter
                # jeder Parameter, der erklärt wird, soll auch angelegt sein:
                if ti_ctx.cnf.get_db_config(parameter, 'keinWert!') == 'keinWert!':
                    ti_ctx.cnf.set_db_config(parameter, cstandard) 

            if anzeigeparameter.startswith('s_any_pdf_'):
                anzeigeparameter = anzeigeparameter[10:]
            elif anzeigeparameter.startswith('s_any_'):
                anzeigeparameter = anzeigeparameter[6:]
            elif anzeigeparameter.startswith('s_turbo_'):
                anzeigeparameter = anzeigeparameter[8:]

            wert = ti_ctx.cnf.get_db_config(parameter)  # muss jetzt klappen
            
            md_response.append_s(f'''
            <div class="inputdiv">
                <form action="manage?task=config" method="post" >
                    {anzeigeparameter}
                    <input name="paerchen_parameter" value="{parameter}" type="hidden">''')
            if parameter.lower()[:5] == 'flag_':
                checked = " checked=checked " if ti.strg2bool(wert) else ""
                md_response.append_s(f'''
                    <input name="paerchen_wert"  type="checkbox" {checked} />''')
            else:
                md_response.append_s(f'''
                    <input name="paerchen_wert" value="{wert}" 
                    type="text" size="{str(eingabelaenge)}" 
                    maxlength="{str(maximallaenge)}"  class="eingabe" />''')
            md_response.append_s('''
                    <input name="easid" value="{checkedsessionid}" type="hidden"><input value="OK" type="submit">
                </form>
            </div>
            <div class="buttonsdiv">
                <form action="manage?task=config" method="post" >
                    <input name="paerchen_parameter" value="{parameter}" type="hidden" />
                    <input name="paerchen_wert" value="{cstandard}" type="hidden" />
                    <input name="easid" value="{checkedsessionid}" type="hidden">
                    <input value="Standard" type="submit">
                    {poph}
                </form>
            </div>'''.format(checkedsessionid=checkedsessionid, 
                             parameter=parameter, 
                             cstandard=cstandard, 
                             poph=popuphilfe(parameter, chelp)))
            
            md_response.append_s('<div class="unfloatleft"></div>')
            
        md_response.append_s('</div>')
        
    # # Benutzerverwaltung
    md_response.next_abschnitt()
    md_response.append_s(kategoriekastendefinition)
    md_response.append_s('<h3>Kontext: ' + 'Benutzerverwaltung' + '</h3>\n')
    import login
    alleuser = ti_ctx.ses.get_userlist()
    ti_ctx.res.debug('AlleUser:', str(alleuser))
    for tupel in alleuser:
        name, rechte = tupel
        ti_ctx.res.debug('name und rechte', name + '#' + str(rechte))
        rechtenamenhilfe = 'Aktueller Stand:<br><br>'
        for i in range(10):
            rn, rk = login.get_rechte_bez(i)
            if rn:
                rechtenamenhilfe += rk + ': ' + rn + ' ' + \
                                    ('erlaubt' if rechte & login.get_rechte_nr(i) else 
                                     'verboten') + '<br>'
    
        md_response.append_s('<div style="float:left;width:6em;text-align:right;">')       
        md_response.append_s(name + '&nbsp;&nbsp;')
        md_response.append_s('</div>')
        
        md_response.append_s(htmloptionenzeile(rechte, login.get_rechte_bez, 
                                               'manage?task=config', 'rechtetoggle', 'username', 
                                               name, checkedsessionid))
        md_response.append_s('<div style="float:left;">')       
        md_response.append_s(popuphilfe('Benutzerrechte', rechtenamenhilfe))
        md_response.append_s('</div>')
        
        if name != 'vianetwork':
            # # Passwort zurücksetzen
            md_response.append_s(f'''<div style="float:left;">
            <form action="manage?task=config" method="post" >&nbsp;&nbsp;
            <input name="user2reset" value="{name}" type="hidden" />
            <input name="easid" value="{checkedsessionid}" type="hidden" />
            <input value="PW" type="submit" />
            </form></div>''')
            # # User löschen
            md_response.append_s('<div style="float:left;">')       
            md_response.append_s('<form action="manage?task=config" method="post" >&nbsp;&nbsp;')
            md_response.append_s('<input name="user2delete" value="' + name +
                                 '" type="hidden" />')
            md_response.append_s('<input name="easid" value="' + checkedsessionid +
                                 '" type="hidden" />')
            md_response.append_s('''<input value="Del" type="submit" 
                style="background-color:red;color:white;font-weight:bold;" />
            </form></div>
            <div style="float:left;">''')
            md_response.append_s(popuphilfe('Passwort rücksetzen und User löschen', '''
            Das Passwort wird auf den Namen "tabula.info" zurückgesetzt. <br>
            Ein User, der nicht mehr benötigt wird, kann hier ohne Rückfrage gelöscht werden!'''))
            md_response.append_s('</div>')
        else:
            md_response.append_s('''<div style="float:left;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
            <div style="float:left;">''')       
            md_response.append_s(popuphilfe('kein Passwort, kein Löschen', '''Die Berechtigung
            wird in der ti-Basiskonfiguration über das "ManagementNetwork" geregelt. 
            Hier wird nur festgelegt, welche Rechte so ein Benutzer erhält. 
            Diese Rechte kumulieren mit seinen durch Anmeldung nachgewiesenen Rechten.'''))
            md_response.append_s('</div>')
            
        md_response.append_s('<div class="unfloatleft"></div>')

    md_response.append_s('''<div style="float:left;">
    <form action="manage?task=config" method="post" >
    Neuer User: 
    <input name="newuser" value="neuername" type="text" size="10" maxlength="16"  class="eingabe"/>''')
    md_response.append_s('<input name="easid" value="' + checkedsessionid + 
                         '" type="hidden"><input value="Anlegen" type="submit">')
    md_response.append_s('</form>')
    md_response.append_s('</div><div class="unfloatleft"></div>')
        
    md_response.append_s('</div>')  # schließt Benutzerverwaltungs-kategorie
    md_response.next_abschnitt()
    md_response.append_s('''
        <div class="kategorie" style="text-align:justify;color:blue;float:right;">
            <p>Anleitung:<br>
            Jede einzelne Eingabemöglichkeit hat ihre eigene Hilfe.<br>
            Es wird jeweils ein typischer Standardwert vorgeschlagen<br>
            Jede einzelne Änderung muss mit Klick auf "OK" o.ä. abgeschickt werden!</p>
            <p>Bei Problemen können Sie eine <a href="debugfile" target="_blank">Datei</a> 
            mit Informationen zu Ihrem System erstellen lassen. Diese dann in einer Mail an den Programmautor
            im Anhang versenden.</p>
        </div><!--Ende Anleitungskasten-->
''')
    return md_response
    

def popuphilfe(titel, hilfetext):
    return f'''
            <div class="tooltip">
                <span class="fragezeichen">?</span><span class="tooltiptext"><b>{titel}</b><br>{hilfetext}</span>
            </div>'''
    
    
def config_todo(ti_ctx):
    eapp = ""
    easid = ti_ctx.req.get_cgi('easid')
    ti_ctx.res.debug('easid', easid)
    if len(easid) > 5:
        if easid == ti_ctx.get_checkedsessionid():
            # Ab hier werden die Eingaben verarbeitet
            eapp = ti_ctx.req.get_cgi('paerchen_parameter')
            if len(eapp) > 0:
                eapw = ti_ctx.req.get_cgi('paerchen_wert')
                if eapp.startswith('s_any_') or eapp.startswith('flag_s_any_'):
                    ti_ctx.cnf.set_db_config('s_any_force_convert', 1)
                    ti_ctx.cnf.set_db_config('background_timeout', 0)
                if eapp[:5].lower() == 'flag_':
                    ti_ctx.res.debug('eapw', eapw)
                    if eapw in ['1', 'on']:
                        ti_ctx.cnf.set_db_config(eapp, "1")
                        return '+Eingeschaltet wurde: ' + eapp
                    else:
                        ti_ctx.cnf.set_db_config(eapp, "0")
                        return '+Ausgeschaltet wurde: ' + eapp
                ti_ctx.cnf.set_db_config(eapp, eapw)
                return '+Gesetzt wurde: ' + eapp + '="' + eapw + '"'
            username = ti_ctx.req.get_cgi('username')
            if len(username) > 0:
                rechtetoggle = ti_ctx.req.get_cgi_int('rechtetoggle')
                if rechtetoggle > 0 and rechtetoggle < 65:
                    rechte = ti_ctx.ses.get_userrechte(username)
                    rechte = rechte ^ rechtetoggle  # xor
                    ti_ctx.ses.set_userrechte(username, rechte)
                    return '+Rechte geändert'
                else:
                    return '-keine Rechte geändert'
            user2reset = ti_ctx.req.get_cgi('user2reset')
            if len(user2reset) > 0:
                ti_ctx.ses.reset_userpasswd(user2reset)
                return '+Passwort von ' + user2reset + ' wurde zurückgesetzt'
            user2delete = ti_ctx.req.get_cgi('user2delete')
            if len(user2delete) > 0:
                ti_ctx.ses.remove_dbuser(user2delete)
                return '+Der User ' + user2delete + ' wurde gelöscht'
            newuser = ti_ctx.req.get_cgi('newuser')
            if len(newuser) > 0:
                ti_ctx.ses.adduser(newuser)
                return '+' + newuser + ' wurde mit Standardpasswort angelegt'
            # Sonst...
            return '-Kein Parameter übergeben (' + easid + ')'
            # Abbruch
        else:
            return '-Falsche Session-ID! Nicht aus dem Browsercache aufrufen...'
    return ''  # wurde garnicht mit Formular aufgerufen
    
    
def make_debugfile(ti_ctx):
    import subprocess
    import ti_lib
    datei = ['''<!DOCTYPE html>\n<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
    </head>
    <body>
    <h1>Debuginformationen</h1>
    ''', konst.TI_BRANDING]
    datapath = ti_lib.get_ti_datapath()
    basepath = ti_lib.get_ti_basepath()

    files = [('ti.ini', 'yellow'),
             ('log/background.log', 'lightgreen'), 
             ('log/background.log.old', 'lightyellow')]
    commands = ['date', 
                'tree -d ' + datapath.as_posix(), 
                'ls -Rhaltn ' + datapath.as_posix(),
                'ls -Rhaltn -I.git ' + basepath.as_posix()]

    linkliste = '<b>GoTo:</b> '
    for i in range(len(files)):
        linkliste += f'<a href="#file{i}">{i}. {files[i][0]}</a> '
    for i in range(len(commands)):
        linkliste += f'<a href="#command{i}">{i}. {commands[i].split()[0]}</a> '
    linkliste += '<a href="#dbentries">DB</a>\n'
    
    for count, (filename, color) in enumerate(files):
        chunk = [f'''<h2 style="background-color:{color};" id="file{count}">Datei: {datapath / filename}</h2>\n
            <pre style="background-color:{color};">''']
        try:
            with (datapath / filename).open(mode='r', encoding='utf-8') as fin:
                for line in fin:
                    chunk.append(line)
            datei.append(linkliste)
            datei.append(''.join(chunk) + '\n</pre>')
        except Exception as e:
            import ti_lib
            datei.append('Datei konnte nicht gelesen werden<br>' + 
                         '<br>\n'.join(chunk) + '\n</pre>\n' + ti_lib.beschreibe_exception(e))
                         
    for count, command in enumerate(commands):
        chunk = [f'<h2 style="background-color:#DDF;" id="command{count}">Befehl: {command}</h2>\n<pre>']
        try:
            stdout = subprocess.check_output(command, shell=True, 
                                             stderr=subprocess.STDOUT).decode("utf-8", "ignore")
            for line in stdout.split("\n"):
                line = line.replace(' ', '&nbsp;')
                chunk.append(line)
            datei.append(linkliste)
            datei.append('\n'.join(chunk) + '\n</pre>')   
        except Exception as e:
            import ti_lib
            datei.append('Befehl konnte nicht ausgeführt werden<br>' + 
                         '<br>\n'.join(chunk) + '\n</pre>' + ti_lib.beschreibe_exception(e))

    dump = ti_ctx.cnf.dump_db_config()
    datei.append(linkliste)
    chunk = ['<h2 id="dbentries">DATENBANK: Konfigurationseinträge</h2>\n']
    chunk.append('<table>')
    for line in dump:
        chunk.append('<tr><td>{}</td><td>{}</td></tr>'.format(line[0], line[1]))
    chunk.append('</table>')
    datei.append('\n'.join(chunk))   
    datei.append(linkliste)
    datei.append("\n ** Das war's! Viel Erfolg beim Debuggen...")
    return "<br>\n<br>\n ============================================================== <br>\n<br>\n".join(datei)
    
    
def do_config(ti_ctx):
    if not ti_ctx.check_management_rights(konst.RECHTEAdministration):
        return [], -1

    debugfileflag = ti_ctx.req.get_cgi('debugfile')
    # ti_ctx.res.debug('debugfile', debugfileflag)
    if debugfileflag.lower() == 'true':
        df = make_debugfile(ti_ctx).encode('utf-8')
        sys.stdout.buffer.write(b'Content-Type: application/octet-stream\n')
        sys.stdout.buffer.write(b'Content-Disposition: attachment;filename=config.html.xz\n\n')
        sys.stdout.buffer.write(lzma.compress(df))
        exit(0)
    else:
        erfolg = config_todo(ti_ctx)
        ti_ctx.res.debug("Erfolg", erfolg)
        if erfolg:
            ti_ctx.set_erfolgsmeldung(erfolg)
        return config_dialog(ti_ctx)

def debugfile(ti_ctx):
    if not ti_ctx.check_management_rights(konst.RECHTEAdministration):
        return ''
    df = make_debugfile(ti_ctx).encode('utf-8')
    return lzma.compress(df)
    
if __name__ == '__main__':
    ti_lib.quickabort2frames()
