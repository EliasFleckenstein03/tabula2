#!/usr/bin/python3

"""
________________________________________________________________________
 ti.py                                                            
 EN: This file is part of tabula.info, which is free software under              
    the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
        der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti.py ist die Toolbox für die CGI-orientierten tabula.info-Module
        Allgemeine Tools finden sich in ti_lib
________________________________________________________________________
"""
# Batteries
import datetime
import time
import subprocess
# TI
import ti_lib
import konst
 
try:
    import config_db
    import ti_init
    import ti_context
except Exception as e:
    ti_lib.panic('ti.py: IMPORT Teil 1 fehlgeschlagen', e, '')


def check_n_background(ti_cnf):
    # normale funktion, die aber einen Betriebssystem-Hintergrundtask erzeugen kann
    timeout = ti_cnf.get_db_config_int('background_timeout', 0) 
    # timeout: der Zeitpunkt, ab dem routinemäßig der Task laufen soll.
    now = ti_lib.get_now()
    if now < timeout and not ti_cnf.get_db_config_bool('s_any_force_convert'):
        # ti_lib.log('BKG: zu früh für einen Hintergrundprozess\n') # geschwätzig
        return False    # Es ist nichts zu erledigen
    cgipfad = ti_lib.get_ti_binpath().as_posix()
    daskommando = ["sh", "-c", "cd {pfad}; ./background.py".format(pfad=cgipfad)]
    ti_lib.log(f'BKG: ->Aufruf: {daskommando}')
    ti_lib.log('BKG: ->Beginn Hintergrundprozessstarten: {t}'.format(t=time.time()))
    subprocess.Popen(daskommando, stdout=subprocess.DEVNULL)
    ti_lib.log('BKG: ->Ende Hintergrundprozessstarten:   {t}'.format(t=time.time()))
    # da die Ausgabe nicht interessiert wird der Prozess von Python nicht weiter verfolgt -> Hintergrundprozess
    return True


##################################
# Stringfunktionen, die get_cgi* nutzen
def strg2int(value, default=0):
    return ti_lib.strg2int(value, default)


def strg2bool(value, default=False):
    return ti_lib.strg2bool(value, default)

    
####################################
# Identifikation des Clients


def client_force_gruppen(clientgroups):
    ti_lib.client_force_gruppen(clientgroups)
    
      
#######################################################################
# Zeitfunktionen
# dayofweek bzw. dow ist 0...6, wobei Montag 0 bedeutet
# day_of_year bzw. doy ist die Nummer des Tags im Jahr
# dayofepoche ist ein eigenes int-Zeitformat yyyyddd 
#      mit vierstelliger Jahreszahl (Tausender) und dann 
#      dreistelligem doy (Einer)
# epoche ist die Zahl der Sekunden in der Unix-epoche (seit 1.1.1970)

def get_todays_calendarweek():
    dummy, cw, dummy = datetime.date.today().isocalendar()
    return cw
    
    
def get_todays_dayofweek():
    return get_dayofweek(time.time())
    
    
def get_todays_dayofepoche():
    return get_dayofepoche(time.time())
    
    
def get_dayofweek(epoche):
    ts = time.localtime(epoche)
    dow = ts.tm_wday     # Monday = 0
    return dow
    
    
def get_dayofepoche(epoche):            # eigenes Zeitformat yyyyddd
    ts = time.localtime(epoche)
    doe = ts.tm_year * 1000 + ts.tm_yday
    return doe


def get_epoche_from_dayofepoche(doe):
    return time.mktime((doe // 1000, 1, doe % 1000, 0, 0, 0, 0, 0, 0))


def get_dayname_from_dayofepoche(doe):
    epoche = time.mktime((doe // 1000, 1, doe % 1000, 0, 0, 0, 0, 0, 0)) 
    # 3. Feb 2009 wird als doe = 2009034 umgewandelt in 34. Januar 2009
    return time.strftime("%A", time.localtime(epoche))


def get_daydate_from_dayofepoche(doe):
    epoche = time.mktime((doe // 1000, 1, doe % 1000, 0, 0, 0, 0, 0, 0))
    return time.strftime("%d.%m.", time.localtime(epoche))


def get_datestring_from_epoche(epoche):
    return time.strftime('%a, %d %b %Y %H:%M:%S', time.localtime(epoche))


def get_timestring_from_epoche(epoche):
    return time.strftime('%Y-%m-%d_%T', time.localtime(epoche))


def vergleiche_mit_uhrzeit(stunde, minute=0):
    'liefert -1, 0, 1 wenn aktuelle Uhrzeit kleiner, gleich, größer der übergebenen Uhrzeit ist'
    # debug('vergleiche_m_U', str(stunde)+', '+str(minute)+'?'+str(__ti_hour_min__))
    if type(stunde) is str:
        beide = stunde.split(':', 1)
        stunde = int(beide[0]) if beide[0].isdigit() else 0
        minute = int(beide[1]) if stunde >= 0 and len(beide) == 2 and beide[1].isdigit() else 0
    elif type(stunde) is tuple:
        stunde, minute = stunde
    zeit = (stunde % 24) * 100 + minute % 60
    aktzeit = ti_lib.get_akt_zeit()
    # debug('vergleiche_m_U', str(zeit) + '?' + str(aktzeit))
    if aktzeit < zeit:
        return -1
    if aktzeit > zeit:
        return 1
    return 0


# Berechnung des um delta weitergerechneten Arbeitstag (Mo-Fr) ab heute 
# (0 = heute, führt aber, wenn heute ein Sa oder So ist zu Montag!)
def get_following_workdayofepoche(delta):
    ts = time.localtime()
    day_of_year = ts.tm_yday
    # if day_of_year + delta > 357:
    #    day_of_year + =14                      # halbsauberer Weihnachtsferien-Workaround
    day_of_week = ts.tm_wday
    deltaweeks = delta // 5                     # There are just 5 work days
    delta = delta % 5

    # Die folgende Tabelle hat als ersten Index den aktuellen Wochentag und als zweiten das Delta modulo 5
    # Mit dem Tabellenwert ist das aktuelle Datum zu addieren um den entsprechend delta-nächsten Arbeitstag zu finden
    skip_weekend_delta = [[0, 1, 2, 3, 4], [0, 1, 2, 3, 6], [0, 1, 2, 5, 6], 
                          [0, 1, 4, 5, 6], [0, 3, 4, 5, 6], [2, 3, 4, 5, 6], [1, 2, 3, 4, 5]]
    doy = day_of_year + skip_weekend_delta[day_of_week][delta] + 7 * deltaweeks
    doe = ts.tm_year * 1000 + doy
    # if doy > 366:
    return get_dayofepoche(get_epoche_from_dayofepoche(doe))
    # return doe


# liefert die aktuelle Schulstunde von heute (0) oder prüft erst, dass display_day heute ist
def get_pseudotime(delta=0):
    t = time.localtime()
    realhour = t.tm_hour
    realmin = t.tm_min + delta
    if realmin < 0:
        realmin += 60
        realhour -= 1
    elif realmin > 59:
        realmin -= 60
        realhour += 1
    return realhour + realmin * 0.01  # eigenes Fließkommazahlenformat zum einfacheren Vergleichen


def get_akt_hour(display_day=0):
    if display_day != 0 and display_day != get_todays_dayofepoche():
        return -1
    realtim = get_pseudotime(-5)  # eigenes Fließkommazahlenformat zum einfacheren Vergleichen
    if realtim >= 15.20:
        return 10
    if realtim >= 14.35:
        return 9
    if realtim >= 13.50:
        return 8
    if realtim >= 13.05:
        return 7
    if realtim >= 12.05:
        return 6
    if realtim >= 11.20:
        return 5
    if realtim >= 10.20:
        return 4
    if realtim >= 9.35:
        return 3
    if realtim >= 8.35:
        return 2
    if realtim >= 7.50:
        return 1
    return 0


def get_string_from_epoche(epoche=0, pre="", middle=", ", post="", dtsep=""):
    if epoche < 0:  # soll ein typischer timestamp werden - z.B. für Logfiles
        epoche = 0
        conf = "%Y-%m-%d_%H:%M"
    else:
        if middle:
            conf = pre + "%A" + middle + "%d. %B %Y, %H:%M" + post 
        elif dtsep:
            conf = "%d. %B %Y" + dtsep + "%H:%M"
        else:
            conf = "%d.%m.%y %H:%M"
    if epoche == 0: 
        epoche = time.time()
    return time.strftime(conf, time.localtime(epoche))


def get_string_from_dayofepoche(doe, short=False, mediumshort=False, veryshort=False):
    epoche = time.mktime((doe // 1000, 1, doe % 1000, 0, 0, 0, 0, 0, 0)) 
    if veryshort:
        return time.strftime("%a, %d.", time.localtime(epoche))
    if mediumshort:
        return time.strftime("%a, %d.%m.", time.localtime(epoche))
    if short:
        return time.strftime("%A, %d.&nbsp;%b", time.localtime(epoche))
    return time.strftime("%A, %d.%m.%y", time.localtime(epoche))


def get_dayofepoche_from_string(strg):
    try:
        return get_dayofepoche(time.mktime(time.strptime(strg, "%d.%m.%Y")))
    except ValueError:
        return 1970001  # dummy
    
    
def get_epoche_from_string(strg):
    return int(time.mktime(time.strptime(strg.strip(), "%d.%m.%Y %H:%M")))
    

def get_html_head(ti_ctx, title="tabula.info", extrastyle="", timeout=-1, url="", script_delay=0, tofile=False, 
                  includesearch=False, frameset='', ismanagement=False, keinemeldung=False, onload='', onclick_to=''):
    # eventuelle header (z.B. wg. Cookies) müssen vorher gesendet werden!
    h = "Content-Type: text/html\n\n" if not tofile else ""
    
    if timeout != -1:
        refresh = '<meta http-equiv="refresh" content="{timeout}; URL={url}">'.format(timeout=timeout, url=url)
    else:
        refresh = ""
    if includesearch:
        headend = '''
    <script language="javascript">
        function get_url_param( name )  {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]"+name+"=([^&#]*)";
            var regex = new RegExp( regexS );
            var results = regex.exec( window.location.href );
            if ( results == null )
                return "";
            else
                return results[1];
        }
        function recurse_childs(the_item) {
            var obj = the_item;
            if (the_item.nodeType == 3) {
                return the_item.data; }
            if (the_item.nodeType == 1) {
                var elements = the_item.childNodes;
                var puretext = "";
                for (var e=0; e < elements.length; e++) {
                    puretext += recurse_childs(elements[e]); 
                }
                return puretext;
            }
            return "";
        }
        function highlight_table(t, hl, rl) {
            var obj = t;
            var rows = [];
            var cells = [];
            var self = this;
            var trefferzahl = 0;
            var body = t.getElementsByTagName('tbody').length ? t.getElementsByTagName('tbody')[0] : t;
            var rows = body.getElementsByTagName('tr');
            
            for(var i = 0; i < rows.length; i += 1) {
                doyell=0
                dored=0
                cells=rows[i].getElementsByTagName("td");
                for( var z=0;z< cells.length;z += 1) {
                    if (cells[z].childNodes.length >0) {                /* Die Zelle hat Kindelemente */
                        txt=recurse_childs(cells[z]);
                        txt=txt.toLowerCase();
                        /*txt=cells[z].childNodes[0].data;*/
                        /*if ( z == 0 )
                            alert("neue Zeile"); */
                        if ( hl.length>0 && txt.length  >= hl.length && txt.indexOf( hl )  >= 0 ) 
                            doyell=1;
                        if ( rl.length>0 && txt.length  >= rl.length && txt.indexOf( rl )  >= 0 ) 
                            dored=1;
                    }
                    /*cells[z].style.backgroundColor="#DDDDFF";*/
                }
                
                if (doyell == 1 && dored == 0) {
                    trefferzahl += 1;
                    for( var z=0;z< cells.length;z += 1) {
                        cells[z].style.backgroundColor="#FFFFAA";
                    }
                }
                if (doyell == 0 && dored == 1) {
                    
                    trefferzahl += 1;
                    for( var z=0;z< cells.length;z += 1) {
                        cells[z].style.backgroundColor="#AA88FF";
                    }
                }
                if (doyell == 1 && dored == 1) {
                    
                    trefferzahl += 1;
                    for( var z=0;z< cells.length;z += 1) {
                        cells[z].style.backgroundColor="#FF88AA";
                    }
                }
            } /* if rows.length>0 */
            return trefferzahl;
        } /* end function */

        function do_highlight() {
            var treffersumme=0;
            hl=decodeURIComponent(get_url_param('highlight'));
            rl=decodeURIComponent(get_url_param('redlight'));
            hl=hl.toLowerCase();
            rl=rl.toLowerCase();
    
            var t = document.getElementsByTagName('table');
            if (hl.length>0 || rl.length>0) for(var i = 0; i < t.length; i++) {
                treffersumme += highlight_table(t[i], hl, rl);
            }
        }
        window.onload=do_highlight;
    </script>   
    </head>
    '''
    else:
        headend = '''
    </head>
    '''
        
    head = '''<!DOCTYPE html>
<html>
    <head>
        <title>{title}</title>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="cache-control"    content="no-cache">
        <meta http-equiv="pragma"           content="no-cache">
        <meta http-equiv="expires"          content="-1">
        <link rel="shortcut icon" href="../static/pix/favicon{iconnummer}.ico">
        {meta}
        {css}
        {es}
        {he}'''  
    if ti_ctx:
        til = ti_ctx.get_ti_lastupload()
    else:
        til = 17
    h += head.format(title=title, meta=refresh, 
                     css=ti_lib.standardcss(ismanagement, til=til), 
                     es=extrastyle, he=headend, iconnummer='2' if ismanagement else '')
    if frameset != '':
        h += frameset
    else:
        h += '<body'
        if onload:
            h += f' onload="{onload}"'
        if onclick_to:
            h += f''' onclick="window.location.href='{onclick_to}'"'''
        h += '>'
        if ti_ctx and ismanagement and not keinemeldung:
            h += ti_ctx.ses.get_session_meldung()
    return h


def prt(*args):
    ti_lib.prt(*args)


# gibt den Status der Anzeigen als HTML-Liste aus
def astatus(ti_ctx, wsgi=False):
    output = []
    output.append(get_html_head(
        ti_ctx, 
        url='#', 
        timeout='20', 
        title=_("tabula.info Anzeigenstatus - Bitte Vollbildschirm wählen"), 
        tofile=wsgi))
    output += ti_ctx.cnf.client_show_status()  # listen konkatenieren
    output.append(konst.TI_BRANDING)
    output.append(ti_ctx.get_html_tail())
    return output

  
def check_semaphore(name):
    return ti_lib.check_n_set_semaphore(name, onlycheck=True)
    

def master_init():
    # #############################################################    
    # ## hier beginnt der Code zur Initialisierung              ###
    # #############################################################
    # ## Module, die direkt aufgerufen werden, sind selbst      ###
    # ## dafür verantwortlich diese Funktion nach allen Importen ##
    # ## aufzurufen.                                            ###
    # #############################################################
            
    ti_sys = ti_init.TI_System()
    ti_cnf = config_db.TI_Config()
    ti_cnf.conf_db_upgrade()  # nur für upgrades nötig, kostet aber nix...
    ti_res = ti_init.TI_Response(ti_sys.get_epoche())
    ti_req = ti_init.TI_Request_CGI(ti_cnf, ti_res)

    # # Import Teil 2
    # # Diese Importe setzen eine grundsätzliche Initialisierung voraus
    try:
        import login
    except Exception as e:
        ti_lib.panic('ti.py: IMPORT Teil 2 fehlgeschlagen', e, '')

    ti_ses = login.TI_Session(ti_req, ti_cnf, ti_sys.get_epoche())

    ti_ctx = ti_context.TIConTeXt(ti_sys, ti_cnf, ti_req, ti_res, ti_ses)
    
    # # untätig, wenn crontab alle 1-4 Minuten klingelt:
    if check_n_background(ti_cnf):
        ti_res.debug('Background noetig')

    ti_res.debug("tabula.info in ti.py fertig initialisiert")   
    return ti_ctx


if __name__ == '__main__':
    ti_lib.quickabort2frames()
